using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public PlayerElements pElements;

	public State_Not_Sitting notSittingState;

	public State_Wait_For_Game_T0_Start waitForGameToStartState;

	public State_Game_Starts gameStartsState;

	public State_Wait_For_My_Turn waitForMyTurnState;

	public State_My_Turn myTurnState;

	public State_Side_Show sideShowState;

	public State_Force_Side_Show forceSideShowState;

	public State_Pack packState;

	public State_Pot_Limit_Over potLimitOverState;

	public State_Game_Win gameWinState;

	public State_Game_Finished gameFinishedState;

	public State_User_Show userShowState;
}
