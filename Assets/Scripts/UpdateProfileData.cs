using EasyMobile;
using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class UpdateProfileData : MonoBehaviour
{
	public FillMainMenu fillMainMenu;

	public TextMeshProUGUI proText;

	public Toggle[] nameToggles;

	public Toggle[] profileToggles;

	public void ChangeNameAndProfile()
	{
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
		for (int i = 0; i < profileToggles.Length; i++)
		{
			if (profileToggles[i].isOn)
			{
				jSONObject.AddField("profile", profileToggles[i].name);
			}
		}
		for (int j = 0; j < nameToggles.Length; j++)
		{
			if (nameToggles[j].isOn)
			{
				URL.Instance.DebugPrint("Name : " + nameToggles[j].GetComponentInChildren<TextMeshProUGUI>().text);
				jSONObject.AddField("tempname", nameToggles[j].GetComponentInChildren<TextMeshProUGUI>().text);
			}
		}

		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.NAME_SELECTOR, jSONObject.ToString())).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("NAME_SELECTOR " + x);
			PlayerData playerData = PlayerData.CreateFromJSON(x);
			if (playerData.status == "success")
			{
				NativeUI.ShowToast("Name & Profile updated successfully.");
				fillMainMenu.profileNameText.text = URL.Instance.TrimNameForOthers(GameManager.Instance.localPlayerName, GameManager.Instance.isFacebookLogin, playerData.response.tempname);
				GameManager.Instance.tempName = fillMainMenu.profileNameText.text;
				if (int.Parse(playerData.response.profile) >= 0)
				{
					GameManager.Instance.LoadImagesCO(fillMainMenu.profileImage, playerData.response.profile);
				}
				else
				{
					//fillMainMenu.profileImage.sprite = GameManager.Instance.sCanvas.settingsProfileImage.sprite;
				}
				GameManager.Instance.tempImage = playerData.response.profile;
			}
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
		});
	}
}
