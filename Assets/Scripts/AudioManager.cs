using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	public static AudioManager instance;

	public Sounds[] sounds;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(base.gameObject);
		}
		else
		{
			Destroy(base.gameObject);
		}
	}

    private void Start()
    {
        if (!PlayerPrefs.HasKey("Sound"))
        {
			PlayerPrefs.SetInt("Sound", 1);
		}

		if (!PlayerPrefs.HasKey("Music"))
		{
			PlayerPrefs.SetInt("Music", 1);
		}

		if (!PlayerPrefs.HasKey("Vibration"))
		{
			PlayerPrefs.SetInt("Vibration", 1);
		}

		Sounds[] array = this.sounds;
		foreach (Sounds sounds in array)
		{
			sounds.source = base.gameObject.AddComponent<AudioSource>();
			sounds.source.clip = sounds.clip;
			sounds.source.volume = sounds.volume;
			sounds.source.pitch = sounds.pitch;
			sounds.source.loop = sounds.loop;
		}
	}

    public void PlaySound(string name)
	{
		Sounds sounds = Array.Find(this.sounds, (Sounds sound) => sound.name == name);
		if (sounds == null)
		{
			Debug.Log("Sound " + name + " not found!");
		}
		else if (PlayerPrefs.GetInt("Sound") == 1)
		{
			sounds.source.Play();
		}
	}

	public void StopSound(string name)
	{
		Sounds sounds = Array.Find(this.sounds, (Sounds sound) => sound.name == name);
		if (sounds == null)
		{
			Debug.Log("Sound " + name + " not found!");
		}
		else
		{
			sounds.source.Stop();
		}
	}

	public void PlayVibration()
	{
		if (PlayerPrefs.GetInt("Vibration") == 1)
		{
			Handheld.Vibrate();
		}
	}
}


