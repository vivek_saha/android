using DG.Tweening;
using EasyMobile;
using SocketIO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NetworkManager : MonoBehaviour
{
    public SocketIOComponent socket;

    public TableElements tableElements;

    public GetPlayersWithPosition players;

    public ChatCanvas chatElement;

    public GiftCanvas giftElement;

    private bool isPack;

    private GameObject firstPos;

    private GameObject secPos;

    private GameObject secForcePos;

    private GameObject firstGiftPos;

    private GameObject secGiftPos;

    public GameObject sideshowEffect;

    //public GameObject forcesideshowEffect;

    private int activeUsers;

    private int activeUsersForMessages;

    private int firstFSSIndex;

    private int secondFSSIndex;

    private float fSSRotation;

    public Queue<SocketIOEvent> networkResponses;

    private string googleAnalyticsString;

    private bool isNotDelay;

    private bool isSideOrForceSideShow;

    public GetButtonList localButtons;

    public GetButtonList nonLocalButtons;

    //public Button liveButton;
    //public string liveurl;

    private int firPosInt;

    private int secPosInt;

    private bool user;

    private List<string> apID = new List<string>();

    private List<string> auID = new List<string>();

    private bool gameIsInBackground;

    private bool canReadResponses;

    private float time;

    public float interpolationPeriod = 1f;

    private DateTime date1;

    private DateTime date2;

    private WWW www;


    private void Awake()
    {
        //socket = FindObjectOfType<SocketIOComponent>();
        //tableElements = FindObjectOfType<TableElements>();
        //players = FindObjectOfType<GetPlayersWithPosition>();
        //forcesideshowEffect.transform.localScale = Vector3.zero;
        GameManager.Instance.isSocketConnectedToServer = false;
        networkResponses = new Queue<SocketIOEvent>();
        canReadResponses = true;
        //GameObject gameObject = new GameObject("UniWebView");
    }


    private void Start()
    {
        socket.Connect();
        socket.On("tableResponse", OnTableResponse);
        StartCoroutine(isSocketConnectedCO());
        isNotDelay = false;
        isSideOrForceSideShow = false;
    }

    private IEnumerator isSocketConnectedCO()
    {
        while (!GameManager.Instance.isSocketConnectedToServer)
        {
            yield return null;
        }

        JSONObject newData = new JSONObject();
        newData.AddField("tbid", GameManager.Instance.tbid);
        newData.AddField("playerId", GameManager.Instance.localPlayerID);
        newData.AddField("type", GameManager.Instance.type);
        newData.AddField("tblBootedAmount", GameManager.Instance.tblBootedAmount);

        socket.Emit("play", newData);
    }

    private void OnTableResponse(SocketIOEvent r)
    {
        networkResponses.Enqueue(r);
    }

    private IEnumerator delayInResponsesCO(float _delay)
    {
        canReadResponses = false;
        if (!isNotDelay)
        {
            yield return new WaitForSeconds(_delay);
        }
        else
        {
            yield return new WaitForSeconds(0f);
        }
        canReadResponses = true;
    }

    private void OnQueueResponse(SocketIOEvent r)
    {
        networkResponses.Dequeue();

        //string data = GameManager.Instance.security.Decrypt(r.data.GetField("data").ToString().Trim('"'));
        //SocketIOEvent socketIOEvent = new SocketIOEvent("data", new JSONObject(GameManager.Instance.security.Decrypt(r.data.GetField("data").ToString().Trim('"'))));

        /*string data = r.data.GetField("data").ToString().Trim('"');
        SocketIOEvent socketIOEvent = new SocketIOEvent("data", new JSONObject(r.data.GetField("data").ToString().Trim('"')));*/

        string data = r.data.ToString();
        SocketIOEvent socketIOEvent = r;


        URL.Instance.DebugPrint(socketIOEvent);
        TableResponseData tableResponseData = TableResponseData.CreateFromJSON(data);

        if (tableResponseData.responseMessage == "get_table_info")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            if (socketIOEvent.data.GetField("response")[0].HasField("type"))
            {
                if (tableResponseData.response[0].type == "limit")
                {
                    tableElements.tableType = "limit";
                    //tableElements.switchButton.SetActive(value: true);
                    //liveButton.gameObject.SetActive(value: false);
                }
                else if (tableResponseData.response[0].type == "nolimit")
                {
                    tableElements.tableType = "nolimit";
                    tableElements.maxChaalText.text = "No Limit";
                    tableElements.maxChaalText_Info.text = "No Limit";
                    tableElements.maxPotLimitText.text = "No Limit";
                    tableElements.maxPotLimitText_Info.text = "No Limit";
                    //tableElements.switchButton.SetActive(value: true);
                    //liveButton.gameObject.SetActive(value: false);
                }
                else if (tableResponseData.response[0].type == "variation_limit")
                {
                    tableElements.tableType = "variation_limit";
                    //tableElements.switchButton.SetActive(value: true);
                    if (socketIOEvent.data.GetField("response")[0].HasField("var_type"))
                    {
                        URL.Instance.DebugPrint("Limit : " + tableResponseData.response[0].var_type);
                        if (tableResponseData.response[0].var_type.Length > 0)
                        {
                            tableElements.variationTableCodeGO.SetActive(value: true);
                        }
                        SetVariationText(tableElements.variationTableCodeText, tableResponseData.response[0].var_type);
                    }
                    //liveButton.gameObject.SetActive(false);
                }
                else if (tableResponseData.response[0].type == "variation_nolimit")
                {
                    tableElements.tableType = "variation_nolimit";
                    tableElements.maxChaalText.text = "No Limit";
                    tableElements.maxChaalText_Info.text = "No Limit";
                    tableElements.maxPotLimitText.text = "No Limit";
                    tableElements.maxPotLimitText_Info.text = "No Limit";
                    //tableElements.switchButton.SetActive(value: true);
                    if (socketIOEvent.data.GetField("response")[0].HasField("var_type"))
                    {
                        URL.Instance.DebugPrint("No Limit : " + tableResponseData.response[0].var_type);
                        if (tableResponseData.response[0].var_type.Length > 0)
                        {
                            tableElements.variationTableCodeGO.SetActive(value: true);
                        }
                        SetVariationText(tableElements.variationTableCodeText, tableResponseData.response[0].var_type);
                    }
                    //liveButton.gameObject.SetActive(value: false);
                }
                else if (tableResponseData.response[0].type == "private")
                {
                    tableElements.tableType = "private";
                    //tableElements.switchButton.SetActive(value: false);
                    //liveButton.gameObject.SetActive(value: false);
                }
                else if (tableResponseData.response[0].type == "OLC")
                {
                    tableElements.tableType = "OLC";
                    //tableElements.switchButton.SetActive(value: true);
                    //liveButton.gameObject.SetActive(value: true);
                }
            }
            if (socketIOEvent.data.GetField("response")[0].HasField("bootedAmountChips") && tableResponseData.response[0].bootedAmountChips.Length > 0)
            {
                tableElements.bootAmountText.text = GameManager.Instance.format.FormatNumberOnTableInfo(long.Parse(tableResponseData.response[0].bootedAmountChips));
                tableElements.bootAmountText_Info.text = GameManager.Instance.format.FormatNumberOnTableInfo(long.Parse(tableResponseData.response[0].bootedAmountChips));
            }
            if (socketIOEvent.data.GetField("response")[0].HasField("chaalAmountLimit") && tableResponseData.response[0].chaalAmountLimit.Length > 0)
            {
                tableElements.maxChaalText.text = GameManager.Instance.format.FormatNumberOnTableInfo(long.Parse(tableResponseData.response[0].chaalAmountLimit));
                tableElements.maxChaalLimit = long.Parse(tableResponseData.response[0].chaalAmountLimit);
                tableElements.maxChaalText_Info.text = GameManager.Instance.format.FormatNumberOnTableInfo(long.Parse(tableResponseData.response[0].chaalAmountLimit));
                tableElements.maxChaalLimit = long.Parse(tableResponseData.response[0].chaalAmountLimit);
            }
            if (socketIOEvent.data.GetField("response")[0].HasField("tableAmountLimit") && tableResponseData.response[0].tableAmountLimit.Length > 0)
            {
                tableElements.maxPotLimitText.text = GameManager.Instance.format.FormatNumberOnTableInfo(long.Parse(tableResponseData.response[0].tableAmountLimit));
                tableElements.maxTableLimit = long.Parse(tableResponseData.response[0].tableAmountLimit);
                tableElements.maxPotLimitText_Info.text = GameManager.Instance.format.FormatNumberOnTableInfo(long.Parse(tableResponseData.response[0].tableAmountLimit));
                tableElements.maxTableLimit = long.Parse(tableResponseData.response[0].tableAmountLimit);
            }
            if (socketIOEvent.data.GetField("response")[0].HasField("private_table_code"))
            {
                if (tableResponseData.response[0].private_table_code.Length > 0)
                {
                    tableElements.privateTableCodeGO.SetActive(value: true);
                }
                tableElements.privateTableCode = tableResponseData.response[0].private_table_code;
                tableElements.privateTableCodeText.text = tableElements.privateTableCode;
                PlayerPrefs.SetString("PreviousBootAmount", tableResponseData.response[0].bootedAmountChips);
            }
            if (socketIOEvent.data.GetField("response")[0].HasField("totalAmount") && long.Parse(tableResponseData.response[0].totalAmount) > 0)
            {
                tableElements.GetTotalAmountOnTable(long.Parse(tableResponseData.response[0].totalAmount));
            }
            if (!tableElements.isGamePlaying)
            {
                tableElements.tableInfoGO.SetActive(value: true);
            }
            GameManager.Instance.LogMessage("Table : " + tableResponseData.response[0].type + " - Boot Amount : " + GameManager.Instance.format.FormatNumberOnTableInfo(long.Parse(tableResponseData.response[0].bootedAmountChips)));
            googleAnalyticsString = "C Table : " + tableResponseData.response[0].type + " - Boot Amount : " + GameManager.Instance.format.FormatNumberOnTableInfo(long.Parse(tableResponseData.response[0].bootedAmountChips));
        }
        else if (tableResponseData.responseMessage == "new_players_connected")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (tableResponseData.status == "success")
            {
                AudioManager.instance.PlaySound("new-player-joined");
                activeUsersForMessages = tableResponseData.response.Length;
                for (int i = 0; i < tableResponseData.response.Length; i++)
                {
                    if (!players._players[i].playerPanelObject.activeInHierarchy)
                    {
                        if (tableResponseData.response[i].playerId == GameManager.Instance.localPlayerID)
                        {
                            players._players[2].pController.pElements.isLocalPlayer = true;
                            GameManager.Instance.localPlayerPosition = long.Parse(tableResponseData.response[i].position);
                            GameManager.Instance.tempLocalPlayerPosition = GameManager.Instance.localPlayerPosition + 1;
                            players._players[2].pController.pElements.playerID = tableResponseData.response[i].playerId;
                            isNotDelay = false;
                        }
                        else
                        {
                            players._players[i].pController.pElements.isLocalPlayer = false;
                        }
                    }
                }
                for (int j = 0; j < tableResponseData.response.Length; j++)
                {
                    for (int k = 0; k < players._players.Length; k++)
                    {
                        if (ConvertPlayersPosition(tableResponseData.response[j].position) == players._players[k].localPosition && !players._players[k].playerPanelObject.activeInHierarchy)
                        {
                            players._players[k].playerID = tableResponseData.response[j].playerId;
                            players._players[k].pController.pElements.localPosition = ConvertPlayersPosition(tableResponseData.response[j].position);
                            players._players[k].pController.pElements.UpdateDetailsUI(socketIOEvent, j, _updateChipsText: true);
                        }
                    }
                }
                for (int l = 0; l < tableResponseData.response.Length; l++)
                {
                    for (int m = 0; m < players._players.Length; m++)
                    {
                        if (!(players._players[m].pController.pElements.position == tableResponseData.response[l].position))
                        {
                            continue;
                        }
                        if (!players._players[m].playerPanelObject.activeInHierarchy)
                        {
                            players._players[m].pController.pElements.ActivePlayers();
                            if (players._players[m].pController.pElements.sittingTable && players._players[m].pController.pElements.isActive)
                            {
                                if (tableResponseData.response[l].turn == "1")
                                {
                                    tableElements.amountMultiplier = 1L;
                                    players._players[m].pController.myTurnState.StartTimer();
                                    if (players._players[m].pController.pElements.isLocalPlayer)
                                    {
                                        players._players[m].pController.myTurnState.MyTurn();
                                    }
                                    if (socketIOEvent.data.GetField("response")[l].HasField("timer"))
                                    {
                                        players._players[m].pController.pElements.timer = float.Parse(tableResponseData.response[l].timer);
                                    }
                                }
                                else
                                {
                                    players._players[m].pController.waitForMyTurnState.StopTimer();
                                    if (players._players[m].pController.pElements.isLocalPlayer)
                                    {
                                        players._players[m].pController.waitForMyTurnState.WaitForMyTurnForSS();
                                    }
                                }
                                players._players[m].pController.pElements.cardsGO.SetActive(true);
                                if (!players._players[m].pController.pElements.isBlind && !players._players[m].pController.pElements.isLocalPlayer)
                                {
                                    //players._players[m].pController.pElements.seenGO.SetActive(value: true);
                                    players._players[m].pController.pElements.cardStatus.text = "Card Seen";
                                }
                                if (players._players[m].pController.pElements.isBlind && players._players[m].pController.pElements.isLocalPlayer)
                                {
                                    players._players[m].pController.pElements.viewCardButton.gameObject.SetActive(value: true);
                                }
                                if (players._players[m].pController.pElements.isLocalPlayer)
                                {
                                    players._players[m].pController.pElements.AssignCards(socketIOEvent, l, _autoView: true, _playCardAnimation: true);
                                }
                                else
                                {
                                    players._players[m].pController.pElements.AssignCards(socketIOEvent, l, _autoView: true, _playCardAnimation: false);
                                }
                                if (tableResponseData.response[l].isBlind == "true")
                                {
                                    if (int.Parse(tableResponseData.response[l].blind) > 1)
                                    {
                                        players._players[m].pController.pElements.AssignTurnAmount(socketIOEvent, l, _rejoin: true, _preAmount: true);
                                    }
                                }
                                else
                                {
                                    players._players[m].pController.pElements.AssignTurnAmount(socketIOEvent, l, _rejoin: true, _preAmount: true);
                                }
                            }
                        }
                        if (players._players[m].pController.pElements.sittingTable && !players._players[m].pController.pElements.isActive)
                        {
                            players._players[m].pController.waitForGameToStartState.WaitForGameToStart();
                            if (activeUsersForMessages > 1)
                            {
                                GameManager.Instance.LanguageConverter(tableElements.messageText, "Game starting shortly...");
                            }
                            else
                            {
                                GameManager.Instance.LanguageConverter(tableElements.messageText, "Waiting for other players...");
                            }
                        }
                        /*if (tableResponseData.response[l].force == "1")
						{
							players._players[m].pController.pElements.forceSSGO.transform.localScale = Vector3.one;
						}
						else
						{
							players._players[m].pController.pElements.forceSSGO.transform.localScale = Vector3.zero;
						}*/
                        if (tableResponseData.response[l].sideShow == "2")
                        {
                            secPos = players._players[m].pController.pElements.cardsGO.gameObject;
                            players._players[m].pController.sideShowState.SideShowReceiver();
                            tableElements.isSideShowSliderOn = true;
                            if (players._players[m].pController.pElements.isLocalPlayer)
                            {
                                if (socketIOEvent.data.GetField("response")[l].HasField("sstimer"))
                                {
                                    tableElements.sideShowSlider.value = float.Parse(tableResponseData.response[l].sstimer);
                                }
                                if (socketIOEvent.data.GetField("response")[l].HasField("ssmaxTimer"))
                                {
                                    tableElements.sideShowSlider.maxValue = float.Parse(tableResponseData.response[l].ssmaxTimer);
                                }
                            }
                        }
                        else if (tableResponseData.response[l].sideShow == "1")
                        {
                            firstPos = players._players[m].pController.pElements.transform.gameObject;
                            players._players[m].pController.sideShowState.SideShowSender();
                            tableElements.acceptSideShowText.text = "Accept Sideshow Request From " + URL.Instance.TrimNameForSideshow(tableResponseData.response[l].name, tableResponseData.response[l].facebook, tableResponseData.response[l].tempname);
                            if (!IsInvoking("SideEffect"))
                            {
                                InvokeRepeating("SideEffect", 0f, 0.1f);
                            }
                        }
                    }
                }
            }
            GameManager.Instance.loadingCanvas.LoadingState(false, false);
        }
        else if (tableResponseData.responseMessage == "start_game")
        {
            StartCoroutine(delayInResponsesCO(3f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            firstPos = null;
            secPos = null;
            secForcePos = null;
            tableElements.isGamePlaying = true;
            if (!tableElements.totalAmountOnTableGO.activeInHierarchy)
            {
                tableElements.totalAmountOnTableText.text = string.Empty;
                tableElements.totalAmountOnTableGO.SetActive(value: true);
            }
            for (int n = 0; n < tableResponseData.response.Length; n++)
            {
                for (int num = 0; num < players._players.Length; num++)
                {
                    if (players._players[num].pController.pElements.position == tableResponseData.response[n].position)
                    {
                        for (int num2 = 0; num2 < players._players[num].pController.pElements.cardMainPanel.Length; num2++)
                        {
                            AudioManager.instance.PlaySound("card-distribution");
                            players._players[num].pController.pElements.cardMainPanel[num2].GetComponent<RectTransform>().localScale = Vector3.zero;
                            players._players[num].pController.pElements.cardMainPanel[num2].GetComponent<RectTransform>().rotation = new Quaternion(0f, 0f, -180f, 0f);
                            players._players[num].pController.pElements.cardMainPanel[num2].GetComponent<RectTransform>().position = tableElements.cardDistributePos.transform.position;
                            players._players[num].pController.pElements.cardMainPanel[0].GetComponent<RectTransform>().DOMove(players._players[num].pController.pElements.cardMainPanel_1_FirstPos.transform.position, 0.5f).SetDelay(0.1f * (float)n + 0.1f);
                            players._players[num].pController.pElements.cardMainPanel[0].GetComponent<RectTransform>().DOScale(Vector3.one, 0.5f).SetDelay(0.1f * (float)n + 0.1f);
                            players._players[num].pController.pElements.cardMainPanel[0].GetComponent<RectTransform>().DORotate(Vector3.zero, 0.5f).SetDelay(0.1f * (float)n + 0.1f);
                            players._players[num].pController.pElements.cardMainPanel[1].GetComponent<RectTransform>().DOMove(players._players[num].pController.pElements.cardMainPanel_2_FirstPos.transform.position, 0.5f).SetDelay(0.1f * (float)n + 0.3f);
                            players._players[num].pController.pElements.cardMainPanel[1].GetComponent<RectTransform>().DOScale(Vector3.one, 0.5f).SetDelay(0.1f * (float)n + 0.3f);
                            players._players[num].pController.pElements.cardMainPanel[1].GetComponent<RectTransform>().DORotate(Vector3.zero, 0.5f).SetDelay(0.1f * (float)n + 0.3f);
                            players._players[num].pController.pElements.cardMainPanel[2].GetComponent<RectTransform>().DOMove(players._players[num].pController.pElements.cardMainPanel_3_FirstPos.transform.position, 0.5f).SetDelay(0.1f * (float)n + 0.6f);
                            players._players[num].pController.pElements.cardMainPanel[2].GetComponent<RectTransform>().DOScale(Vector3.one, 0.5f).SetDelay(0.1f * (float)n + 0.6f);
                            players._players[num].pController.pElements.cardMainPanel[2].GetComponent<RectTransform>().DORotate(Vector3.zero, 0.5f).SetDelay(0.1f * (float)n + 0.6f);
                        }
                        players._players[num].pController.pElements.UpdateDetailsUI(socketIOEvent, n, true);
                        players._players[num].pController.gameStartsState.GameStarts();
                        if (socketIOEvent.data.GetField("response")[n].HasField("totalAmount") && socketIOEvent.data.GetField("response")[n].HasField("amount"))
                        {
                            players._players[num].pController.pElements.MoveChips(players._players[num].pController.pElements.bootAmountTransform.gameObject, tableElements.totalAmountOnTableGO, long.Parse(tableResponseData.response[n].totalAmount), long.Parse(tableResponseData.response[n].amount));
                        }
                        if (players._players[num].pController.pElements.isActive)
                        {
                            players._players[num].pController.waitForMyTurnState.WaitForMyTurn();
                        }
                        if (players._players[num].pController.pElements.isLocalPlayer)
                        {
                            players._players[num].pController.pElements.viewCardButton.gameObject.SetActive(value: true);
                            players._players[num].pController.pElements.viewCardButton.gameObject.GetComponent<RectTransform>().localScale = Vector3.zero;
                            players._players[num].pController.pElements.viewCardButton.gameObject.GetComponent<RectTransform>().DOScale(Vector3.one, 0.5f).SetDelay(2f);
                            isNotDelay = false;
                        }
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "send_message")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            for (int num3 = 0; num3 < tableResponseData.response.Length; num3++)
            {
                for (int num4 = 0; num4 < players._players.Length; num4++)
                {
                    if (players._players[num4].pController.pElements.playerID == tableResponseData.response[num3].playerId)
                    {
                        if (players._players[num4].pController.pElements.isLocalPlayer)
                        {
                            chatElement.CreateChat(tableResponseData.response[num3].messages, true);
                        }
                        else
                        {
                            chatElement.CreateChat(tableResponseData.response[num3].messages, false);
                        }

                        players._players[num4].pController.pElements.DisplayChatHead(tableResponseData.response[num3].messages);
						if (players._players[num4].pController.pElements.isLocalPlayer)
						{
							tableElements.CloseChatCanvas();
							tableElements.chatInputField.text = string.Empty;
						}
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "user_turn")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            if (IsInvoking("SideEffect"))
            {
                CancelInvoke("SideEffect");
            }
            firstPos = null;
            secPos = null;
            tableElements.sideShowPanelGO.SetActive(value: false);
            AudioManager.instance.StopSound("timer-tick-tok");
            for (int num5 = 0; num5 < tableResponseData.response.Length; num5++)
            {
                for (int num6 = 0; num6 < players._players.Length; num6++)
                {
                    if (!(players._players[num6].pController.pElements.position == tableResponseData.response[num5].position))
                    {
                        continue;
                    }
                    players._players[num6].pController.pElements.timerColorSliderImage.color = players._players[num6].pController.pElements.normalColor;
                    tableElements.ResetAmountMultiplier(long.Parse(tableResponseData.response[num5].amount));
                    players._players[num6].pController.pElements.UpdateDetailsUI(socketIOEvent, num5, _updateChipsText: true);
                    if (players._players[num6].pController.pElements.isLocalPlayer)
                    {
                        AudioManager.instance.PlaySound("local-player-turn");
                        AudioManager.instance.PlayVibration();
                        if (players._players[num6].pController.pElements.isActive)
                        {
                            tableElements.CloseSideShowPanel();
                        }
                        if (socketIOEvent.data.GetField("response")[num5].HasField("amount") && socketIOEvent.data.GetField("response")[num5].HasField("chips"))
                        {
                            /*if (tableElements.packButtonToggle.isOn)
							{
								tableElements.packButtonToggle.isOn = false;
								User_Pack();
								return;
							}*/
                            if (long.Parse(tableResponseData.response[num5].amount) <= long.Parse(tableResponseData.response[num5].chips))
                            {
                                /*if (tableElements.showButtonToggle.isOn)
								{
									tableElements.showButtonToggle.isOn = false;
									User_Show();
									return;
								}
								if (tableElements.chaalButtonToggle.isOn)
								{
									tableElements.chaalButtonToggle.isOn = false;
									Play_Chaal();
									return;
								}*/
                                /*if (long.Parse(players._players[num6].pController.pElements.forceSideShow) >= tableElements.forceDeduct)
								{
									tableElements.forcesideshowButton.interactable = true;
									tableElements.forcesideshowButtonEvent.enabled = true;
								}
								else
								{
									tableElements.forcesideshowButton.interactable = false;
									tableElements.forcesideshowButtonEvent.enabled = false;
								}*/
                            }
                        }
                        players._players[num6].pController.myTurnState.MyTurn();
                    }
                    else
                    {
                        AudioManager.instance.PlaySound("other-player-turn");
                    }
                    players._players[num6].pController.myTurnState.StartTimer();
                }
            }
        }
        else if (tableResponseData.responseMessage == "play_chaal")
        {
            StartCoroutine(delayInResponsesCO(1f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            tableElements.DisableLoadingButtons();
            AudioManager.instance.StopSound("timer-tick-tok");
            GameManager.Instance.LogMessage(googleAnalyticsString);
            for (int num7 = 0; num7 < tableResponseData.response.Length; num7++)
            {
                for (int num8 = 0; num8 < players._players.Length; num8++)
                {
                    if (players._players[num8].pController.pElements.position == tableResponseData.response[num7].position)
                    {
                        players._players[num8].pController.pElements.UpdateDetailsUI(socketIOEvent, num7, _updateChipsText: true);
                        if ((bool)socketIOEvent.data.GetField("response")[num7].GetField("totalAmount") && (bool)socketIOEvent.data.GetField("response")[num7].GetField("amount"))
                        {
                            players._players[num8].pController.pElements.MoveChips(players._players[num8].pController.pElements.bootAmountTransform.gameObject, tableElements.totalAmountOnTableGO, long.Parse(tableResponseData.response[num7].totalAmount), long.Parse(tableResponseData.response[num7].amount));
                        }
                        players._players[num8].pController.pElements.AssignTurnAmount(socketIOEvent, num7, _rejoin: false, _preAmount: false);
                        players._players[num8].pController.waitForMyTurnState.StopTimer();
                        if (players._players[num8].pController.pElements.isLocalPlayer)
                        {
                            players._players[num8].pController.waitForMyTurnState.WaitForMyTurn();
                        }
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "view_cards")
        {
            StartCoroutine(delayInResponsesCO(0f));

            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            for (int num9 = 0; num9 < tableResponseData.response.Length; num9++)
            {
                for (int num10 = 0; num10 < players._players.Length; num10++)
                {
                    if (!(players._players[num10].pController.pElements.position == tableResponseData.response[num9].position))
                    {
                        continue;
                    }
                    players._players[num10].pController.pElements.UpdateDetailsUI(socketIOEvent, num9, _updateChipsText: true);
                    players._players[num10].pController.pElements.AssignCards(socketIOEvent, num9, _autoView: true, _playCardAnimation: true);
                    players._players[num10].pController.pElements.viewCardButton.gameObject.SetActive(value: false);
                    if (!players._players[num10].pController.pElements.isLocalPlayer)
                    {
                        //players._players[num10].pController.pElements.seenGO.transform.localScale = Vector3.zero;
                        //players._players[num10].pController.pElements.seenGO.transform.rotation = Quaternion.Euler(0f, 0f, 180f);
                        //players._players[num10].pController.pElements.seenGO.SetActive(value: true);
                        //players._players[num10].pController.pElements.seenGO.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBack);
                        //players._players[num10].pController.pElements.seenGO.transform.DORotate(Vector3.zero, 0.5f).SetEase(Ease.OutBack);
                        players._players[num10].pController.pElements.cardStatus.text = "Card Seen";
                    }
                    else
                    {
                        players._players[num10].pController.pElements.cardStatus.text = "Card Seen";
                    }
                    if (!players._players[num10].pController.pElements.isLocalPlayer)
                    {
                        continue;
                    }
                    GameManager.Instance.LanguageConverter(tableElements.chaalButtonText, "Chaal");
                    //GameManager.Instance.LanguageConverter(tableElements.chaalToggleText, "Chaal");
                    if (socketIOEvent.data.GetField("response")[num9].HasField("amount") && socketIOEvent.data.GetField("response")[num9].HasField("chips") && socketIOEvent.data.GetField("response")[num9].HasField("forcesideshow"))
                    {
                        if (long.Parse(tableResponseData.response[num9].amount) > long.Parse(tableResponseData.response[num9].chips))
                        {
                            tableElements.increaseButton.interactable = false;
                            tableElements.decreaseButton.interactable = false;
                            tableElements.chaalButton.interactable = false;
                            tableElements.chaalButtonEvent.enabled = false;
                            tableElements.showButton.interactable = false;
                            tableElements.showButtonEvent.enabled = false;
                            //tableElements.forcesideshowButton.interactable = false;
                            //tableElements.forcesideshowButtonEvent.enabled = false;
                        }
                        else
                        {
                            if (long.Parse(tableResponseData.response[num9].amount) < tableElements.maxChaalLimit)
                            {
                                tableElements.increaseButton.interactable = true;
                            }
                            else
                            {
                                tableElements.increaseButton.interactable = false;
                            }
                            tableElements.decreaseButton.interactable = false;
                            /*if (long.Parse(tableResponseData.response[num9].forcesideshow) >= long.Parse(tableResponseData.response[num9].forceDeduct) && long.Parse(tableResponseData.response[num9].amount) <= long.Parse(tableResponseData.response[num9].chips))
							{
								tableElements.forcesideshowButton.interactable = true;
								tableElements.forcesideshowButtonEvent.enabled = true;
							}
							else
							{
								tableElements.forcesideshowButton.interactable = false;
								tableElements.forcesideshowButtonEvent.enabled = false;
							}*/
                        }
                    }
                    tableElements.amountMultiplier = 1L;
                }
            }
        }
        else if (tableResponseData.responseMessage == "view_cards_var")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            for (int num11 = 0; num11 < tableResponseData.response.Length; num11++)
            {
                for (int num12 = 0; num12 < players._players.Length; num12++)
                {
                    if (players._players[num12].pController.pElements.position == tableResponseData.response[num11].position)
                    {
                        players._players[num12].pController.pElements.AssignCardsAfterDelay(socketIOEvent, num11, _autoView: true, _playCardAnimation: false);
                        players._players[num12].pController.pElements.viewCardButton.gameObject.SetActive(value: false);
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "side_show")
        {
            StartCoroutine(delayInResponsesCO(2f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            AudioManager.instance.PlaySound("side-show-request");
            tableElements.DisableLoadingButtons();
            for (int num13 = 0; num13 < tableResponseData.response.Length; num13++)
            {
                for (int num14 = 0; num14 < players._players.Length; num14++)
                {
                    if (!(players._players[num14].pController.pElements.position == tableResponseData.response[num13].position))
                    {
                        continue;
                    }
                    switch (num13)
                    {
                        case 0:
                            players._players[num14].pController.sideShowState.SideShowSender();
                            players._players[num14].pController.pElements.UpdateDetailsUI(socketIOEvent, num13, _updateChipsText: true);
                            players._players[num14].pController.pElements.MoveChips(players._players[num14].pController.pElements.bootAmountTransform.gameObject, tableElements.totalAmountOnTableGO, long.Parse(tableResponseData.response[num13].totalAmount), long.Parse(tableResponseData.response[num13].amount));
                            players._players[num14].pController.pElements.AssignTurnAmount(socketIOEvent, num13, _rejoin: false, _preAmount: false);
                            firstPos = players._players[num14].pController.pElements.transform.gameObject;
                            break;
                        case 1:
                            if (players._players[num14].pController.pElements.isLocalPlayer)
                            {
                                players._players[num14].pController.sideShowState.SideShowReceiver();
                                tableElements.isSideShowSliderOn = true;
                                tableElements.acceptSideShowText.text = "Accept Sideshow Request From " + URL.Instance.TrimNameForSideshow(tableResponseData.response[0].name, tableResponseData.response[0].facebook, tableResponseData.response[0].tempname);
                            }
                            secPos = players._players[num14].pController.pElements.cardsGO.transform.gameObject;
                            break;
                    }
                }
            }
            tableElements.userSideShowGO.transform.position = firstPos.transform.position;
            tableElements.userSideShowGO.transform.localScale = Vector3.one;
            tableElements.userSideShowGO.SetActive(value: true);
            tableElements.userSideShowGO.transform.DOMove(secPos.transform.position, 1f).SetEase(Ease.InOutQuad).OnComplete(delegate
            {
                tableElements.userSideShowGO.transform.DOShakeScale(1f).OnComplete(delegate
                {
                    tableElements.userSideShowGO.transform.DOScale(0f, 0.5f);
                });
            });
            InvokeRepeating("SideEffect", 0f, 0.1f);
        }
        else if (tableResponseData.responseMessage == "side_show_accept")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (tableResponseData.status == "success")
            {
                AudioManager.instance.PlaySound("side-show-accept");
                tableElements.DisableLoadingButtons();
                if (IsInvoking("SideEffect"))
                {
                    CancelInvoke("SideEffect");
                }
                for (int num15 = 0; num15 < tableResponseData.response.Length; num15++)
                {
                    for (int num16 = 0; num16 < players._players.Length; num16++)
                    {
                        if (players._players[num16].pController.pElements.position == tableResponseData.response[num15].position)
                        {
                            if (num15 == 0)
                            {
                                firPosInt = num16;
                                firstPos = players._players[num16].pController.pElements.transform.gameObject;
                            }
                            if (num15 == 1)
                            {
                                players._players[num16].pController.sideShowState.SideShowAccept();
                                secPosInt = num16;
                                secPos = players._players[num16].pController.pElements.transform.gameObject;
                            }
                        }
                        if (tableResponseData.response[0].playerId == players._players[num16].pController.pElements.playerID && players._players[num16].pController.pElements.isLocalPlayer)
                        {
                            for (int num17 = 0; num17 < players._players.Length; num17++)
                            {
                                if (tableResponseData.response[1].playerId == players._players[num17].pController.pElements.playerID)
                                {
                                    players._players[num17].pController.pElements.AssignCards(socketIOEvent, 1, _autoView: true, _playCardAnimation: false);
                                }
                            }
                        }
                        if (!(tableResponseData.response[1].playerId == players._players[num16].pController.pElements.playerID) || !players._players[num16].pController.pElements.isLocalPlayer)
                        {
                            continue;
                        }
                        for (int num18 = 0; num18 < players._players.Length; num18++)
                        {
                            if (tableResponseData.response[0].playerId == players._players[num18].pController.pElements.playerID)
                            {
                                players._players[num18].pController.pElements.AssignCards(socketIOEvent, 0, _autoView: true, _playCardAnimation: false);
                            }
                        }
                    }
                }
            }
            tableElements.isSideShowSliderOn = false;
        }
        else if (tableResponseData.responseMessage == "side_show_winner")
        {
            StartCoroutine(delayInResponsesCO(2f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            tableElements.DisableLoadingButtons();
            AudioManager.instance.PlaySound("side-show-winner");
            if (IsInvoking("SideEffect"))
            {
                CancelInvoke("SideEffect");
            }
            for (int num19 = 0; num19 < tableResponseData.response.Length; num19++)
            {
                for (int num20 = 0; num20 < players._players.Length; num20++)
                {
                    if (players._players[num20].pController.pElements.position == tableResponseData.response[num19].position)
                    {
                        for (int num21 = 0; num21 < players._players[num20].pController.pElements.cardMainPanel.Length; num21++)
                        {
                            //players._players[num20].pController.pElements.EnablePlayerGlow();
                            Sequence s = DOTween.Sequence();
                            s.PrependInterval(0.5f);
                            s.AppendInterval(0.1f * (float)num21).Append(players._players[num20].pController.pElements.cardMainPanel[num21].transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f)).Append(players._players[num20].pController.pElements.cardMainPanel[num21].transform.DOScale(Vector3.one, 0.5f));
                        }
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "side_show_decline")
        {
            StartCoroutine(delayInResponsesCO(0f));
            tableElements.DisableLoadingButtons();
            AudioManager.instance.PlaySound("side-show-decline");
            if (IsInvoking("SideEffect"))
            {
                CancelInvoke("SideEffect");
            }
            if (tableResponseData.status == "success")
            {
                for (int num22 = 0; num22 < tableResponseData.response.Length; num22++)
                {
                    for (int num23 = 0; num23 < players._players.Length; num23++)
                    {
                        if (players._players[num23].pController.pElements.position == tableResponseData.response[num22].position)
                        {
                            players._players[num23].pController.sideShowState.SideShowDecline();
                        }
                    }
                }
            }
            tableElements.isSideShowSliderOn = false;
        }

        else if (tableResponseData.responseMessage == "pack")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            tableElements.DisableLoadingButtons();
            AudioManager.instance.StopSound("timer-tick-tok");
            AudioManager.instance.PlaySound("pack");
            for (int num31 = 0; num31 < tableResponseData.response.Length; num31++)
            {
                for (int num32 = 0; num32 < players._players.Length; num32++)
                {
                    if (players._players[num32].pController.pElements.position == tableResponseData.response[num31].position)
                    {
                        players._players[num32].pController.pElements.UpdateDetailsUI(socketIOEvent, num31, _updateChipsText: true);
                        players._players[num32].pController.packState.UserPack();
                        if (players._players[num32].pController.pElements.isLocalPlayer)
                        {
                            isPack = true;
                            tableElements.messageText.text = string.Empty;
                        }
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "pot_limit_over")
        {
            StartCoroutine(delayInResponsesCO(2f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            for (int num33 = 0; num33 < tableResponseData.response.Length; num33++)
            {
                for (int num34 = 0; num34 < players._players.Length; num34++)
                {
                    if (players._players[num34].pController.pElements.position == tableResponseData.response[num33].position)
                    {
                        players._players[num34].pController.pElements.UpdateDetailsUI(socketIOEvent, num33, _updateChipsText: true);
                        players._players[num34].pController.pElements.AssignCards(socketIOEvent, num33, _autoView: true, _playCardAnimation: false);
                        tableElements.tableMessageText.text = tableResponseData.popupMessageBody;
                        players._players[num34].pController.potLimitOverState.PotLimitOver();
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "send_items")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            for (int num35 = 0; num35 < players._players.Length; num35++)
            {
                if (players._players[num35].pController.pElements.playerID == tableResponseData.response[0].firstPerson)
                {
                    firstGiftPos = players._players[num35].pController.pElements.gameObject;
                    if (players._players[num35].pController.pElements.isLocalPlayer)
                    {
                        tableElements.CloseGiftPopup();
                        //tableElements.CloseEmojiPopup();
                    }
                }
                if (players._players[num35].pController.pElements.playerID == tableResponseData.response[0].secondPerson)
                {
                    secGiftPos = players._players[num35].pController.pElements.gameObject;
                    players._players[num35].pController.pElements.DisableAndEnableGiftIcon();
                }
            }
            /*if (tableResponseData.response[0].type == "local")
			{
				StartCoroutine(DownloadAssetBundle(tableResponseData.response[0].itemNumber));
				return;
			}*/
            GameObject items = Instantiate(giftElement.giftButtonList[int.Parse(tableResponseData.response[0].itemNumber)]);
            items.transform.localScale = Vector3.zero;
            items.transform.position = firstGiftPos.transform.position;
            items.transform.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutSine).OnComplete(delegate
            {
                items.transform.DOMove(secGiftPos.transform.position, 1f).OnComplete(delegate
                {
                    InstantiateGiftsAnimation component = items.GetComponent<InstantiateGiftsAnimation>();
                    GameObject itemsAnims = Instantiate(component.giftAnimation);
                    itemsAnims.transform.position = items.transform.position;
                    Destroy(items);
                    itemsAnims.transform.DOScale(Vector3.zero, 0.3f).SetDelay(4f).SetEase(Ease.OutSine)
                        .OnComplete(delegate
                        {
                            Destroy(itemsAnims);
                        });
                });
            });
        }
        else if (tableResponseData.responseMessage == "select_variations")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            for (int num36 = 0; num36 < tableResponseData.response.Length; num36++)
            {
                for (int num37 = 0; num37 < players._players.Length; num37++)
                {
                    if (players._players[num37].pController.pElements.position == tableResponseData.response[num36].position)
                    {
                        if (players._players[num37].pController.pElements.isLocalPlayer)
                        {
                            tableElements.OpenSelectVariationCanvas();
                        }
                        else
                        {
                            players._players[num37].pController.pElements.selectingVariationGO.SetActive(value: true);
                        }
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "set_variations")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (tableResponseData.status == "success")
            {
                tableElements.CloseSelectVariationCanvas();
                tableElements.variationTableCodeGO.SetActive(value: true);
                SetVariationText(tableElements.variationTableCodeText, tableResponseData.response[0].var_type);
                tableElements.isVariation = false;
                for (int num38 = 0; num38 < players._players.Length; num38++)
                {
                    players._players[num38].pController.pElements.selectingVariationGO.SetActive(value: false);
                }
            }
        }
        else if (tableResponseData.responseMessage == "user_show")
        {
            StartCoroutine(delayInResponsesCO(2f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            tableElements.DisableLoadingButtons();
            if (IsInvoking("SideEffect"))
            {
                CancelInvoke("SideEffect");
            }
            AudioManager.instance.StopSound("timer-tick-tok");
            for (int num39 = 0; num39 < tableResponseData.response.Length; num39++)
            {
                for (int num40 = 0; num40 < players._players.Length; num40++)
                {
                    if (players._players[num40].pController.pElements.position == tableResponseData.response[num39].position)
                    {
                        players._players[num40].pController.pElements.UpdateDetailsUI(socketIOEvent, num39, _updateChipsText: true);
                        players._players[num40].pController.pElements.AssignCards(socketIOEvent, num39, _autoView: false, _playCardAnimation: false);
                        players._players[num40].pController.userShowState.UserShow();
                        if (num39 == 0)
                        {
                            players._players[num40].pController.pElements.MoveChips(players._players[num40].pController.pElements.bootAmountTransform.gameObject, tableElements.totalAmountOnTableGO, long.Parse(tableResponseData.response[num39].totalAmount), long.Parse(tableResponseData.response[num39].amount));
                            players._players[num40].pController.pElements.AssignTurnAmount(socketIOEvent, num39, _rejoin: false, _preAmount: false);
                            firstPos = players._players[num40].pController.pElements.bootAmountTransform.gameObject;
                            firPosInt = num40;
                        }
                        if (num39 == 1)
                        {
                            secPos = players._players[num40].pController.pElements.cardsGO.gameObject;
                            secPosInt = num40;
                        }
                    }
                }
            }
            tableElements.userShowGO.transform.position = firstPos.transform.position;
            tableElements.userShowGO.transform.localScale = Vector3.one;
            tableElements.userShowGO.SetActive(value: true);
            tableElements.userShowGO.transform.DOMove(secPos.transform.position, 1f).SetEase(Ease.InOutQuad).OnComplete(delegate
            {
                tableElements.userShowGO.transform.DOShakeScale(1f).OnComplete(delegate
                {
                    tableElements.userShowGO.transform.DOScale(0f, 0.5f);
                });
                AutoCardViewInShow();
            });
        }
        else if (tableResponseData.responseMessage == "user_winner_card")
        {
            StartCoroutine(delayInResponsesCO(2f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            if (IsInvoking("SideEffect"))
            {
                CancelInvoke("SideEffect");
            }
            StartRecording();
            for (int num41 = 0; num41 < tableResponseData.response.Length; num41++)
            {
                for (int num42 = 0; num42 < players._players.Length; num42++)
                {
                    if (players._players[num42].pController.pElements.position == tableResponseData.response[num41].position)
                    {
                        players._players[num42].pController.pElements.UpdateDetailsUI(socketIOEvent, num41, _updateChipsText: false);
                        players._players[num42].pController.pElements.Winner_Card(socketIOEvent, num41);
                        for (int num43 = 0; num43 < players._players[num42].pController.pElements.cardMainPanel.Length; num43++)
                        {
                            Sequence s3 = DOTween.Sequence();
                            s3.AppendInterval(0.1f * (float)num43).Append(players._players[num42].pController.pElements.cardMainPanel[num43].transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f)).Append(players._players[num42].pController.pElements.cardMainPanel[num43].transform.DOScale(Vector3.one, 0.5f));
                        }
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "user_winner")
        {
            StartCoroutine(delayInResponsesCO(2f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            if (IsInvoking("SideEffect"))
            {
                CancelInvoke("SideEffect");
            }
            for (int num44 = 0; num44 < tableResponseData.response.Length; num44++)
            {
                for (int num45 = 0; num45 < players._players.Length; num45++)
                {
                    if (players._players[num45].pController.pElements.position == tableResponseData.response[num44].position)
                    {
                        players._players[num45].pController.pElements.UpdateDetailsUI(socketIOEvent, num44, _updateChipsText: false);
                        players._players[num45].pController.gameWinState.Winner();
                        if (socketIOEvent.data.GetField("response")[num44].HasField("totalAmount"))
                        {
                            tableElements.GetTotalAmountOnTable(long.Parse(tableResponseData.response[num44].totalAmount));
                        }
                        GameObject tableTotalAmountGO = Instantiate(tableElements.tableTotalAmountGO, tableElements.totalAmountOnTableGO.transform.position, Quaternion.identity);
                        TextMeshProUGUI componentInChildren = tableTotalAmountGO.GetComponentInChildren<TextMeshProUGUI>();
                        componentInChildren.text = GameManager.Instance.format.FormatNumber(tableElements.totalAmountOnTable / tableResponseData.response.Length);
                        tableElements.totalAmountOnTableGO.SetActive(value: false);
                        long prevChips = 0L;
                        long newChips = 0L;
                        if (socketIOEvent.data.GetField("response")[num44].HasField("chips"))
                        {
                            prevChips = long.Parse(players._players[num45].pController.pElements.previousChips);
                            newChips = long.Parse(players._players[num45].pController.pElements.chips);
                        }
                        Text tempChipsText = players._players[num45].pController.pElements.chipsText;
                        tableTotalAmountGO.gameObject.transform.DOMove(players._players[num45].pController.pElements.bootAmountTransform.transform.position, 1f).SetDelay(1f).SetEase(Ease.InOutQuad)
                            .OnComplete(delegate
                            {
                                if (prevChips.ToString() != string.Empty && prevChips != 0 && newChips.ToString() != string.Empty && newChips != 0)
                                {
                                    GameManager.Instance.CountChipsSinglePlayer(prevChips, newChips, tempChipsText);
                                }
                                tableTotalAmountGO.transform.localScale = Vector3.zero;
                                UnityEngine.Object.Destroy(tableTotalAmountGO, 2f);
                            });
                        if (players._players[num45].pController.pElements.isLocalPlayer)
                        {
                            AudioManager.instance.PlaySound("player-winner");
                        }
                        else
                        {
                            AudioManager.instance.PlaySound("player-winner-other");
                        }
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "OLC_winning_amount")
        {
            StartCoroutine(delayInResponsesCO(2f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            if (IsInvoking("SideEffect"))
            {
                CancelInvoke("SideEffect");
            }
            for (int num46 = 0; num46 < tableResponseData.response.Length; num46++)
            {
                for (int num47 = 0; num47 < players._players.Length; num47++)
                {
                    if (tableResponseData.response[num46].playerId == players._players[num47].pController.pElements.playerID)
                    {
                        if (players._players[num47].pController.pElements.isLocalPlayer)
                        {
                            tableElements.tableMessageText.text = "You've won " + GameManager.Instance.format.FormatNumber(long.Parse(tableResponseData.response[num46].amount));
                        }
                        else
                        {
                            tableElements.tableMessageText.text = URL.Instance.TrimNameForOthers(tableResponseData.response[num46].name, bool.Parse(tableResponseData.response[num46].facebook), tableResponseData.response[num46].tempname) + " won " + GameManager.Instance.format.FormatNumber(long.Parse(tableResponseData.response[num46].amount));
                        }
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "game_finished")
        {
            StartCoroutine(delayInResponsesCO(5f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            if (IsInvoking("SideEffect"))
            {
                CancelInvoke("SideEffect");
            }
            activeUsersForMessages = tableResponseData.response.Length;
            for (int num48 = 0; num48 < tableResponseData.response.Length; num48++)
            {
                for (int num49 = 0; num49 < players._players.Length; num49++)
                {
                    if (players._players[num49].pController.pElements.position == tableResponseData.response[num48].position)
                    {
                        players._players[num49].pController.pElements.UpdateDetailsUI(socketIOEvent, num48, _updateChipsText: true);
                        players._players[num49].pController.gameFinishedState.GameFinished();
                        if (activeUsersForMessages > 1)
                        {
                            GameManager.Instance.LanguageConverter(tableElements.messageText, "Next Game Starts Shortly...");
                        }
                        else
                        {
                            GameManager.Instance.LanguageConverter(tableElements.messageText, "Waiting for other players...");
                        }
                        if (isPack)
                        {
                            isPack = false;
                            GameManager.Instance.LanguageConverter(tableElements.messageText, "Wait until the next game starts...");
                        }
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "outoff_chips")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (!(tableResponseData.status == "success") || !(tableResponseData.response[0].playerId == GameManager.Instance.localPlayerID))
            {
                return;
            }
            if (IsInvoking("SideEffect"))
            {
                CancelInvoke("SideEffect");
            }
            AudioManager.instance.StopSound("timer-tick-tok");
            GameManager.Instance.isSocketConnectedToServer = false;
            if (socketIOEvent.data.GetField("response")[0].HasField("MMmode"))
            {
                if (tableResponseData.response[0].MMmode == 1)
                {
                    SceneManager.LoadScene(URL.Instance.LOADING_SCENE);
                }
                else
                {
                    SceneManager.LoadScene(URL.Instance.MAIN_MENU_SCENE);
                }
            }
            else
            {
                SceneManager.LoadScene(URL.Instance.MAIN_MENU_SCENE);
            }
        }
        else if (tableResponseData.responseMessage == "leave_table")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (!(tableResponseData.status == "success"))
            {
                return;
            }
            if (tableResponseData.response[0].playerId == GameManager.Instance.localPlayerID)
            {
                if (IsInvoking("SideEffect"))
                {
                    CancelInvoke("SideEffect");
                }
                AudioManager.instance.StopSound("timer-tick-tok");
                if (!GameManager.Instance.isSwitchingTable)
                {
                    GameManager.Instance.isSocketConnectedToServer = false;
                    if (socketIOEvent.data.GetField("response")[0].HasField("MMmode"))
                    {
                        if (tableResponseData.response[0].MMmode == 1)
                        {
                            SceneManager.LoadScene(URL.Instance.LOADING_SCENE);
                        }
                        else
                        {
                            SceneManager.LoadScene(URL.Instance.MAIN_MENU_SCENE);
                        }
                    }
                    else
                    {
                        SceneManager.LoadScene(URL.Instance.MAIN_MENU_SCENE);
                    }
                    if (tableResponseData.popupMessageTitle == "Booted Out!")
                    {
                        GameManager.Instance.isBootedOut = true;
                        GameManager.Instance.bootedOutTitle = "Booted Out";
                        GameManager.Instance.bootedOutBody = tableResponseData.popupMessageBody;
                    }
                    else
                    {
                        GameManager.Instance.isBootedOut = false;
                        GameManager.Instance.bootedOutTitle = string.Empty;
                        GameManager.Instance.bootedOutBody = string.Empty;
                    }
                }
                else
                {
                    for (int num50 = 0; num50 < players._players.Length; num50++)
                    {
                        players._players[num50].pController.notSittingState.NotSitting(_isFirstTime: true);
                    }
                    GameManager.Instance.isSwitchingTable = false;
                    tableElements.DisableEmojis();
                    tableElements.userShowGO.transform.localScale = Vector3.zero;
                    //tableElements.userForceSideShowGO.transform.localScale = Vector3.zero;
                    tableElements.userSideShowGO.transform.localScale = Vector3.zero;
                }
                tableElements.ResetTableElements();
                tableElements.OnSwitchButton(4f);
                tableElements.OffSwitchButton();
                return;
            }
            for (int num51 = 0; num51 < tableResponseData.response.Length; num51++)
            {
                for (int num52 = 0; num52 < players._players.Length; num52++)
                {
                    if (players._players[num52].pController.pElements.position == tableResponseData.response[num51].position)
                    {
                        players._players[num52].pController.notSittingState.NotSitting(_isFirstTime: false);
                    }
                }
            }
        }
        else if (tableResponseData.responseMessage == "active_users_on_table")
        {
            StartCoroutine(delayInResponsesCO(0f));
            if (tableResponseData.status == "success")
            {
                activeUsers = tableResponseData.response.Length;
                URL.Instance.DebugPrint("Active Users : " + activeUsers);
                ResetSideShow();
                CheckSideButtons(data);
            }
        }

    }

    private void AutoCardViewInForce()
    {
        if (players._players[secPosInt].pController.pElements.isLocalPlayer)
        {
            players._players[firPosInt].pController.pElements.AutoViewCards();
        }
        if (players._players[firPosInt].pController.pElements.isLocalPlayer)
        {
            players._players[secPosInt].pController.pElements.AutoViewCards();
        }
    }

    private void AutoCardViewInShow()
    {
        players._players[firPosInt].pController.pElements.AutoViewCards();
        players._players[secPosInt].pController.pElements.AutoViewCards();
    }

    public void Send_Chat(string msg)
    {
        JSONObject jSONObject = new JSONObject();
        jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
        jSONObject.AddField("messages", msg);

        socket.Emit("send_message", jSONObject);
    }

    /*public void Send_Chat(TextMeshProUGUI chatText)
	{
		ChaatJSON obj = new ChaatJSON(GameManager.Instance.localPlayerID, chatText.text);
		string plainText = JsonUtility.ToJson(obj);
		string val = GameManager.Instance.security.Encrypt(plainText);
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("data", val);
		socket.Emit("send_message", jSONObject);
	}

	public void Send_Chat_Input()
	{
		if (tableElements.chatInputField.text.Length > 0)
		{
			ChaatJSON obj = new ChaatJSON(GameManager.Instance.localPlayerID, tableElements.chatInputField.text);
			string plainText = JsonUtility.ToJson(obj);
			string val = GameManager.Instance.security.Encrypt(plainText);
			JSONObject jSONObject = new JSONObject();
			jSONObject.AddField("data", val);
			socket.Emit("send_message", jSONObject);
		}
	}*/

    public void Play_Chaal()
    {
        AudioManager.instance.PlaySound("click");
        tableElements.EnableLoadingButtons(true, true, false, false, false);

        JSONObject jSONObject = new JSONObject();
        jSONObject.AddField("multiplier", tableElements.amountMultiplier.ToString());

        socket.Emit("play_chaal", jSONObject);
    }

    public void User_Pack()
    {
        JSONObject newData = new JSONObject();
        newData.AddField("tbid", GameManager.Instance.tbid);
        newData.AddField("playerId", GameManager.Instance.localPlayerID);
        tableElements.EnableLoadingButtons(true, false, false, true, false);
        socket.Emit("pack",newData);
    }

    public void User_Show()
    {
        JSONObject newData = new JSONObject();
        newData.AddField("tbid", GameManager.Instance.tbid);
        newData.AddField("playerId", GameManager.Instance.localPlayerID);
        AudioManager.instance.PlaySound("click");
        tableElements.EnableLoadingButtons(true, false, true, false, false);
        if (activeUsers > 2)
        {
            socket.Emit("side_show",newData);
        }
        else
        {
            socket.Emit("user_show", newData);
        }
    }

    public void Force_Side_show()
    {
        JSONObject newData = new JSONObject();
        newData.AddField("tbid", GameManager.Instance.tbid);
        newData.AddField("playerId", GameManager.Instance.localPlayerID);
        AudioManager.instance.PlaySound("click");
        tableElements.EnableLoadingButtons(true, false, false, false, true);
        if (activeUsers > 2)
        {
            socket.Emit("force_side_show",newData);
        }
    }

    public void Side_Show_Decline()
    {
        JSONObject newData = new JSONObject();
        newData.AddField("tbid", GameManager.Instance.tbid);
        newData.AddField("playerId", GameManager.Instance.localPlayerID);
        AudioManager.instance.PlaySound("click");
        socket.Emit("side_show_decline", newData);
    }

    public void Side_Show_Accept()
    {
        JSONObject newData = new JSONObject();
        newData.AddField("tbid", GameManager.Instance.tbid);
        newData.AddField("playerId", GameManager.Instance.localPlayerID);
        AudioManager.instance.PlaySound("click");
        socket.Emit("side_show_accept",newData);
    }

    public void View_Cards()
    {
        JSONObject newData = new JSONObject();
        newData.AddField("tbid", GameManager.Instance.tbid);
        newData.AddField("playerId", GameManager.Instance.localPlayerID);
        AudioManager.instance.PlaySound("click");
        URL.Instance.DebugPrint("View Card Button Pressed");
        socket.Emit("view_cards",newData);
        players._players[2].pController.pElements.viewCardButton.gameObject.SetActive(value: false);
    }

    public void Select_Variations(string _variationType)
    {
        AudioManager.instance.PlaySound("click");

        JSONObject jSONObject = new JSONObject();
        jSONObject.AddField("var_type", _variationType);

        socket.Emit("set_variations", jSONObject);
    }

    public void LeaveTable()
    {
        JSONObject newData = new JSONObject();
        newData.AddField("tbid", GameManager.Instance.tbid);
        newData.AddField("playerId", GameManager.Instance.localPlayerID);
        isNotDelay = true;
        AudioManager.instance.PlaySound("click");
        GameManager.Instance.loadingCanvas.LoadingState(true, false);
        socket.Emit("leave_table",newData);
    }

    public void SwitchTable()
    {
        AudioManager.instance.PlaySound("click");
        isNotDelay = true;
        GameManager.Instance.loadingCanvas.LoadingState(true, false);
        GameManager.Instance.isSwitchingTable = true;
        socket.Emit("switch_table");
    }

    private string ConvertPlayersPosition(string _position)
    {
        int num = 0;
        if (GameManager.Instance.localPlayerPosition == 0)
        {
            num = ((_position == "1") ? 3 : ((_position == "2") ? 4 : ((!(_position == "3")) ? ((_position == "4") ? 1 : 2) : 0)));
        }
        else if (GameManager.Instance.localPlayerPosition == 1)
        {
            num = ((_position == "0") ? 1 : ((_position == "2") ? 3 : ((_position == "3") ? 4 : ((!(_position == "4")) ? 2 : 0))));
        }
        else if (GameManager.Instance.localPlayerPosition == 2)
        {
            num = ((!(_position == "0")) ? ((_position == "1") ? 1 : ((_position == "3") ? 3 : ((!(_position == "4")) ? 2 : 4))) : 0);
        }
        else if (GameManager.Instance.localPlayerPosition == 3)
        {
            num = ((_position == "0") ? 4 : ((!(_position == "1")) ? ((_position == "2") ? 1 : ((!(_position == "4")) ? 2 : 3)) : 0));
        }
        else if (GameManager.Instance.localPlayerPosition == 4)
        {
            num = ((_position == "0") ? 3 : ((_position == "1") ? 4 : ((!(_position == "2")) ? ((_position == "3") ? 1 : 2) : 0)));
        }
        return num.ToString();
    }

    private void ResetSideShow()
    {
        if (apID.Count > 0)
        {
            apID.Clear();
        }
        if (auID.Count > 0)
        {
            auID.Clear();
        }
        user = false;
    }

    private void CheckSideButtons(string _data)
    {
        int num = 0;
        TableResponseData tableResponseData = TableResponseData.CreateFromJSON(_data);
        if (tableResponseData == null)
        {
            return;
        }
        for (int i = 0; i < tableResponseData.response.Length; i++)
        {
            if (tableResponseData.response[i].isBlind == "false")
            {
                if (num == 0)
                {
                    if (tableResponseData.response[i].position == GameManager.Instance.tempLocalPlayerPosition.ToString() && i != 0)
                    {
                        num = int.Parse(tableResponseData.response[i - 1].position);
                    }
                    if (tableResponseData.response[i].position == GameManager.Instance.tempLocalPlayerPosition.ToString() && i == 0)
                    {
                        num = int.Parse(tableResponseData.response[tableResponseData.response.Length - 1].position);
                    }
                }
                apID.Add(tableResponseData.response[i].playerId);
            }
            if (tableResponseData.response[i].isActive == "true")
            {
                auID.Add(tableResponseData.response[i].playerId);
                URL.Instance.DebugPrint("AP : " + tableResponseData.response[i].playerId);
            }
        }
        for (int j = 0; j < tableResponseData.response.Length; j++)
        {
            if (tableResponseData.response[j].position == num.ToString())
            {
                if (tableResponseData.response[j].isBlind == "false")
                {
                    user = true;
                }
                else
                {
                    user = false;
                }
            }
        }
        if (auID.Count == 2)
        {
            tableElements.showButton.gameObject.SetActive(value: true);
            GameManager.Instance.LanguageConverter(tableElements.showButton.GetComponentInChildren<Text>(), "Show");
            //tableElements.showButtonToggle.gameObject.SetActive(value: true);
            //GameManager.Instance.LanguageConverter(tableElements.showButtonToggle.GetComponentInChildren<Text>(), "Show");
            //tableElements.forcesideshowButton.gameObject.SetActive(value: false);
            return;
        }
        URL.Instance.DebugPrint("AUID : " + auID.Count);
        URL.Instance.DebugPrint("APID : " + apID.Count);
        URL.Instance.DebugPrint("User : " + user);
        if (apID.Count >= 2)
        {
            if (apID.Count >= 2 && user)
            {
                tableElements.showButton.gameObject.SetActive(value: true);
                GameManager.Instance.LanguageConverter(tableElements.showButton.GetComponentInChildren<Text>(), "S.Show");
                //tableElements.showButtonToggle.gameObject.SetActive(value: true);
                //GameManager.Instance.LanguageConverter(tableElements.showButtonToggle.GetComponentInChildren<Text>(), "S.Show");
                GameManager.Instance.LanguageConverter(tableElements.chaalButton.GetComponentInChildren<Text>(), "Chaal");
                //GameManager.Instance.LanguageConverter(tableElements.chaalButtonToggle.GetComponentInChildren<Text>(), "Chaal");
                //tableElements.forcesideshowButton.gameObject.SetActive(value: true);
            }
            else
            {
                tableElements.showButton.gameObject.SetActive(value: false);
                //tableElements.showButtonToggle.gameObject.SetActive(value: false);
                //tableElements.forcesideshowButton.gameObject.SetActive(value: false);
            }
        }
        else
        {
            tableElements.showButton.gameObject.SetActive(value: false);
            //tableElements.showButtonToggle.gameObject.SetActive(value: false);
            //tableElements.forcesideshowButton.gameObject.SetActive(value: false);
        }
    }

    private void SideEffect()
    {
        if (firstPos != null && secPos != null)
        {
            GameObject gameObject = UnityEngine.Object.Instantiate(sideshowEffect, firstPos.transform.position, firstPos.transform.rotation);
            gameObject.transform.localScale = Vector3.zero;
            StartCoroutine(SideShowMove(gameObject, firstPos.gameObject, secPos.gameObject));
        }
        else if (IsInvoking("SideEffect"))
        {
            CancelInvoke("SideEffect");
        }
    }

    private IEnumerator SideShowMove(GameObject moveObject, GameObject _firstPos, GameObject _secPos)
    {
        float timeSinceStarted = 0f;
        while (secPos != null && firstPos != null)
        {
            timeSinceStarted += Time.deltaTime * 0.5f;
            moveObject.transform.position = Vector3.Lerp(_firstPos.transform.position, _secPos.transform.position, Mathf.SmoothStep(0f, 1f, timeSinceStarted));
            moveObject.transform.localScale = Vector3.Lerp(Vector3.zero, new Vector3(0.7f, 0.7f, 0.7f), Mathf.SmoothStep(0f, 1f, timeSinceStarted));
            moveObject.GetComponent<CanvasGroup>().alpha = Mathf.Lerp(1f, 0.2f, Mathf.SmoothStep(0f, 1f, timeSinceStarted));
            if (moveObject.transform.position == _secPos.transform.position)
            {
                moveObject.transform.localScale = Vector3.one;
                UnityEngine.Object.Destroy(moveObject.gameObject);
                yield break;
            }
            yield return null;
        }
        UnityEngine.Object.Destroy(moveObject.gameObject);
    }

    public void SendGift(int index)
    {
        JSONObject jSONObject = new JSONObject();
        jSONObject.AddField("type", "nonlocal");
        jSONObject.AddField("firstPerson", GameManager.Instance.localPlayerID);
        jSONObject.AddField("secondPerson", GameManager.Instance.secondPlayerNameForGifts);
        jSONObject.AddField("itemNumber", index.ToString());

        socket.Emit("send_items", jSONObject);
        nonLocalButtons.DisableButtons();
    }

    /*public void SendLocalGiftItems(string _giftNumber)
	{
		GiftItemsJSON obj = new GiftItemsJSON("local", GameManager.Instance.localPlayerID, GameManager.Instance.secondPlayerNameForGifts, _giftNumber);
		string plainText = JsonUtility.ToJson(obj);
		string val = GameManager.Instance.security.Encrypt(plainText);
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("data", val);
		socket.Emit("send_items", jSONObject);
		localButtons.DisableButtons();
	}

	public void SendNonLocalGiftItems(int _giftNumber)
	{
		GiftItemsJSON obj = new GiftItemsJSON("nonlocal", GameManager.Instance.localPlayerID, GameManager.Instance.secondPlayerNameForGifts, _giftNumber.ToString());
		string plainText = JsonUtility.ToJson(obj);
		string val = GameManager.Instance.security.Encrypt(plainText);
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("data", val);
		socket.Emit("send_items", jSONObject);
		nonLocalButtons.DisableButtons();
	}*/

    private void SetVariationText(Text _text, string _varType)
    {
        if (_varType == "AK47")
        {
            _text.text = "AK47";
        }
        else if (_varType == "LowJoker")
        {
            _text.text = "Lowest Joker";
        }
        else if (_varType == "HighJoker")
        {
            _text.text = "Highest Joker";
        }
        else if (_varType == "muflis")
        {
            _text.text = "Muflis";
        }
    }

    private void Update()
    {
        time += Time.deltaTime;
        if (time >= interpolationPeriod)
        {
            time = 0f;
        }
        if (networkResponses.Count > 0 && !gameIsInBackground && canReadResponses)
        {
            canReadResponses = false;
            OnQueueResponse(networkResponses.Peek());
        }
    }

    public void OnApplicationPause(bool paused)
    {
        gameIsInBackground = paused;
        if (paused)
        {
            date1 = DateTime.Now;
            date2 = date1;
            return;
        }
        date2 = DateTime.Now;
        if ((date2 - date1).TotalSeconds > 10.0)
        {
            socket.Close();
            GameManager.Instance.isSocketConnectedToServer = false;
            canReadResponses = true;
        }
    }

    private IEnumerator DownloadAssetBundle(string _url)
    {
        if (PlayerPrefs.HasKey(_url))
        {
            URL.Instance.DebugPrint("Download Started");
            www = WWW.LoadFromCacheOrDownload(Path.Combine(URL.Instance.AssetBundleURL(), _url), 1);
            yield return www;
            URL.Instance.DebugPrint("Download Finished");
            AssetBundle bundle = www.assetBundle;
            AssetBundleRequest request = bundle.LoadAssetAsync<GameObject>(bundle.mainAsset.name);
            URL.Instance.DebugPrint("Download Name " + bundle.mainAsset.name);
            yield return request;
            GameObject emo_ = request.asset as GameObject;
            GameObject gm = UnityEngine.Object.Instantiate(emo_);
            bundle.Unload(unloadAllLoadedObjects: false);
            gm.transform.localScale = Vector3.zero;
            gm.transform.position = firstGiftPos.transform.position;
            gm.transform.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutSine).OnComplete(delegate
            {
                gm.transform.DOScale(Vector3.zero, 0.1f).SetDelay(6f).OnComplete(delegate
                {
                    UnityEngine.Object.Destroy(gm);
                });
            });
        }
    }

    public void StartRecording()
    {
    }

    public void StopRecording()
    {
    }

    private void PlayRecording()
    {
    }

    public void ExportMyGif()
    {
    }
}
