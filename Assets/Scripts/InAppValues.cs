using EasyMobile;
using System;
using System.Collections;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.SceneManagement;

public class InAppValues : MonoBehaviour, IStoreListener
{
	public GameObject iapCanvas;

	public InAppBuyer bestDealIAB;
	public InAppBuyer dealIAB;
	public Transform dealParent;

	private static IStoreController m_StoreController;
	private static IExtensionProvider m_StoreExtensionProvider;

	private DealsDataResponse[] iap_data;


	private void Start()
	{
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.PACKAGE_LISTING, string.Empty)).Subscribe(delegate (string x)
		{
			DealsData dealsData = DealsData.CreateFromJSON(x);
			if (dealsData.status == "success")
			{
				for (int i = 0; i < dealsData.response.Length; i++)
				{
					int index = i;
					var response = dealsData.response[i];

					if (index == 0)
                    {
						bestDealIAB.SetIAPData(response.chips, response.product_id, response.chips_display, response.price, this);
					}
                    else if(index == 1)
                    {
						dealIAB.SetIAPData(response.chips, response.product_id, response.discount_chips, response.discount_percent, response.price, this);
					}
                    else
                    {
						var deal = Instantiate(dealIAB);
						deal.transform.parent = dealParent;
						deal.SetIAPData(response.chips, response.product_id, response.discount_chips, response.discount_percent, response.price, this);
					}
				}

				iap_data = dealsData.response;
				if (m_StoreController == null)
				{
					InitializePurchasing();
				}
			}
		}, delegate (Exception ex)
		{
			Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
		});
	}

	public void OpenIAPCanvas()
    {
		iapCanvas.SetActive(true);
	}

	public void CloseIAPCanvas()
	{
		iapCanvas.SetActive(false);
	}

	public void PurchessItem(string productId)
    {
		if (IsInitialized())
		{
			Product product = m_StoreController.products.WithID(productId);

			if (product != null && product.availableToPurchase)
			{
				Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
				m_StoreController.InitiatePurchase(product);
			}
			else
			{
				Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		}
		else
		{
			Debug.Log("BuyProductID FAIL. Not initialized.");
		}
	}

	private void InitializePurchasing()
	{
		if (IsInitialized())
		{
			return;
		}

		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

		foreach (var item in iap_data)
		{
			builder.AddProduct(item.product_id, ProductType.Consumable);
		}

		UnityPurchasing.Initialize(this, builder);
	}

	private bool IsInitialized()
	{
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}


	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		m_StoreController = controller;
		m_StoreExtensionProvider = extensions;
	}

	public void OnInitializeFailed(InitializationFailureReason error)
	{
		Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	}

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
	{
		UnityMainThreadDispatcher.Instance.Enqueue(ProcessPurchaseIE(args));

		return PurchaseProcessingResult.Complete;
	}

	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
	}


	private IEnumerator ProcessPurchaseIE(PurchaseEventArgs args)
	{
        foreach (var item in iap_data)
        {
			if (args.purchasedProduct.definition.id == item.product_id)
            {
				JSONObject jSONObject = new JSONObject();
				jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
				jSONObject.AddField("package_name", item.product_id);
				jSONObject.AddField("amount", item.chips);
				jSONObject.AddField("order_id", args.purchasedProduct.receipt);

				ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.IAP_COMPLETE, jSONObject.ToString())).Subscribe(delegate (string x)
				{
					PlayerData playerData = PlayerData.CreateFromJSON(x);
					if (playerData.status == "success")
					{
						GameManager.Instance.localPlayerChips = long.Parse(playerData.response.chips);
						if (SceneManager.GetActiveScene().name == "MainMenuScene")
                        {
							FillMainMenu fmm = FindObjectOfType<FillMainMenu>();
							fmm.profileChipsStatusText.text = fmm.profileChipsText.text = GameManager.Rs + GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
						}
					}
                    else
                    {
						NativeUI.ShowToast("Not Valid");
					}
				}, delegate (Exception ex)
				{
					Debug.LogException(ex);
					NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
				});

			}
		}

		yield return null;
	}
}
