using DG.Tweening;
using EasyMobile;
using System;
using System.Collections;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SlotsManager : MonoBehaviour
{
	[Header("Elements")]
	public TextMeshProUGUI chipsText;

	public int numberOfSpins;

	public TextMeshProUGUI numberOfSpinsText;

	public long betAmount;

	public TextMeshProUGUI betAmountText;

	public Animator slideAnim;

	public TextMeshProUGUI pairText;

	public TextMeshProUGUI colorText;

	public TextMeshProUGUI seqText;

	public TextMeshProUGUI pureSeqText;

	public TextMeshProUGUI trailText;

	[Header("Buttons")]
	public Button spinDecreaseButton;

	public Button spinIncreaseButton;

	public Button betAmountDecreaseButton;

	public Button betAmountIncreaseButton;

	public Button maxButton;

	public Button spinButton;

	public Button autoSpinButton;

	public Image spinButtonImage;

	public Sprite spinImage;

	public Sprite stopImage;

	[Header("Slots Image")]
	public Image slot1Img;

	public Image slot2Img;

	public Image slot3Img;

	public Material slot1;

	public Material slot2;

	public Material slot3;

	public Sprite texImage;

	[Header("Slots Winner Text")]
	public TextMeshProUGUI winnerText;

	public TextMeshProUGUI winningTextPro;

	private Cards cards;

	private bool autoBet;

	private bool isPlayingAutoBet;

	private Vector3 offset;

	private float card1Offset;

	private float card2Offset;

	private float card3Offset;

	private float gameIndex;

	[SerializeField]
	private Texture2D heart;

	[SerializeField]
	private Texture2D spade;

	[SerializeField]
	private Texture2D diamond;

	[SerializeField]
	private Texture2D club;

	[SerializeField]
	private Sprite heartSP;

	[SerializeField]
	private Sprite spadeSP;

	[SerializeField]
	private Sprite diamondSP;

	[SerializeField]
	private Sprite clubSP;

	[SerializeField]
	private GameObject[] winningParticle;

	private void Awake()
	{
		GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
		cards = UnityEngine.Object.FindObjectOfType<Cards>();
		offset = new Vector3(0.923f, 0.923f, 0.923f);
		slot1.SetTextureOffset("_MainTex", new Vector2(0f, offset.x));
		slot2.SetTextureOffset("_MainTex", new Vector2(0f, offset.y));
		slot3.SetTextureOffset("_MainTex", new Vector2(0f, offset.z));
		gameIndex = 0f;
		AssignValues();
		PlaySlots();
		pairText.text = GameManager.Instance.slotPair + "x";
		colorText.text = GameManager.Instance.slotColor + "x";
		seqText.text = GameManager.Instance.slotSeq + "x";
		pureSeqText.text = GameManager.Instance.slotPureSeq + "x";
		trailText.text = GameManager.Instance.slotTrail + "x";
	}

	public void OpenMainMenu()
	{
		GameManager.Instance.loadingCanvas.LoadingState(_isActive: true, _tip: false);
		AudioManager.instance.StopSound("slot-loose");
		AudioManager.instance.StopSound("slot-roll");
		AudioManager.instance.StopSound("slot-winner");
		AudioManager.instance.StopSound("slot-stop");
		AudioManager.instance.StopSound("slot-winner-chips");
		DOTween.KillAll();
		SceneManager.LoadScene(URL.Instance.MAIN_MENU_SCENE);
	}

	public void SlideInfo()
	{
		if (slideAnim.GetCurrentAnimatorStateInfo(0).IsName("default"))
		{
			slideAnim.SetBool("slide", value: true);
		}
		if (slideAnim.GetCurrentAnimatorStateInfo(0).IsName("slidein"))
		{
			slideAnim.SetBool("slide", value: false);
		}
	}

	private void AssignValues()
	{
		chipsText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
		numberOfSpins = 10;
		numberOfSpinsText.text = numberOfSpins.ToString();
		betAmount = 1000000L;
		betAmountText.text = GameManager.Instance.format.FormatNumberOnTable(betAmount);
		betAmountDecreaseButton.interactable = false;
		if (GameManager.Instance.localPlayerChips < 1000000)
		{
			DisableAllButtons();
		}
	}

	public void MaxCall()
	{
		if (GameManager.Instance.localPlayerChips >= GameManager.Instance.maxSlotsChaalAmount)
		{
			betAmount = GameManager.Instance.maxSlotsChaalAmount;
		}
		else
		{
			betAmount = GameManager.Instance.localPlayerChips;
		}
		betAmountText.text = GameManager.Instance.format.FormatNumberOnTable(betAmount);
		betAmountIncreaseButton.interactable = false;
		betAmountDecreaseButton.interactable = true;
	}

	public void IncreaseSpin()
	{
		AudioManager.instance.PlaySound("click");
		if (numberOfSpins < 50)
		{
			numberOfSpins++;
			if (numberOfSpins >= 50)
			{
				spinIncreaseButton.interactable = false;
			}
		}
		if (numberOfSpins > 1)
		{
			spinDecreaseButton.interactable = true;
		}
		numberOfSpinsText.text = numberOfSpins.ToString();
	}

	public void DecreaseSpin()
	{
		AudioManager.instance.PlaySound("click");
		if (numberOfSpins > 1)
		{
			numberOfSpins--;
			spinDecreaseButton.interactable = true;
		}
		if (numberOfSpins <= 1)
		{
			spinDecreaseButton.interactable = false;
		}
		spinIncreaseButton.interactable = true;
		numberOfSpinsText.text = numberOfSpins.ToString();
	}

	public void IncreaseChips()
	{
		AudioManager.instance.PlaySound("click");
		if (GameManager.Instance.localPlayerChips >= betAmount)
		{
			betAmount *= 2L;
			if (betAmount >= GameManager.Instance.maxSlotsChaalAmount)
			{
				MaxCall();
			}
			if (betAmount > GameManager.Instance.localPlayerChips)
			{
				MaxCall();
			}
		}
		betAmountDecreaseButton.interactable = true;
		betAmountText.text = GameManager.Instance.format.FormatNumberOnTable(betAmount);
	}

	public void DecreaseChips()
	{
		AudioManager.instance.PlaySound("click");
		if (betAmount > 1000000)
		{
			betAmount /= 2L;
			if (betAmount < 1000000)
			{
				betAmount = 1000000L;
				betAmountDecreaseButton.interactable = false;
			}
		}
		betAmountIncreaseButton.interactable = true;
		betAmountText.text = GameManager.Instance.format.FormatNumberOnTable(betAmount);
	}

	public void SpinSlots()
	{
		AudioManager.instance.PlaySound("click");
		if (isPlayingAutoBet)
		{
			isPlayingAutoBet = false;
		}
		else
		{
			DisableAllButtons();
			PlaySlots();
			autoBet = false;
			SpinSlotsCO(GameManager.Instance.localPlayerID, betAmount.ToString());
		}
		AudioManager.instance.PlaySound("slot-roll");
	}

	private void DisableAllButtons()
	{
		spinIncreaseButton.interactable = false;
		betAmountIncreaseButton.interactable = false;
		spinDecreaseButton.interactable = false;
		betAmountDecreaseButton.interactable = false;
		maxButton.interactable = false;
		if (isPlayingAutoBet)
		{
			spinButtonImage.sprite = stopImage;
		}
		else
		{
			spinButton.interactable = false;
		}
		autoSpinButton.interactable = false;
	}

	private void EnableAllButtons()
	{
		spinIncreaseButton.interactable = true;
		betAmountIncreaseButton.interactable = true;
		spinDecreaseButton.interactable = true;
		betAmountDecreaseButton.interactable = true;
		maxButton.interactable = true;
		spinButtonImage.sprite = spinImage;
		spinButton.interactable = true;
		autoSpinButton.interactable = true;
	}

	public void AutoSpin()
	{
		isPlayingAutoBet = true;
		AutoSpinSlots();
	}

	public void AutoSpinSlots()
	{
		if (numberOfSpins > 0 && betAmount <= GameManager.Instance.localPlayerChips && isPlayingAutoBet)
		{
			DisableAllButtons();
			numberOfSpins--;
			numberOfSpinsText.text = numberOfSpins.ToString();
			PlaySlots();
			autoBet = true;
			isPlayingAutoBet = true;
			SpinSlotsCO(GameManager.Instance.localPlayerID, betAmount.ToString());
		}
		else
		{
			EnableAllButtons();
			isPlayingAutoBet = false;
		}
	}

	private void PlaySlots()
	{
		if (slot1Img.sprite != null)
		{
			slot1Img.sprite = null;
		}
		if (slot2Img.sprite != null)
		{
			slot2Img.sprite = null;
		}
		if (slot3Img.sprite != null)
		{
			slot3Img.sprite = null;
		}
		slot1Img.material = slot1;
		slot2Img.material = slot2;
		slot3Img.material = slot3;
		for (int i = 0; i < winningParticle.Length; i++)
		{
			winningParticle[i].SetActive(value: false);
		}
	}

	private void SpinSlotsCO(string _playerID, string _amount)
	{
		gameIndex += 1f;
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("playerId", _playerID);
		jSONObject.AddField("amount", _amount);
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.SLOTS, jSONObject.ToString())).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint(GameManager.Instance.security.Decrypt(x));
			AssignSlotCards(GameManager.Instance.security.Decrypt(x));
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
		});
	}

	private void AssignSlotCards(string _data)
	{
		AssignOffset(_data);
		PlayerData playerData = PlayerData.CreateFromJSON(_data);
		GameManager.Instance.localPlayerChips = long.Parse(playerData.response.chips);
		if (playerData.status == "success")
		{
			GameManager.Instance.LogMessage("Slots Winner");
		}
		else
		{
			GameManager.Instance.LogMessage("Slots Losser");
		}
		slot1.DOOffset(new Vector2(0f, card1Offset), "_MainTex", 5f).SetEase(Ease.InOutBack, 0.2f, 1f).OnComplete(delegate
		{
			AudioManager.instance.PlaySound("slot-stop");
		});
		slot2.DOOffset(new Vector2(0f, card2Offset), "_MainTex", 5f).SetDelay(0.5f).SetEase(Ease.InOutBack, 0.2f, 1f)
			.OnComplete(delegate
			{
				AudioManager.instance.PlaySound("slot-stop");
			});
		slot3.DOOffset(new Vector2(0f, card3Offset), "_MainTex", 5f).SetDelay(1f).SetEase(Ease.InOutBack, 0.2f, 1f)
			.OnComplete(delegate
			{
				CompleteSpinCallback(_data);
			});
		StartCoroutine(AssignTexture(playerData.response.cards[0], "0", 1f));
		StartCoroutine(AssignTexture(playerData.response.cards[1], "1", 2f));
		StartCoroutine(AssignTexture(playerData.response.cards[2], "2", 3f));
	}

	private IEnumerator AssignTexture(string _data, string _index, float _time)
	{
		yield return new WaitForSeconds(_time);
		string[] words = _data.Split('-');
		if (_index == "0")
		{
			if (words[0] == "heart")
			{
				slot1Img.sprite = heartSP;
				slot1.SetTexture("_MainTex", heart);
			}
			else if (words[0] == "spade")
			{
				slot1Img.sprite = spadeSP;
				slot1.SetTexture("_MainTex", spade);
			}
			else if (words[0] == "club")
			{
				slot1Img.sprite = clubSP;
				slot1.SetTexture("_MainTex", club);
			}
			else if (words[0] == "diamond")
			{
				slot1Img.sprite = diamondSP;
				slot1.SetTexture("_MainTex", diamond);
			}
		}
		else if (_index == "1")
		{
			if (words[0] == "heart")
			{
				slot2Img.sprite = heartSP;
				slot2.SetTexture("_MainTex", heart);
			}
			else if (words[0] == "spade")
			{
				slot2Img.sprite = spadeSP;
				slot2.SetTexture("_MainTex", spade);
			}
			else if (words[0] == "club")
			{
				slot2Img.sprite = clubSP;
				slot2.SetTexture("_MainTex", club);
			}
			else if (words[0] == "diamond")
			{
				slot2Img.sprite = diamondSP;
				slot2.SetTexture("_MainTex", diamond);
			}
		}
		else if (_index == "2")
		{
			if (words[0] == "heart")
			{
				slot3Img.sprite = heartSP;
				slot3.SetTexture("_MainTex", heart);
			}
			else if (words[0] == "spade")
			{
				slot3Img.sprite = spadeSP;
				slot3.SetTexture("_MainTex", spade);
			}
			else if (words[0] == "club")
			{
				slot3Img.sprite = clubSP;
				slot3.SetTexture("_MainTex", club);
			}
			else if (words[0] == "diamond")
			{
				slot3Img.sprite = diamondSP;
				slot3.SetTexture("_MainTex", diamond);
			}
		}
	}

	private void AssignOffset(string _data)
	{
		PlayerData playerData = PlayerData.CreateFromJSON(_data);
		card1Offset = gameIndex * 13f + reCardIndex(playerData.response.cards[0]);
		card2Offset = gameIndex * 13f + reCardIndex(playerData.response.cards[1]);
		card3Offset = gameIndex * 13f + reCardIndex(playerData.response.cards[2]);
		URL.Instance.DebugPrint("1 : " + card1Offset + " 2 : " + card2Offset + " 3 : " + card3Offset);
	}

	private void CompleteSpinCallback(string _data)
	{
		AudioManager.instance.PlaySound("slot-stop");
		AudioManager.instance.StopSound("slot-roll");
		PlayerData playerData = PlayerData.CreateFromJSON(_data);
		chipsText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
		if (playerData.status == "success")
		{
			AudioManager.instance.PlaySound("slot-winner");
			for (int i = 0; i < winningParticle.Length; i++)
			{
				winningParticle[i].SetActive(value: true);
			}
			winningParticle[0].GetComponent<Animator>().SetTrigger("slotwinner");
			winnerText.text = "You Won " + GameManager.Instance.format.FormatNumberOnTable(long.Parse(playerData.response.winAmount));
			winningTextPro.text = playerData.response.rank;
			Invoke("PlayS", 2f);
		}
		else
		{
			AudioManager.instance.PlaySound("slot-loose");
			winnerText.text = "Better luck next time!";
		}
		if (isPlayingAutoBet)
		{
			Invoke("AutoSpinSlots", 2f);
		}
		else
		{
			EnableAllButtons();
		}
	}

	private void PlayS()
	{
		AudioManager.instance.PlaySound("slot-winner-chips");
		Invoke("StopS", 4f);
	}

	private void StopS()
	{
		AudioManager.instance.StopSound("slot-winner-chips");
	}

	private float reCardIndex(string _carInd)
	{
		float result = 0f;
		string[] array = _carInd.Split('-');
		if (array[1] == "ace")
		{
			result = 0.923f;
		}
		else if (array[1] == "two")
		{
			result = 1.846f;
		}
		if (array[1] == "three")
		{
			result = 2.769f;
		}
		if (array[1] == "four")
		{
			result = 3.692f;
		}
		if (array[1] == "five")
		{
			result = 4.615f;
		}
		if (array[1] == "six")
		{
			result = 5.538f;
		}
		if (array[1] == "seven")
		{
			result = 6.461f;
		}
		if (array[1] == "eight")
		{
			result = 7.384f;
		}
		if (array[1] == "nine")
		{
			result = 8.307f;
		}
		if (array[1] == "ten")
		{
			result = 9.23f;
		}
		if (array[1] == "jack")
		{
			result = 10.153f;
		}
		if (array[1] == "queen")
		{
			result = 11.076f;
		}
		if (array[1] == "king")
		{
			result = 11.999f;
		}
		return result;
	}
}
