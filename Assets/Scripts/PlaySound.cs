using UnityEngine;

public class PlaySound : MonoBehaviour
{
	public void PlaySoundWithName(string _name)
	{
		AudioManager.instance.PlaySound(_name);
	}

	public void PlayBonusLoopSound()
	{
		AudioManager.instance.PlaySound("bonus-idle");
	}
}
