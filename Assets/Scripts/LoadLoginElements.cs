using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoadLoginElements : MonoBehaviour
{
    public GameObject loginPanel, profilePanel;

    public TextMeshProUGUI fbChipsTextOnly;
    public TMP_InputField inputField;

    public Image mainProfile;
    public Image[] profiles;
    public Sprite[] profileL, profileD;

    public static string profilePicIndex = "NaN";


    private void Start()
    {
        if (GameManager.Instance.fbChips.Length > 0)
        {
            fbChipsTextOnly.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(GameManager.Instance.fbChips)) + " Reward";
        }

        GameManager.Instance.loadingCanvas.LoadingState(false, false);

        if (PlayerPrefs.GetFloat("Facebook") == 1f)
        {
            GameManager.Instance.loadingCanvas.LoadingState(true, false);
            FacebookLogin();
        }
        else if (!string.IsNullOrEmpty(PlayerPrefs.GetString("GuestName")))
        {
            GameManager.Instance.loadingCanvas.LoadingState(true, false);
            GuestLogin(false);
        }
        else
        {
            profilePicIndex = "0";
            SetProfileImage(true);
        }
    }

    public void GuestLogin(bool openProfile)
    {
        //AudioManager.instance.PlaySound("click");
        if (openProfile)
        {
            loginPanel.SetActive(false);
            profilePanel.SetActive(true);
        }
        else
        {
            string name = inputField.text;
            if (string.IsNullOrEmpty(name))
            {
                name = "Guest" + Random.Range(500, 1000);
            }
            PlayerPrefs.SetString("GuestName", name);
            GameManager.Instance.loginManager.GuestLoginCO(name);
        }
    }

    public void ImageSelect(int index)
    {
        profilePicIndex = index.ToString();
        SetProfileImage();
    }

    public void FacebookLogin()
    {
        GameManager.Instance.loginManager.LoginWithFacebook();
    }

    private void SetProfileImage(bool defStart = false)
    {
        int currantProfileIndex;

        if (profilePicIndex != "NaN")
        {
            currantProfileIndex = int.Parse(profilePicIndex);
        }
        else
        {
            if (defStart)
            {
                currantProfileIndex = 0;
            }
            else
            {
                return;
            }
        }

        for (int i = 0; i < profiles.Length; i++)
        {
            int index = i;
            if (index == currantProfileIndex)
            {
                profiles[index].sprite = profileL[index];
                mainProfile.sprite = profileL[index];
                profiles[index].transform.GetChild(0).GetComponent<Image>().sprite = profileL[10];
            }
            else
            {
                profiles[index].sprite = profileD[index];
                profiles[index].transform.GetChild(0).GetComponent<Image>().sprite = profileD[10];
            }
        }
    }
}
