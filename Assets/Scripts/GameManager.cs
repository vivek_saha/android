﻿using EasyMobile;
using Facebook.Unity;
using System.Collections;
using System.IO;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public static GameManager Instance;

	public static string Rs = "₹ ";

	[Header("Version Number")]
	public string versionNumber;

	[Header("Socket Details")]
	public string host;

	public string port;

	public string tbid;

	public string type;

	public string tblBootedAmount;

	public bool isSocketConnectedToServer;

	public double internetSpeed;

	public bool isConnectedToInternet;

	[Header("Local Player Details")]
	public string localPlayerName;

	public string id;

	public string localPlayerID;

	public string tempImage;

	public string tempName;

	public string fbChips;

	public bool isBlind;

	//public bool isVIP;

	public long localPlayerChips;

	//public long localPlayerForceSideshow;

	public long localPlayerPosition;

	public long tempLocalPlayerPosition;

	//public string notificationID;

	public float lastbonusTime;

	public long forceDeduct;

	public bool isFacebookLogin;

	public bool isSwitchingTable;

	public long maxSlotsChaalAmount;

	public string slotPair;

	public string slotTrail;

	public string slotPureSeq;

	public string slotSeq;

	public string slotColor;

	public bool isBootedOut;

	public string bootedOutTitle;

	public string bootedOutBody;

	//public int VipDetails;

	public bool canDoAutoLogin;

	[Header("Profile Sprites")]
	public Sprite[] profileDummyImages;

	[Header("Second Player Name")]
	public string secondPlayerNameForGifts;

	[Header("Counting Elements")]
	public float chipCountDuration = 1f;

	public long tempTotalAmountOnTable;

	[Header("Setting Canvas")]
	public GameObject settingCanvas;
	public NumberFormat format;
	public LoadingCanvas loadingCanvas;
	public InAppValues inAppValuesCanvas;
	public AdsManager adsManager;
	public Settings sCanvas;
	public LoginManager loginManager;
	public EncDec security;

	public bool canDeviceCheck;

	[Header("Other Data")]
	public string appURL;
	public string privacyURL;


	private void Awake()
	{
        if (Instance == null)
        {
			Instance = this;
			DontDestroyOnLoad(gameObject);
        }
        else
        {
			Destroy(gameObject);
        }

		if (!FB.IsInitialized)
		{
			FB.Init(InitCallback, OnHideUnity);
		}
		else
		{
			FB.ActivateApp();
		}

		Application.targetFrameRate = 60;
		Screen.sleepTimeout = -1;
		//notificationID = "0";
	}

	public void StartManager()
    {
		isConnectedToInternet = true;
		adsManager.enabled = true;
		inAppValuesCanvas.enabled = true;
		adsManager.enabled = true;
		//InvokeRepeating("CheckDeviceConnection", 5f, 5f);
		PlayerPrefs.GetFloat("Facebook", 0f);

		SceneManager.LoadScene(URL.Instance.LOGIN_SCENE);
	}

	public void LanguageConverter(TextMeshProUGUI _text, string _english)
	{
		_text.text = _english;
	}

	public void LanguageConverter(Text _text, string _english)
	{
		_text.text = _english;
	}

	private void InitCallback()
	{
		if (FB.IsInitialized)
		{
			FB.ActivateApp();
		}
		else
		{
			Debug.Log("Failed to Initialize the Facebook SDK");
		}
	}

	private void OnHideUnity(bool isGameShown)
	{
		if (!isGameShown)
		{
			Time.timeScale = 0f;
		}
		else
		{
			Time.timeScale = 1f;
		}
	}

	public void CountChips(long _prev, long _target, Text _text, long _amount)
	{
		StartCoroutine(CountTo(_prev, _target, _text, _amount));
	}

	private IEnumerator CountTo(long _prev, long _target, Text _text, long _amount)
	{
		for (float timer = 0f; timer < chipCountDuration; timer += Time.deltaTime)
		{
			float progress = timer / chipCountDuration;
			_amount = (long)Mathf.Lerp(_prev, _target, progress);
			_text.text = Rs + format.FormatNumberOnTable(_amount);
			yield return null;
		}
		_amount = _target;
		tempTotalAmountOnTable = _amount;
		_text.text = Rs + format.FormatNumberOnTable(_amount);
	}

	public void CountChipsSinglePlayer(long _prev, long _target, Text _text)
	{
		StartCoroutine(CountToSinglePlayer(_prev, _target, _text));
	}

	private IEnumerator CountToSinglePlayer(long _prev, long _target, Text _text)
	{
		for (float timer = 0f; timer < chipCountDuration; timer += Time.deltaTime)
		{
			float progress = timer / chipCountDuration;
			long tempChips = (long)Mathf.Lerp(_prev, _target, progress);
			_text.text = format.FormatNumber(tempChips);
			yield return null;
		}
		_text.text = format.FormatNumber(_target);
	}

	public void ResetGameManagerAfterEveryGame()
	{
		tempTotalAmountOnTable = 0L;
	}

	public void OpenSettingCanvas()
	{
		AudioManager.instance.PlaySound("click");
		settingCanvas.transform.GetChild(0).gameObject.SetActive(true);
	}

	public void CloseSettingCanvas()
	{
		AudioManager.instance.PlaySound("close-popup");
		settingCanvas.transform.GetChild(0).gameObject.SetActive(false);
	}

	public void CheckDeviceConnection()
	{
		if (!(localPlayerID == string.Empty))
		{
			JSONObject jSONObject = new JSONObject();
			jSONObject.AddField("playerId", localPlayerID);
			ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.MULTI_LOGIN, jSONObject.ToString())).Subscribe(delegate(string x)
			{
				PlayerData playerData = PlayerData.CreateFromJSON(security.Decrypt(x));
				if (playerData.status == "success")
				{
					if (playerData.response.deviceId != SystemInfo.deviceUniqueIdentifier)
					{
						if (canDeviceCheck)
						{
							canDeviceCheck = false;
							URL.Instance.DebugPrint("Another Device");
							NativeUI.ShowToast("Sorry! You've been kicked out.As you've logged in from another device");
							PlayerPrefs.SetFloat("Facebook", 0f);
							SceneManager.LoadScene(URL.Instance.LOGIN_SCENE);
						}
					}
					else
					{
						canDeviceCheck = true;
					}
				}
			});
		}
	}

	public void LogMessage(string _message)
	{
		Debug.LogError(_message);
		//GoogleAnalyticsV4.instance.LogScreen("Android : " + _message);
	}

	public void LoadImagesCO(Image _profileImage, string _name)
	{
		int index = int.Parse(_name);

        if (index < loginManager.profileSprites.Length)
        {
			_profileImage.sprite = loginManager.profileSprites[index];
		}


		/*if (File.Exists(ImageName(_name)))
		{
			byte[] array = File.ReadAllBytes(ImageName(_name));
			if ((array[0] == 137 && array[1] == 80 && array[2] == 78 && array[3] == 71 && array[4] == 13 && array[5] == 10 && array[6] == 26 && array[7] == 10) || (array[0] == byte.MaxValue && array[1] == 216 && array[2] == byte.MaxValue && array[3] == 224 && array[4] == 0 && array[5] == 16 && array[6] == 74 && array[7] == 70 && array[8] == 73 && array[9] == 70))
			{
				Texture2D texture2D = new Texture2D(8, 8);
				texture2D.LoadImage(array);
				Sprite sprite2 = _profileImage.sprite = Sprite.Create(texture2D, new Rect(0f, 0f, texture2D.width, texture2D.height), Vector2.zero);
				URL.Instance.DebugPrint("Image already downloaded : " + _name);
			}
			else
			{
				URL.Instance.DebugPrint("Image error downloaded : " + _name);
			}
		}
		else
		{
			URL.Instance.DebugPrint("Profile Image not found : " + _name);
		}*/
	}

	private string ImageName(string _name)
	{
		return Path.Combine(path2: "profile_image_" + (23 - int.Parse(_name)).ToString() + ".png", path1: Application.persistentDataPath);
	}

	public static void ShareMessage(string msg)
    {
		NativeShare ns = new NativeShare();
		ns.SetTitle("Thanks for Sharing");
		ns.SetText(msg);
		ns.Share();
	}

	public void LogOutButton()
    {
		CloseSettingCanvas();
		PlayerPrefs.DeleteKey("Facebook");
		PlayerPrefs.DeleteKey("GuestName");
		SceneManager.LoadScene(URL.Instance.LOGIN_SCENE);
	}


}
