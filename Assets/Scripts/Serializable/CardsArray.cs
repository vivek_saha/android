using System;
using UnityEngine;

[Serializable]
public class CardsArray
{
	public string rank;

	public string type;

	public static CardsArray CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<CardsArray>(data);
	}
}
