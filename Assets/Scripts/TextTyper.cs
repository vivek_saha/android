using System.Collections;
using TMPro;
using UnityEngine;

public class TextTyper : MonoBehaviour
{
	private TMP_Text m_TextComponent;

	private bool hasTextChanged;

	private void Awake()
	{
		m_TextComponent = base.gameObject.GetComponent<TMP_Text>();
	}

	private void Start()
	{
		StartCoroutine(RevealCharacters(m_TextComponent));
	}

	private void OnEnable()
	{
		TMPro_EventManager.TEXT_CHANGED_EVENT.Add(ON_TEXT_CHANGED);
	}

	private void OnDisable()
	{
		TMPro_EventManager.TEXT_CHANGED_EVENT.Remove(ON_TEXT_CHANGED);
	}

	private void ON_TEXT_CHANGED(Object obj)
	{
		hasTextChanged = true;
	}

	private IEnumerator RevealCharacters(TMP_Text textComponent)
	{
		textComponent.ForceMeshUpdate();
		TMP_TextInfo textInfo = textComponent.textInfo;
		int totalVisibleCharacters = textInfo.characterCount;
		int visibleCount = 0;
		while (true)
		{
			if (hasTextChanged)
			{
				totalVisibleCharacters = textInfo.characterCount;
				hasTextChanged = false;
			}
			if (visibleCount > totalVisibleCharacters)
			{
				yield return new WaitForSeconds(5f);
				visibleCount = 0;
			}
			yield return new WaitForSeconds(0.1f);
			textComponent.maxVisibleCharacters = visibleCount;
			visibleCount++;
			yield return null;
		}
	}
}
