using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
[ExecuteInEditMode]
[AddComponentMenu("2DxFX/Standard/Mystic_Distortion_Additive")]
public class _2dxFX_Mystic_Distortion_Additive : MonoBehaviour
{
	[HideInInspector]
	public Material ForceMaterial;

	[HideInInspector]
	public bool ActiveChange = true;

	private string shader = "2DxFX/Standard/Mystic_Distortion_Additive";

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Alpha = 1f;

	[HideInInspector]
	[Range(0f, 0.45f)]
	public float _Pitch = 0.45f;

	[HideInInspector]
	public bool Pitch_Wave = true;

	[HideInInspector]
	[Range(0f, 16f)]
	public float _Pitch_Speed = 1f;

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Pitch_Offset;

	[HideInInspector]
	[Range(0f, 128f)]
	public float _OffsetX = 56f;

	[HideInInspector]
	[Range(0f, 128f)]
	public float _OffsetY = 28f;

	[HideInInspector]
	[Range(0f, 1f)]
	public float _DistanceX = 0.01f;

	[HideInInspector]
	[Range(0f, 1f)]
	public float _DistanceY = 0.04f;

	[HideInInspector]
	[Range(0f, 6.28f)]
	public float _WaveTimeX = 1.16f;

	[HideInInspector]
	[Range(0f, 6.28f)]
	public float _WaveTimeY = 5.12f;

	[HideInInspector]
	public bool AutoPlayWaveX;

	[HideInInspector]
	[Range(0f, 5f)]
	public float AutoPlaySpeedX = 5f;

	[HideInInspector]
	public bool AutoPlayWaveY;

	[HideInInspector]
	[Range(0f, 50f)]
	public float AutoPlaySpeedY = 5f;

	[HideInInspector]
	public bool AutoRandom;

	[HideInInspector]
	[Range(0f, 50f)]
	public float AutoRandomRange = 10f;

	[HideInInspector]
	public int ShaderChange;

	private Material tempMaterial;

	private Material defaultMaterial;

	private Image CanvasImage;

	private void Awake()
	{
		if (base.gameObject.GetComponent<Image>() != null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
	}

	private void Start()
	{
		ShaderChange = 0;
	}

	public void CallUpdate()
	{
		Update();
	}

	private void Update()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (ShaderChange == 0 && ForceMaterial != null)
		{
			ShaderChange = 1;
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
			ForceMaterial.hideFlags = HideFlags.None;
			ForceMaterial.shader = Shader.Find(shader);
		}
		if (ForceMaterial == null && ShaderChange == 1)
		{
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
			ShaderChange = 0;
		}
		if (ActiveChange)
		{
			if (Pitch_Wave)
			{
				_Pitch_Offset = Mathf.Sin(Time.time * _Pitch_Speed) * 0.05f;
				UnityEngine.Debug.Log(_Pitch_Offset);
			}
			else
			{
				_Pitch_Offset = 0f;
			}
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial.SetFloat("_Alpha", 1f - _Alpha);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_Pitch", _Pitch + _Pitch_Offset);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetX", _OffsetX);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetY", _OffsetY);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_DistanceX", _DistanceX);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_DistanceY", _DistanceY);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_WaveTimeX", _WaveTimeX);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_WaveTimeY", _WaveTimeY);
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material.SetFloat("_Alpha", 1f - _Alpha);
				CanvasImage.material.SetFloat("_Pitch", _Pitch + _Pitch_Offset);
				CanvasImage.material.SetFloat("_OffsetX", _OffsetX);
				CanvasImage.material.SetFloat("_OffsetY", _OffsetY);
				CanvasImage.material.SetFloat("_DistanceX", _DistanceX);
				CanvasImage.material.SetFloat("_DistanceY", _DistanceY);
				CanvasImage.material.SetFloat("_WaveTimeX", _WaveTimeX);
				CanvasImage.material.SetFloat("_WaveTimeY", _WaveTimeY);
			}
			float num = (!AutoRandom) ? Time.deltaTime : (UnityEngine.Random.Range(1f, AutoRandomRange) / 5f * Time.deltaTime);
			if (AutoPlayWaveX)
			{
				_WaveTimeX += AutoPlaySpeedX * num;
			}
			if (AutoPlayWaveY)
			{
				_WaveTimeY += AutoPlaySpeedY * num;
			}
			if (_WaveTimeX > 6.28f)
			{
				_WaveTimeX = 0f;
			}
			if (_WaveTimeY > 6.28f)
			{
				_WaveTimeY = 0f;
			}
		}
	}

	private void OnDestroy()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (Application.isPlaying || !Application.isEditor)
		{
			return;
		}
		if (tempMaterial != null)
		{
			UnityEngine.Object.DestroyImmediate(tempMaterial);
		}
		if (base.gameObject.activeSelf && defaultMaterial != null)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnDisable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (base.gameObject.activeSelf && defaultMaterial != null)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnEnable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (defaultMaterial == null)
		{
			defaultMaterial = new Material(Shader.Find("Sprites/Default"));
		}
		if (ForceMaterial == null)
		{
			ActiveChange = true;
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
		}
		else
		{
			ForceMaterial.shader = Shader.Find(shader);
			ForceMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
		}
	}
}
