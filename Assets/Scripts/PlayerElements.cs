using DG.Tweening;
using Facebook.Unity;
using SocketIO;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerElements : MonoBehaviour
{
	[HideInInspector]
	public TableElements tElements;

	public NetworkManager networkManager;

	[Header("Player Objects")]
	public GameObject playerPanelGO;

	public GameObject playerInviteGO;

	[Header("Details")]
	public bool isLocalPlayer;

	public string playerID;

	public string chips;

	public string previousChips;

	public string forceSideShow;

	public string position;

	public string tempposition;

	public string localPosition;

	public string amount;

	public bool sittingTable;

	public bool isFacebook;

	public bool isActive;

	public bool isBlind;

	public bool isDealer;

	public bool playWinnerAnimation;

	public GameObject giftButton;

	[Header("Dealer Object")]
	public GameObject dealerGO;

	[Header("Profile Elements")]
	public Text nameText;

	public Text chipsText;

	public Image profileImage;

	//public Image profileImageBlur;

	//public GameObject seenGO;

	//public GameObject packGO;

	public Text cardStatus;

	//public GameObject forceSSGO;

	[Header("Timer Slider")]
	public float maxTimer;

	public float timer;

	public bool isTimerRunning;

	public Color normalColor;

	public Color alertColor;

	public Slider timerSlider;

	public Image timerColorSliderImage;

	//public Image timerHandleColorSliderImage;

	//public GameObject turnAnimationGO;

	[Header("Cards Holder")]
	public GameObject cardsGO;

	public SpriteRenderer[] localCardsImage;

	public bool[] isVarCard = new bool[3];

	public GameObject[] jokerCardsGO;

	public Transform[] cardMainPanel;

	public GameObject cardMainPanel_1_FirstPos;

	public GameObject cardMainPanel_2_FirstPos;

	public GameObject cardMainPanel_3_FirstPos;

	public Sprite defaultCardSprite;

	public Button viewCardButton;

	//public GameObject glowImageGO;

	[Header("Chips Movement")]
	public CanvasGroup chipsCanvasGroup;

	public Text chipsMovementText;

	public Transform bootAmountTransform;

	public bool isChipsMoving;

	[Header("Winner Text")]
	public GameObject winnerTextGO;

	public Text winningCardText;

	[Header("Winner Text")]
	public GameObject selectingVariationGO;

	[Header("Turn Amount Elements")]
	public Text turnAmountText;
	//public GameObject turnAmountGo;
	//public string turn;
	//public Text turnTypeText;

	[Header("Side Show Elements")]
	public GameObject acceptSideShowGO;

	public GameObject declineSideShowGO;

	[Header("Player Win Elements")]
	public GameObject winGO;
	public Image winnerTextImage;

	[Header("Chat Elements")]
	public GameObject chatHeadPanel;
	public TextMeshProUGUI chatText;

	[Header("Black Strip")]
	public Image[] blackStrip;

	public SpriteRenderer[] blackSprites;

	private float myFloat;

	public bool firstTimer;

	public bool secondTimer;

	private float chaalTimerTest = 17.8f;

	private void Awake()
	{
		tElements = FindObjectOfType<TableElements>();
		networkManager = FindObjectOfType<NetworkManager>();
		ResetChatHeads();
		timerColorSliderImage.color = normalColor;
		timerColorSliderImage.color = normalColor;
	}

	private void Start()
	{
	}

	public void UpdateDetailsUI(SocketIOEvent e, int _pos, bool _updateChipsText)
	{
		string data = e.data.ToString();
		TableResponseData tableResponseData = TableResponseData.CreateFromJSON(data);
		if (e.data.GetField("response")[_pos].HasField("playerId"))
		{
			playerID = tableResponseData.response[_pos].playerId;
			if (playerID == GameManager.Instance.localPlayerID)
			{
				isLocalPlayer = true;
			}
		}
		if (e.data.GetField("response")[_pos].HasField("maxBlind") && isLocalPlayer)
		{
			tElements.maxNumberOfBlind = tableResponseData.response[_pos].maxBlind;
			tElements.maxNumberOfBlindText_Info.text = tableResponseData.response[_pos].maxBlind;
		}
		if (e.data.GetField("response")[_pos].HasField("forceDeduct") && isLocalPlayer)
		{
			tElements.forceDeduct = long.Parse(tableResponseData.response[_pos].forceDeduct);
			GameManager.Instance.forceDeduct = tElements.forceDeduct;
			//tElements.forceDeductText.text = tElements.forceDeduct.ToString();
		}
		if (e.data.GetField("response")[_pos].HasField("maxTimer"))
		{
			maxTimer = float.Parse(tableResponseData.response[_pos].maxTimer);
		}
		if (e.data.GetField("response")[_pos].HasField("variation"))
		{
			if (tableResponseData.response[_pos].variation == "1")
			{
				if (!isLocalPlayer)
				{
					selectingVariationGO.SetActive(value: true);
				}
				else
				{
					tElements.selectVariationCanvas.SetActive(value: true);
				}
			}
			else
			{
				selectingVariationGO.SetActive(value: false);
			}
		}
		if (e.data.GetField("response")[_pos].HasField("chips"))
		{
			chips = tableResponseData.response[_pos].chips;
			if (isLocalPlayer)
			{
				GameManager.Instance.localPlayerChips = long.Parse(chips);
			}
			if (_updateChipsText)
			{
				previousChips = chips;
				chipsText.text = GameManager.Instance.format.FormatNumber(long.Parse(chips));
			}
		}
		/*if (e.data.GetField("response")[_pos].HasField("forcesideshow"))
		{
			forceSideShow = tableResponseData.response[_pos].forcesideshow;
			if (isLocalPlayer)
			{
				tElements.forceSideShowCountText.text = GameManager.Instance.format.FormatNumber(long.Parse(forceSideShow));
				GameManager.Instance.localPlayerForceSideshow = long.Parse(forceSideShow);
			}
		}*/
		if (e.data.GetField("response")[_pos].HasField("position"))
		{
			position = tableResponseData.response[_pos].position;
			tempposition = position + 1;
		}
		if (e.data.GetField("response")[_pos].HasField("sittingTable"))
		{
			sittingTable = (bool.Parse(tableResponseData.response[_pos].sittingTable) ? true : false);
		}
		if (e.data.GetField("response")[_pos].HasField("amount"))
		{
			amount = tableResponseData.response[_pos].amount;
			if (isLocalPlayer)
			{
				tElements.amount = long.Parse(amount);
				tElements.tempAmount = long.Parse(amount);
				tElements.chaalAmountText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(tableResponseData.response[_pos].amount));
				//tElements.chaalNONAmountText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(tableResponseData.response[_pos].amount));
			}
		}
		if (e.data.GetField("response")[_pos].HasField("facebook"))
		{
			if (tableResponseData.response[_pos].facebook == "1")
			{
				isFacebook = true;
				if (tableResponseData.response[_pos].profile == "-1")
				{
					FB.API("/" + playerID + "/picture?type=square&height=128&width=128", HttpMethod.GET, delegate(IGraphResult result)
					{
						if (result.Error == null && SceneManager.GetActiveScene().name == URL.Instance.TABLE_SCENE)
						{
							profileImage.sprite = Sprite.Create(result.Texture, new Rect(0f, 0f, result.Texture.width, result.Texture.height), default(Vector2));
							//profileImageBlur.sprite = Sprite.Create(result.Texture, new Rect(0f, 0f, result.Texture.width, result.Texture.height), default(Vector2));
						}
					});
				}
				else if (tableResponseData.response[_pos].profile == null)
				{
					profileImage.sprite = GameManager.Instance.sCanvas.defaultSprite;
					//profileImageBlur.sprite = GameManager.Instance.sCanvas.defaultSprite;
				}
				else
				{
					GameManager.Instance.LoadImagesCO(profileImage, tableResponseData.response[_pos].profile);
					//GameManager.Instance.LoadImagesCO(profileImageBlur, tableResponseData.response[_pos].profile);
				}
			}
			else
			{
				isFacebook = false;
				profileImage.sprite = GameManager.Instance.sCanvas.defaultSprite;
				//profileImageBlur.sprite = GameManager.Instance.sCanvas.defaultSprite;
			}
		}
		if (e.data.GetField("response")[_pos].HasField("isActive"))
		{
			isActive = (bool.Parse(tableResponseData.response[_pos].isActive) ? true : false);
		}
		if (e.data.GetField("response")[_pos].HasField("isBlind"))
		{
			isBlind = (bool.Parse(tableResponseData.response[_pos].isBlind) ? true : false);
			if (isLocalPlayer)
			{
				GameManager.Instance.isBlind = isBlind;
			}
		}
		if (e.data.GetField("response")[_pos].HasField("dealer"))
		{
			if (tableResponseData.response[_pos].dealer == "1")
			{
				dealerGO.SetActive(value: true);
				isDealer = true;
			}
			else
			{
				dealerGO.SetActive(value: false);
				isDealer = false;
			}
		}
		/*if (e.data.GetField("response")[_pos].HasField("turn"))
		{
			turn = tableResponseData.response[_pos].turn;
		}*/
		if (e.data.GetField("response")[_pos].HasField("name"))
		{
			nameText.text = URL.Instance.TrimNameForOthers(tableResponseData.response[_pos].name, isFacebook, tableResponseData.response[_pos].tempname);
		}
	}

	public void AssignCards(SocketIOEvent e, int _pos, bool _autoView, bool _playCardAnimation)
	{
		string data = e.data.ToString();
		TableResponseData tableResponseData = TableResponseData.CreateFromJSON(data);
		Debug.Log(e.data.GetField("response")[_pos].GetField("cards"));
		if (!e.data.GetField("response")[_pos].HasField("cards"))
		{
			return;
		}
		if (tableResponseData.response[_pos].cards.Length > 0)
		{
			for (int i = 0; i < tElements._cards.cardImage.Length; i++)
			{
				if (e.data.GetField("response")[_pos].GetField("cards")[0].HasField("var"))
				{
					isVarCard[0] = true;
				}
				if (e.data.GetField("response")[_pos].GetField("cards")[1].HasField("var"))
				{
					isVarCard[1] = true;
				}
				if (e.data.GetField("response")[_pos].GetField("cards")[2].HasField("var"))
				{
					isVarCard[2] = true;
				}
				if (tElements._cards.cardImage[i].name == tableResponseData.response[_pos].cards[0].type + "-" + tableResponseData.response[_pos].cards[0].rank)
				{
					localCardsImage[0].sprite = tElements._cards.cardImage[i];
				}
				if (tElements._cards.cardImage[i].name == tableResponseData.response[_pos].cards[1].type + "-" + tableResponseData.response[_pos].cards[1].rank)
				{
					localCardsImage[1].sprite = tElements._cards.cardImage[i];
				}
				if (tElements._cards.cardImage[i].name == tableResponseData.response[_pos].cards[2].type + "-" + tableResponseData.response[_pos].cards[2].rank)
				{
					localCardsImage[2].sprite = tElements._cards.cardImage[i];
				}
				if (viewCardButton.gameObject.activeInHierarchy)
				{
					viewCardButton.gameObject.SetActive(value: false);
				}
				/*if (seenGO.gameObject.activeInHierarchy)
				{
					seenGO.gameObject.SetActive(value: false);
				}*/
				cardStatus.text = "Blind";
			}
		}
		if (!_autoView)
		{
			return;
		}
		for (int j = 0; j < cardMainPanel.Length; j++)
		{
			Quaternion localRotation = cardMainPanel[j].localRotation;
			if (!(localRotation.y < 100f))
			{
				continue;
			}
			if (tElements.tableType == "limit" || tElements.tableType == "nolimit" || tElements.tableType == "private" || tElements.tableType == "OLC")
			{
				if (_playCardAnimation)
				{
					Sequence s = DOTween.Sequence();
					s.Append(cardMainPanel[j].DOLocalRotate(new Vector3(0f, 180f, 0f), 1f).SetEase(Ease.InOutBack, 2f)).AppendInterval(0.1f * (float)j).Append(cardMainPanel[j].transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f))
						.Append(cardMainPanel[j].transform.DOScale(Vector3.one, 0.5f));
				}
				else
				{
					Sequence s2 = DOTween.Sequence();
					s2.Append(cardMainPanel[j].DOLocalRotate(new Vector3(0f, 180f, 0f), 1f).SetEase(Ease.InOutBack, 2f));
				}
			}
			else if (tElements.tableType == "variation_limit" || tElements.tableType == "variation_nolimit")
			{
				Sequence s3 = DOTween.Sequence();
				s3.Append(cardMainPanel[j].DOLocalRotate(new Vector3(0f, 180f, 0f), 1f).SetEase(Ease.InOutBack, 2f)).AppendCallback(delegate
				{
					if (isVarCard[0])
					{
						isVarCard[0] = false;
						jokerCardsGO[0].SetActive(true);
						cardMainPanel[0].transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f);
						cardMainPanel[0].GetComponent<RectTransform>().DOShakeAnchorPos(2f, 3f, 10, 3f);
					}
					if (isVarCard[1])
					{
						isVarCard[1] = false;
						jokerCardsGO[1].SetActive(true);
						cardMainPanel[1].transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f);
						cardMainPanel[1].GetComponent<RectTransform>().DOShakeAnchorPos(2f, 3f, 10, 3f);
					}
					if (isVarCard[2])
					{
						isVarCard[2] = false;
						jokerCardsGO[2].SetActive(true);
						cardMainPanel[2].transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f);
						cardMainPanel[2].GetComponent<RectTransform>().DOShakeAnchorPos(2f, 3f, 10, 3f);
					}
				}).Append(cardMainPanel[j].transform.DOScale(Vector3.one, 0.5f));
			}
		}
	}

	public void AssignCardsAfterDelay(SocketIOEvent e, int _pos, bool _autoView, bool _playCardAnimation)
	{
		string data = e.data.ToString();
		Sequence s = DOTween.Sequence();
		s.PrependInterval(2f);
		s.AppendCallback(delegate
		{
			TableResponseData tableResponseData = TableResponseData.CreateFromJSON(data);
			if (e.data.GetField("response")[_pos].HasField("cards"))
			{
				if (tableResponseData.response[_pos].cards.Length > 0)
				{
					for (int i = 0; i < tElements._cards.cardImage.Length; i++)
					{
						if (e.data.GetField("response")[_pos].GetField("cards")[0].HasField("var"))
						{
							isVarCard[0] = true;
						}
						if (e.data.GetField("response")[_pos].GetField("cards")[1].HasField("var"))
						{
							isVarCard[1] = true;
						}
						if (e.data.GetField("response")[_pos].GetField("cards")[2].HasField("var"))
						{
							isVarCard[2] = true;
						}
						if (tElements._cards.cardImage[i].name == tableResponseData.response[_pos].cards[0].type + "-" + tableResponseData.response[_pos].cards[0].rank)
						{
							localCardsImage[0].sprite = tElements._cards.cardImage[i];
						}
						if (tElements._cards.cardImage[i].name == tableResponseData.response[_pos].cards[1].type + "-" + tableResponseData.response[_pos].cards[1].rank)
						{
							localCardsImage[1].sprite = tElements._cards.cardImage[i];
						}
						if (tElements._cards.cardImage[i].name == tableResponseData.response[_pos].cards[2].type + "-" + tableResponseData.response[_pos].cards[2].rank)
						{
							localCardsImage[2].sprite = tElements._cards.cardImage[i];
						}
						if (viewCardButton.gameObject.activeInHierarchy)
						{
							viewCardButton.gameObject.SetActive(false);
						}
						/*if (seenGO.gameObject.activeInHierarchy)
						{
							seenGO.gameObject.SetActive(value: false);
						}*/
						cardStatus.text = "Blind";
					}
				}
				if (_autoView)
				{
					for (int j = 0; j < cardMainPanel.Length; j++)
					{
						Quaternion localRotation = cardMainPanel[j].localRotation;
						if (localRotation.y < 100f)
						{
							if (tElements.tableType == "limit" || tElements.tableType == "nolimit" || tElements.tableType == "private" || tElements.tableType == "OLC")
							{
								if (_playCardAnimation)
								{
									Sequence s2 = DOTween.Sequence();
									s2.Append(cardMainPanel[j].DOLocalRotate(new Vector3(0f, 180f, 0f), 1f).SetEase(Ease.InOutBack, 2f)).AppendInterval(0.1f * (float)j).Append(cardMainPanel[j].transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f))
										.Append(cardMainPanel[j].transform.DOScale(Vector3.one, 0.5f));
								}
								else
								{
									Sequence s3 = DOTween.Sequence();
									s3.Append(cardMainPanel[j].DOLocalRotate(new Vector3(0f, 180f, 0f), 1f).SetEase(Ease.InOutBack, 2f));
								}
							}
							else if (tElements.tableType == "variation_limit" || tElements.tableType == "variation_nolimit")
							{
								Sequence s4 = DOTween.Sequence();
								s4.Append(cardMainPanel[j].DOLocalRotate(new Vector3(0f, 180f, 0f), 1f).SetEase(Ease.InOutBack, 2f)).AppendCallback(delegate
								{
									if (isVarCard[0])
									{
										isVarCard[0] = false;
										jokerCardsGO[0].SetActive(value: true);
										cardMainPanel[0].transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f);
										cardMainPanel[0].GetComponent<RectTransform>().DOShakeAnchorPos(2f, 3f, 10, 3f);
									}
									if (isVarCard[1])
									{
										isVarCard[1] = false;
										jokerCardsGO[1].SetActive(value: true);
										cardMainPanel[1].transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f);
										cardMainPanel[1].GetComponent<RectTransform>().DOShakeAnchorPos(2f, 3f, 10, 3f);
									}
									if (isVarCard[2])
									{
										isVarCard[2] = false;
										jokerCardsGO[2].SetActive(value: true);
										cardMainPanel[2].transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f);
										cardMainPanel[2].GetComponent<RectTransform>().DOShakeAnchorPos(2f, 3f, 10, 3f);
									}
								}).Append(cardMainPanel[j].transform.DOScale(Vector3.one, 0.5f));
							}
						}
					}
				}
			}
		});
	}

	public void AutoViewCards()
	{
		for (int i = 0; i < cardMainPanel.Length; i++)
		{
			Quaternion localRotation = cardMainPanel[i].localRotation;
			if (localRotation.y < 100f)
			{
				cardMainPanel[i].DOLocalRotate(new Vector3(0f, 180f, 0f), 1f).SetEase(Ease.InOutBack, 2f);
			}
		}
	}

	public void ActivePlayers()
	{
		if (!playerPanelGO.activeInHierarchy)
		{
			playerPanelGO.SetActive(value: true);
		}
	}

	public void RemovePlayer(bool _isFirstTime)
	{
		if (!_isFirstTime)
		{
			playerPanelGO.SetActive(false);
		}
		else
		{
			playerPanelGO.SetActive(false);
		}
		playerInviteGO.SetActive(false);
		profileImage.sprite = tElements.defaultSprite;
		//profileImageBlur.sprite = tElements.defaultSprite;
		isLocalPlayer = false;
		playerID = string.Empty;
		chips = string.Empty;
		forceSideShow = string.Empty;
		position = string.Empty;
		localPosition = string.Empty;
		amount = string.Empty;
		sittingTable = false;
		isFacebook = false;
		isActive = false;
		isBlind = false;
		isDealer = false;
		nameText.text = string.Empty;
		chipsText.text = string.Empty;
		cardsGO.SetActive(value: false);
		ResetChatHeads();
		ResetPlayer();
	}

	public void ResetPlayer()
	{
		dealerGO.SetActive(false);
		timerSlider.value = 0f;
		timerSlider.maxValue = maxTimer;
		timerColorSliderImage.color = normalColor;
		timerColorSliderImage.color = normalColor;
		//turnAnimationGO.SetActive(false);
		//turnAmountGo.SetActive(false);
		giftButton.SetActive(true);
		firstTimer = false;
		secondTimer = false;
		isVarCard[0] = false;
		isVarCard[1] = false;
		isVarCard[2] = false;
		playWinnerAnimation = true;
		//forceSSGO.transform.localScale = Vector3.zero;
		chipsCanvasGroup.alpha = 0f;
		jokerCardsGO[0].SetActive(false);
		jokerCardsGO[1].SetActive(false);
		jokerCardsGO[2].SetActive(false);
		localCardsImage[0].sprite = defaultCardSprite;
		localCardsImage[1].sprite = defaultCardSprite;
		localCardsImage[2].sprite = defaultCardSprite;
		winnerTextGO.SetActive(false);
		selectingVariationGO.SetActive(false);
		for (int i = 0; i < cardMainPanel.Length; i++)
		{
			cardMainPanel[i].localRotation = new Quaternion(0f, 0f, 0f, 0f);
		}
		acceptSideShowGO.SetActive(false);
		declineSideShowGO.SetActive(false);
		//seenGO.SetActive(value: false);
		//packGO.SetActive(value: false);
		cardStatus.text = "Blind";
		viewCardButton.gameObject.SetActive(false);
		winGO.SetActive(false);
		//winGO_Particles.SetActive(false);
		winnerTextImage.rectTransform.localScale = Vector3.zero;
		winnerTextImage.rectTransform.rotation = new Quaternion(0f, 0f, -180f, 0f);
		//DisablePlayerGlow();
		ResetPlayerTimer();
		ResetColor();
	}

	/*public void EnablePlayerGlow()
	{
		glowImageGO.SetActive(value: true);
		Invoke("DisablePlayerGlow", 3f);
	}

	public void DisablePlayerGlow()
	{
		glowImageGO.SetActive(value: false);
	}*/

	public void DisableAndEnableGiftIcon()
	{
		giftButton.SetActive(false);
		Invoke("EnableGiftButton", 5f);
	}

	private void EnableGiftButton()
	{
		if (!giftButton.activeInHierarchy)
		{
			giftButton.SetActive(true);
		}
	}

	public void CardsDistribute()
	{
		cardsGO.SetActive(true);
	}

	public void AssignTurnAmount(SocketIOEvent e, int _pos, bool _rejoin, bool _preAmount)
	{
		string data = e.data.ToString();
		TableResponseData tableResponseData = TableResponseData.CreateFromJSON(data);
		if (e.data.GetField("response")[_pos].HasField("amount"))
		{
			/*if (!turnAmountGo.activeInHierarchy)
			{
				turnAmountGo.SetActive(true);
			}*/
			if (_preAmount)
			{
				turnAmountText.text = GameManager.Rs + GameManager.Instance.format.FormatNumber(long.Parse(tableResponseData.response[_pos].preAmount));
			}
			else
			{
				turnAmountText.text = GameManager.Rs + GameManager.Instance.format.FormatNumber(long.Parse(tableResponseData.response[_pos].amount));
			}
			/*if (!_rejoin)
			{
				turnTypeText.text = ((!isBlind) ? "Chaal" : ("Blind " + tableResponseData.response[_pos].blind));
			}
			else
			{
				turnTypeText.text = ((!isBlind) ? "Chaal" : "Blind ");
			}*/
		}
	}

	public void ResetPlayerTimer()
	{
		isTimerRunning = false;
		firstTimer = false;
		timer = 0f;
		timerSlider.value = 0f;
		timerColorSliderImage.color = normalColor;
		timerColorSliderImage.color = normalColor;
		//turnAnimationGO.SetActive(false);
	}

	public void PackColor()
	{
		for (int i = 0; i < blackStrip.Length; i++)
		{
			blackStrip[i].color = tElements.packColor;
		}
		for (int j = 0; j < blackSprites.Length; j++)
		{
			blackSprites[j].color = tElements.packColor;
		}
	}

	public void ResetColor()
	{
		for (int i = 0; i < blackStrip.Length; i++)
		{
			blackStrip[i].color = tElements.playColor;
		}
		for (int j = 0; j < blackSprites.Length; j++)
		{
			blackSprites[j].color = tElements.playColor;
		}
	}

	public void Winner()
	{
		tElements.UserPackLocally();
		ResetColor();
	}

	public void Winner_Card(SocketIOEvent e, int _pos)
	{
		ResetColor();
		winGO.SetActive(true);
		//winGO_Particles.SetActive(true);
		winnerTextImage.rectTransform.DOScale(Vector3.one, 1f);
		winnerTextImage.rectTransform.DORotate(Vector3.zero, 1f).OnComplete(delegate
		{
			winnerTextImage.rectTransform.DOScale(Vector3.zero, 0.5f).SetDelay(0.5f);
		});
		string data = e.data.ToString();
		playWinnerAnimation = false;
		TableResponseData tableResponseData = TableResponseData.CreateFromJSON(data);
		if (e.data.GetField("response")[_pos].HasField("msg"))
		{
			if (!winnerTextGO.activeInHierarchy)
			{
				winnerTextGO.SetActive(true);
			}
			/*if (turnAmountGo.activeInHierarchy)
			{
				turnAmountGo.SetActive(false);
			}*/
			winningCardText.text = tableResponseData.response[_pos].msg;
		}
		ResetPlayerTimer();
	}

	public void MoveChips(GameObject _firstPos, GameObject _lastPos, long _totalAmount, long _amount)
	{
		chipsCanvasGroup.transform.position = _firstPos.transform.position;
		chipsCanvasGroup.alpha = 1f;
		chipsMovementText.text = GameManager.Rs + GameManager.Instance.format.FormatNumber(_amount);
		chipsCanvasGroup.transform.DOMove(_lastPos.transform.position, 0.8f).SetEase(Ease.OutSine).OnComplete(delegate
		{
			ChipsMovementCallback(_totalAmount);
		});
	}

	private void ChipsMovementCallback(long _amount)
	{
		chipsCanvasGroup.alpha = 0f;
		tElements.SetTotalAmountOnTable(_amount);
	}

	private IEnumerator MoveItemsObject(GameObject firstPos, GameObject lastPos)
	{
		isChipsMoving = true;
		float timeSinceStarted = 0f;
		chipsCanvasGroup.transform.position = firstPos.transform.position;
		while (true)
		{
			timeSinceStarted += Time.deltaTime;
			chipsCanvasGroup.transform.position = Vector3.Lerp(firstPos.transform.position, lastPos.transform.position, Mathf.SmoothStep(0f, 1f, timeSinceStarted));
			if (chipsCanvasGroup.transform.position == lastPos.transform.position)
			{
				break;
			}
			yield return null;
		}
		chipsCanvasGroup.alpha = 0f;
		chipsCanvasGroup.transform.position = firstPos.transform.position;
		isChipsMoving = false;
		StopCoroutine("MoveItemsObject");
	}

	public void DisplayChatHead(string _message)
	{
		if (IsInvoking("ResetChatHeads"))
		{
			CancelInvoke("ResetChatHeads");
		}
		chatText.text = _message;
		chatHeadPanel.SetActive(true);
		Invoke("ResetChatHeads", 5f);
	}

	public void ResetChatHeads()
	{
		chatHeadPanel.SetActive(false);
	}

	public void OpenItemsWindow()
	{
		AudioManager.instance.PlaySound("click");
		GameManager.Instance.secondPlayerNameForGifts = playerID;
		networkManager.chatElement.OpenChatCanvas();
		/*if (isLocalPlayer)
		{
			tElements.emojiGO.GetComponent<Animator>().SetBool("isopen", true);
		}
		else
		{
			tElements.giftsGO.GetComponent<Animator>().SetBool("isopen", true);
		}*/
	}

	public void OpenPlayerInfo()
    {
		tElements.playerInfo.OpenPlayerInfo(playerID, profileImage.sprite, nameText.text, chips, "NO");
	}

	private void Update()
	{
		if (!isTimerRunning)
		{
			return;
		}
		timer += Time.deltaTime;
		timerSlider.value = timer;
		//turnAnimationGO.SetActive(true);
		if (timerSlider.value >= maxTimer * 0.7f && firstTimer && timerSlider.value < maxTimer * 0.9f)
		{
			if (isLocalPlayer)
			{
				AudioManager.instance.PlayVibration();
				AudioManager.instance.PlaySound("timer-tick-tok");
			}
			firstTimer = false;
			timerColorSliderImage.color = Color.black;
			timerColorSliderImage.DOColor(alertColor, 0.5f).SetLoops(10, LoopType.Yoyo);
			timerColorSliderImage.color = Color.black;
			timerColorSliderImage.DOColor(alertColor, 0.5f).SetLoops(10, LoopType.Yoyo);
		}
		else if (timerSlider.value >= maxTimer * 0.9f && secondTimer)
		{
			secondTimer = false;
			timerColorSliderImage.color = alertColor;
			timerColorSliderImage.DOColor(Color.black, 0.15f).SetLoops(50, LoopType.Yoyo);
			timerColorSliderImage.color = alertColor;
			timerColorSliderImage.DOColor(Color.black, 0.15f).SetLoops(10, LoopType.Yoyo);
		}
		else if (firstTimer && secondTimer)
		{
			timerColorSliderImage.color = normalColor;
			timerColorSliderImage.color = normalColor;
		}
	}
}
