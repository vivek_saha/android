using UnityEngine;

public class SendEmailToSupport : MonoBehaviour
{
	public void SendEmail()
	{
		string text = "info@sahajanandinfotech.com";
		string text2 = MyEscapeURL("Game ID : " + GameManager.Instance.id);
		string text3 = MyEscapeURL(string.Empty);
		Application.OpenURL("mailto:" + text + "?subject=" + text2 + "&body=" + text3);
	}

	private string MyEscapeURL(string url)
	{
		return WWW.EscapeURL(url).Replace("+", "%20");
	}
}
