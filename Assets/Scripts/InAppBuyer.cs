using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InAppBuyer : MonoBehaviour
{
	public int chips;
	public string product_id;
	public TextMeshProUGUI chips_text;
	public TextMeshProUGUI discount_percent;
	public TextMeshProUGUI discount_chips_text;
	public Text price_text;
	private InAppValues inAppValues;

	public void SetIAPData(int chips, string product_id, string chips_text, string price_text, InAppValues iav)
    {
		this.chips = chips;
		this.product_id = product_id;
		this.chips_text.text = chips_text;
		this.price_text.text = price_text;
		this.inAppValues = iav;
	}

	public void SetIAPData(int chips, string product_id, int discount_chips, string discount_percent, string price_text, InAppValues iav)
    {
		this.chips = chips;
		this.product_id = product_id;
		chips_text.text = GameManager.Instance.format.FormatNumberOnTable(chips);
		this.discount_percent.text = discount_percent;
		this.discount_chips_text.text = GameManager.Instance.format.FormatNumberOnTable(discount_chips);
		this.price_text.text = price_text;
		this.inAppValues = iav;
	}

	public void InAppBuy()
	{
		inAppValues.PurchessItem(product_id);
	}
}
