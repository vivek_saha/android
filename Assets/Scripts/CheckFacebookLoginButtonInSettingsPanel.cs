using Facebook.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CheckFacebookLoginButtonInSettingsPanel : MonoBehaviour
{
	public Text fbLoginText;

	private void OnEnable()
	{
		if (GameManager.Instance.isFacebookLogin)
		{
			GameManager.Instance.LanguageConverter(fbLoginText, "Logout with Facebook");
		}
		else
		{
			GameManager.Instance.LanguageConverter(fbLoginText, "Login with Facebook");
		}
	}

	public void SettingsLogin()
	{
		GameManager.Instance.loadingCanvas.LoadingState(true, false);
		GameManager.Instance.CloseSettingCanvas();
		PlayerPrefs.SetFloat("Facebook", 0f);
		if (GameManager.Instance.isFacebookLogin)
		{
			SceneManager.LoadScene(URL.Instance.LOGIN_SCENE);
			FB.LogOut();
		}
		else
		{
			GameManager.Instance.loginManager.LoginWithFacebook();
		}
	}
}
