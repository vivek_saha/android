using UnityEngine;

public class OpenClosePopup : MonoBehaviour
{
	private Animator anim;

	private void Awake()
	{
		anim = GetComponent<Animator>();
	}

	public void OpenPopup()
	{
		AudioManager.instance.PlaySound("click");
		anim.SetBool("isopen", true);
	}

	public void ClosePopup()
	{
		AudioManager.instance.PlaySound("close-popup");
		anim.SetBool("isopen", false);
	}

	public void OpenStore()
	{
		Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
	}

}
