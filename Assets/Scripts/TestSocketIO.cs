using SocketIO;
using System.Collections;
using UnityEngine;

public class TestSocketIO : MonoBehaviour
{
	private SocketIOComponent socket;

	public void Start()
	{
		GameObject gameObject = GameObject.Find("SocketIO");
		socket = gameObject.GetComponent<SocketIOComponent>();
		socket.On("open", TestOpen);
		socket.On("boop", TestBoop);
		socket.On("error", TestError);
		socket.On("close", TestClose);
		StartCoroutine("BeepBoop");
	}

	private IEnumerator BeepBoop()
	{
		yield return new WaitForSeconds(1f);
		socket.Emit("beep");
		yield return new WaitForSeconds(3f);
		socket.Emit("beep");
		yield return new WaitForSeconds(2f);
		socket.Emit("beep");
		yield return null;
		socket.Emit("beep");
		socket.Emit("beep");
	}

	public void TestOpen(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Open received: " + e.name + " " + e.data);
	}

	public void TestBoop(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Boop received: " + e.name + " " + e.data);
		if (e.data != null)
		{
			Debug.Log("#####################################################THIS: " + e.data.GetField("this").str + "#####################################################");
		}
	}

	public void TestError(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Error received: " + e.name + " " + e.data);
	}

	public void TestClose(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Close received: " + e.name + " " + e.data);
	}
}
