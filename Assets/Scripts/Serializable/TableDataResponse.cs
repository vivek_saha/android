using System;
using UnityEngine;

[Serializable]
public class TableDataResponse
{
	public string type;

	public string[] bootAmount;

	public static TableDataResponse CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<TableDataResponse>(data);
	}
}
