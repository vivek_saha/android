using EasyMobile;
using System;
using TMPro;
using UniRx;
using UnityEngine;
public class LoadLevel : MonoBehaviour
{
	[SerializeField]
	private TextMeshProUGUI versionText;

	[SerializeField]
	private GameObject noInternetPanel;

	[SerializeField]
	private GameObject updateGame;
	[SerializeField]
	private TextMeshProUGUI updateText;
	[SerializeField]
	private GameObject maintenancePanel;
	[SerializeField]
	private TextMeshProUGUI maintenanceText;

	private bool flag1;


	private void Start()
	{
		versionText.text = "V-" + GameManager.Instance.versionNumber;
		GameManager.Instance.loadingCanvas.LoadingState(false, false);
	}

    private void Update()
    {
        if (!flag1)
        {
			if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork || Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
			{
				flag1 = true;
				VersionCheck();
			}
			else
			{
				flag1 = true;
				OpenNoInternetCanvas();
			}
		}

	}

    public void QuitApplication()
    {
		Application.Quit();
    }

    public void ReconnectInternetCanvas()
	{
		noInternetPanel.SetActive(false);
		flag1 = false;
	}

	private void OpenNoInternetCanvas()
    {
		noInternetPanel.SetActive(true);
	}

	private void VersionCheck()
	{
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.VERSION_CHECK, string.Empty)).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("VERSION_CHECK " + x);
			PlayerData playerData = PlayerData.CreateFromJSON(x);

			if (playerData.status == "success")
			{
				if (GameManager.Instance.versionNumber == playerData.response.version)
				{
					GameManager.Instance.fbChips = playerData.response.fbChips;
					MaintenanceMode();
				}
				else
				{
					// Update Panel show
					updateText.text = playerData.response.message;
					updateGame.SetActive(true);
					URL.Instance.DebugPrint("Please Update the version.");
				}
			}
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			Invoke("VersionCheck", 2f);
		});
	}

	private void MaintenanceMode()
	{
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.MAINTENANCE_CHECK, string.Empty)).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("MAINTENANCE_CHECK " + x);
			PlayerData playerData = PlayerData.CreateFromJSON(x);
			if (playerData.status == "success")
			{
				// Maintenance Panel show
				maintenanceText.text = playerData.response.message;
				maintenancePanel.SetActive(true);
				NativeUI.ShowToast("Maintenance is on.", true);
			}
			else
			{
				GameManager.Instance.appURL = playerData.response.app_link;
				GameManager.Instance.privacyURL = playerData.response.privacy_policy;

				GameManager.Instance.adsManager.firstAdName = playerData.response.first_ad_name;
				GameManager.Instance.adsManager.admob_reward_id = playerData.response.admob_reward_id;
				GameManager.Instance.adsManager.admob_interstitial_id = playerData.response.admob_interstitial_id;
				GameManager.Instance.adsManager.fb_reward_id = playerData.response.fb_reward_id;
				GameManager.Instance.adsManager.fb_interstitial_id = playerData.response.fb_interstitial_id;
				GameManager.Instance.adsManager.unity_app_id = playerData.response.unity_app_id;
				GameManager.Instance.adsManager.reward_video_amount = playerData.response.reward_video_amount;

				GameManager.Instance.StartManager();
			}
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			Invoke("MaintenanceMode", 2f);
		});
	}

}
