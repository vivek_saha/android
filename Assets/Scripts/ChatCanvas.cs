﻿using UnityEngine;
using UnityEngine.UI;

public class ChatCanvas : MonoBehaviour
{
    public NetworkManager networkManager;

    public GameObject panel;
    public InputField input;
    public Transform chatParent;
    public Transform chatPrefab;


    public void OpenChatCanvas()
    {
        panel.SetActive(true);
    }

    public void CloseChatCanvas()
    {
        panel.SetActive(false);
    }

    public void CreateChat(string msg, bool is_local)
    {
        Transform chatObj = Instantiate(chatPrefab, chatParent);

        if (is_local)
        {
            Text chatMsg = chatObj.Find("local/Text").GetComponent<Text>();
            chatMsg.text = msg;
            chatObj.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            Text chatMsg = chatObj.Find("other/Text").GetComponent<Text>();
            chatMsg.text = msg;
            chatObj.GetChild(1).gameObject.SetActive(true);
        }
    }

    public void SendButton()
    {
        string msg = input.text;
        if (!string.IsNullOrEmpty(msg))
        {
            networkManager.Send_Chat(msg);
            input.text = "";
        }
    }

    public void SendMessageDirect(Text text_msg)
    {
        networkManager.Send_Chat(text_msg.text);
    }

}
