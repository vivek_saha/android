using UnityEngine;
using TMPro;

public class Settings : MonoBehaviour
{
	public Sprite defaultSprite;
	public TextMeshProUGUI versionText;


    private void Start()
    {
		versionText.text = "Version: " + GameManager.Instance.versionNumber;
	}

    public void PrivacyPolicy()
    {
		Application.OpenURL(GameManager.Instance.privacyURL);
	}

	public void RateButton()
	{
		Application.OpenURL(GameManager.Instance.appURL);
	}
}
