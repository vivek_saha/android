using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoadingSliderValueChange : MonoBehaviour
{
	public Slider slider;

	public TextMeshProUGUI text;

	public void OnValueChange()
	{
		text.text = ((int)(slider.value * 100f)).ToString() + "%";
	}
}
