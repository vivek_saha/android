using System;
using UnityEngine;

[Serializable]
public class PlayerDataResponse
{
	public string playerId;

	public string chips;

	public bool sittingTable;

	public bool isActive;

	public bool hasCards;

	public int position;

	public int turn;

	public bool isBlind;

	public string[] cards;

	public string amount;

	public string timer;

	public long totalAmount;

	public string msg;

	public int blind;

	public string chaalAmountLimit;

	public string bootedAmountChips;

	public string tableAmountLimit;

	public string type;

	public string forcesideshow;

	public string join_number;

	public string variation;

	public string user_id;

	public string facebook;

	public string name;

	public string uid;

	public string tempname;

	public string timediff;

	public string lastbonus;

	public string reward;

	public string tbid;

	public string host;

	public string port;

	public string proto;

	public string winChips;

	public string version;

	public string seconds;

	public string rank;

	public string winAmount;

	public string profile;

	public string deviceId;

	//public string vip;

	public string maxSlotChaalAmount;

	//public string vipDetail;

	public string fbChips;

	//public string imageUrl;

	//public string promotionalURLString;

	public string fbMultiMsg;

	public string message;

	//public SlotsDataResponse slotRewards;

	public string inviteCode;

	public string inviteChips;

	public string app_link;
	public string privacy_policy;

	public string first_ad_name;
	public string admob_reward_id;
	public string admob_interstitial_id;
	public string fb_reward_id;
	public string fb_interstitial_id;
	public string unity_app_id;
	public int reward_video_amount;


	public static PlayerDataResponse CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<PlayerDataResponse>(data);
	}
}
