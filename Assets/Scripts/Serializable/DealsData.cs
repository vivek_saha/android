using System;
using UnityEngine;

[Serializable]
public class DealsData
{
	public string status;

	public string responseMessage;

	public string popupMessageTitle;

	public string popupMessageBody;

	public DealsDataResponse[] response;

	public static DealsData CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<DealsData>(data);
	}
}
