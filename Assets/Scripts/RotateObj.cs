using DG.Tweening;
using UnityEngine;

public class RotateObj : MonoBehaviour
{
	public float rotateAngle;

	public float rotateTime;

	public Vector3 scale;

	public float scaleTime;

	private void Start()
	{
		base.transform.DORotate(new Vector3(0f, 0f, rotateAngle), rotateTime, RotateMode.LocalAxisAdd).SetLoops(-1, LoopType.Incremental);
		base.transform.DOScale(scale, scaleTime).SetLoops(-1, LoopType.Yoyo);
	}
}
