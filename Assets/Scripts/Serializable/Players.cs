using System;
using UnityEngine;

[Serializable]
public class Players
{
	public string playerID;

	public string localPosition;

	public GameObject playerInviteObject;

	public GameObject playerPanelObject;

	public PlayerController pController;
}
