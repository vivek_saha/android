using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

[Serializable]
[ExecuteInEditMode]
[AddComponentMenu("2DxFX/Advanced Lightning/Hologram")]
public class _2dxFX_AL_Hologram : MonoBehaviour
{
	[HideInInspector]
	public Material ForceMaterial;

	[HideInInspector]
	public bool ActiveChange = true;

	[HideInInspector]
	public bool AddShadow = true;

	[HideInInspector]
	public bool ReceivedShadow;

	[HideInInspector]
	public int BlendMode;

	private string shader = "2DxFX/AL/Hologram";

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Alpha = 1f;

	[HideInInspector]
	[Range(0f, 4f)]
	public float Distortion = 1f;

	[HideInInspector]
	private float _TimeX;

	[Range(0f, 3f)]
	[HideInInspector]
	public float Speed = 1f;

	[HideInInspector]
	public int ShaderChange;

	private Material tempMaterial;

	private Material defaultMaterial;

	private Image CanvasImage;

	private void Awake()
	{
		if (base.gameObject.GetComponent<Image>() != null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
	}

	private void Start()
	{
		ShaderChange = 0;
	}

	public void CallUpdate()
	{
		Update();
	}

	private void Update()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (ShaderChange == 0 && ForceMaterial != null)
		{
			ShaderChange = 1;
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
			ForceMaterial.hideFlags = HideFlags.None;
			ForceMaterial.shader = Shader.Find(shader);
		}
		if (ForceMaterial == null && ShaderChange == 1)
		{
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
			ShaderChange = 0;
		}
		if (!ActiveChange)
		{
			return;
		}
		if (base.gameObject.GetComponent<SpriteRenderer>() != null)
		{
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Alpha", 1f - _Alpha);
			if (_2DxFX.ActiveShadow && AddShadow)
			{
				GetComponent<Renderer>().shadowCastingMode = ShadowCastingMode.On;
				if (ReceivedShadow)
				{
					GetComponent<Renderer>().receiveShadows = true;
					GetComponent<Renderer>().sharedMaterial.renderQueue = 2450;
					GetComponent<Renderer>().sharedMaterial.SetInt("_Z", 1);
				}
				else
				{
					GetComponent<Renderer>().receiveShadows = false;
					GetComponent<Renderer>().sharedMaterial.renderQueue = 3000;
					GetComponent<Renderer>().sharedMaterial.SetInt("_Z", 0);
				}
			}
			else
			{
				GetComponent<Renderer>().shadowCastingMode = ShadowCastingMode.Off;
				GetComponent<Renderer>().receiveShadows = false;
				GetComponent<Renderer>().sharedMaterial.renderQueue = 3000;
				GetComponent<Renderer>().sharedMaterial.SetInt("_Z", 0);
			}
			if (BlendMode == 0)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 0);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 1);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 10);
			}
			if (BlendMode == 1)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 0);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 1);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 1);
			}
			if (BlendMode == 2)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 2);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 1);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 2);
			}
			if (BlendMode == 3)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 4);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 1);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 1);
			}
			if (BlendMode == 4)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 2);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 1);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 1);
			}
			if (BlendMode == 5)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 4);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 10);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 10);
			}
			if (BlendMode == 6)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 0);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 2);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 10);
			}
			if (BlendMode == 7)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 0);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 4);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 1);
			}
			if (BlendMode == 8)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 2);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 7);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 2);
			}
			_TimeX += Time.deltaTime * Speed;
			if (_TimeX > 100f)
			{
				_TimeX = 0f;
			}
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Distortion", Distortion);
			GetComponent<Renderer>().sharedMaterial.SetFloat("_TimeX", 1f + _TimeX);
		}
		else if (base.gameObject.GetComponent<Image>() != null)
		{
			CanvasImage.material.SetFloat("_Alpha", 1f - _Alpha);
			_TimeX += Time.deltaTime * Speed;
			if (_TimeX > 100f)
			{
				_TimeX = 0f;
			}
			CanvasImage.material.SetFloat("_Distortion", Distortion);
			CanvasImage.material.SetFloat("_TimeX", 1f + _TimeX);
		}
	}

	private void OnDestroy()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (Application.isPlaying || !Application.isEditor)
		{
			return;
		}
		if (tempMaterial != null)
		{
			UnityEngine.Object.DestroyImmediate(tempMaterial);
		}
		if (base.gameObject.activeSelf && defaultMaterial != null)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnDisable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (base.gameObject.activeSelf && defaultMaterial != null)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnEnable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (defaultMaterial == null)
		{
			defaultMaterial = new Material(Shader.Find("Sprites/Default"));
		}
		if (ForceMaterial == null)
		{
			ActiveChange = true;
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
		}
		else
		{
			ForceMaterial.shader = Shader.Find(shader);
			ForceMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
		}
	}
}
