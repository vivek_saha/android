using System;

[Serializable]
public class GiftItemsJSON
{
	public string type;

	public string firstPerson;

	public string secondPerson;

	public string itemNumber;

	public GiftItemsJSON(string _type, string _firstPerson, string _secondPerson, string _itemNumber)
	{
		type = _type;
		firstPerson = _firstPerson;
		secondPerson = _secondPerson;
		itemNumber = _itemNumber;
	}
}
