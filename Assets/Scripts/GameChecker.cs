using SocketIO;
using System;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameChecker : MonoBehaviour
{
	private NetworkManager networkManager;

	private SocketIOComponent socket;

	[SerializeField]
	private GameObject reconnectPopup;

	[SerializeField]
	private Text messageOnPopup;

	[SerializeField]
	private Button reconnectingButton;

	private bool canCheckForSocket;

	private void Start()
	{
		canCheckForSocket = false;
		//InvokeRepeating("SpeedCheck", 1f, 5f);
	}

	private void Update()
	{
		InternetChecker();
	}

	private void ReconnectPopupState(bool _state, string _message)
	{
		messageOnPopup.text = _message;
		reconnectPopup.SetActive(_state);
	}

	private void InternetChecker()
	{
		if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork || Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
		{
			if (GameManager.Instance.isConnectedToInternet)
			{
				if (SceneManager.GetActiveScene().name == URL.Instance.TABLE_SCENE)
				{
					if (!GameManager.Instance.isSocketConnectedToServer && canCheckForSocket)
					{
						canCheckForSocket = false;
						ReconnectPopupState(true, "Connection failed from Server!");
						GameManager.Instance.loadingCanvas.LoadingState(false, false);
					}
					else if (GameManager.Instance.isSocketConnectedToServer)
					{
						canCheckForSocket = true;
					}
					else
					{
						canCheckForSocket = false;
					}
					reconnectingButton.interactable = true;
					if (GameManager.Instance.canDoAutoLogin)
					{
						ReconnectButtonClicker();
					}
				}
				else if (SceneManager.GetActiveScene().name == URL.Instance.MAIN_MENU_SCENE)
				{
					reconnectingButton.interactable = true;
					canCheckForSocket = false;
				}
				else if (SceneManager.GetActiveScene().name == URL.Instance.LOADING_SCENE || SceneManager.GetActiveScene().name == URL.Instance.LOGIN_SCENE)
				{
					ReconnectPopupState(false, string.Empty);
					canCheckForSocket = false;
				}
			}
			else
			{
				reconnectingButton.interactable = false;
				GameManager.Instance.isSocketConnectedToServer = false;
				canCheckForSocket = false;
				GameManager.Instance.loadingCanvas.LoadingState(false, false);
				ReconnectPopupState(true, "Please connect to Internet!");
				GameManager.Instance.canDoAutoLogin = true;
			}
		}
		else
		{
			reconnectingButton.interactable = false;
			GameManager.Instance.isSocketConnectedToServer = false;
			canCheckForSocket = false;
			GameManager.Instance.loadingCanvas.LoadingState(false, false);
			ReconnectPopupState(true, "Please connect to Internet!");
			GameManager.Instance.canDoAutoLogin = true;
		}
	}

	public void ReconnectButtonClicker()
	{
		GameManager.Instance.canDoAutoLogin = false;
		ReconnectPopupState(false, string.Empty);
		GameManager.Instance.isSocketConnectedToServer = false;
		canCheckForSocket = false;

		/*if (GameManager.Instance.isFacebookLogin)
		{
			GameManager.Instance.loginManager.LoginWithFacebook();
		}
		else
		{
			GameManager.Instance.loginManager.GuestLoginCO();
		}*/
	}

	/*private void SpeedCheck()
	{
		DateTime dt = DateTime.Now;
		ObservableWWW.GetWWW("http://google.com").Subscribe((Action<WWW>)delegate(WWW x)
		{
			byte[] bytes = x.bytes;
			DateTime now = DateTime.Now;
			GameManager.Instance.internetSpeed = Math.Round((double)(bytes.Length / 1024) / (now - dt).TotalSeconds, 2);
			GameManager.Instance.isConnectedToInternet = true;
		}, (Action<Exception>)delegate
		{
			URL.Instance.DebugPrint("Not Connected to Internet");
			GameManager.Instance.isConnectedToInternet = false;
		});
	}*/

}
