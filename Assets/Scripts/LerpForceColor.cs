using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LerpForceColor : MonoBehaviour
{
	public Image shadowImage;

	public Color newColor;

	private void Awake()
	{
	}

	private void Start()
	{
		shadowImage.DOColor(newColor, 0.5f).SetLoops(-1, LoopType.Yoyo);
	}
}
