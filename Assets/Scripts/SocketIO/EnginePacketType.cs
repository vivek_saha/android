namespace SocketIO
{
	public enum EnginePacketType
	{
		UNKNOWN = -1,
		OPEN,
		CLOSE,
		PING,
		PONG,
		MESSAGE,
		UPGRADE,
		NOOP
	}
}
