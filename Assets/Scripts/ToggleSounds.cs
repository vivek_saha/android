using UnityEngine;
using UnityEngine.UI;

public class ToggleSounds : MonoBehaviour
{
	[SerializeField]
	private Sprite onSprite;

	[SerializeField]
	private Sprite offSprite;

	[SerializeField]
	private Image buttonImage;

	private void OnEnable()
	{
		if (PlayerPrefs.GetInt("Sound") == 0)
		{
			buttonImage.sprite = offSprite;
		}
		else
		{
			buttonImage.sprite = onSprite;
		}
	}

	public void ToggleClick()
	{
		if (PlayerPrefs.GetInt("Sound") == 0)
		{
			PlayerPrefs.SetInt("Sound", 1);
			buttonImage.sprite = onSprite;
			AudioManager.instance.PlaySound("click");
		}
		else
		{
			PlayerPrefs.SetInt("Sound", 0);
			buttonImage.sprite = offSprite;
		}
	}
}
