﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleVibration : MonoBehaviour
{
	[SerializeField]
	private Sprite onSprite;

	[SerializeField]
	private Sprite offSprite;

	[SerializeField]
	private Image buttonImage;

	private void OnEnable()
	{
		if (PlayerPrefs.GetInt("Vibration") == 0)
		{
			buttonImage.sprite = offSprite;
		}
		else
		{
			buttonImage.sprite = onSprite;
		}
	}

	public void ToggleClick()
	{
		if (PlayerPrefs.GetInt("Vibration") == 0)
		{
			PlayerPrefs.SetInt("Vibration", 1);
			buttonImage.sprite = onSprite;
			AudioManager.instance.PlaySound("click");
		}
		else
		{
			PlayerPrefs.SetInt("Vibration", 0);
			buttonImage.sprite = offSprite;
		}
	}
}
