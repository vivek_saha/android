using Facebook.Unity;
using System.Collections.Generic;
using UnityEngine;

public class Sharing : MonoBehaviour
{
	private string _textShare = "Hey! Install Teen Patti, #1 Card Game. Download now.";/*http://bit.ly/tptgame*/

	public void Share()
	{
		GameManager.ShareMessage(_textShare);
	}

	public void FBShare()
	{
		FB.AppRequest(_textShare, null, new List<object>
		{
			"app_users"
		}, null, null, null, null, delegate(IAppRequestResult result)
		{
			Debug.Log(result.RawResult);
		});
	}

	public void WhatsappSharing()
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject androidJavaObject = new AndroidJavaObject("android.content.Intent");
		androidJavaObject.Call<AndroidJavaObject>("setAction", new object[1]
		{
			androidJavaClass.GetStatic<string>("ACTION_SEND")
		});
		androidJavaObject.Call<AndroidJavaObject>("setType", new object[1]
		{
			"text/plain"
		});
		androidJavaObject.Call<AndroidJavaObject>("setPackage", new object[1]
		{
			"com.whatsapp"
		});
		androidJavaObject.Call<AndroidJavaObject>("putExtra", new object[2]
		{
			androidJavaClass.GetStatic<string>("EXTRA_TEXT"),
			_textShare
		});
		AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject @static = androidJavaClass2.GetStatic<AndroidJavaObject>("currentActivity");
		@static.Call("startActivity", androidJavaObject);
	}

	private void startService(string packageName)
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaClass androidJavaClass2 = new AndroidJavaClass(packageName);
		AndroidJavaObject androidJavaObject = new AndroidJavaObject("android.content.Intent");
		@static.CallStatic("startActivity", androidJavaObject);
	}

	protected void HandleResult(IResult result)
	{
		if (result == null)
		{
			URL.Instance.DebugPrint("Null Response\n");
		}
		else if (!string.IsNullOrEmpty(result.Error))
		{
			URL.Instance.DebugPrint("Error Response:\n" + result.Error);
		}
		else if (result.Cancelled)
		{
			URL.Instance.DebugPrint("Cancelled Response:\n" + result.RawResult);
		}
		else if (!string.IsNullOrEmpty(result.RawResult))
		{
			URL.Instance.DebugPrint("Success Response:\n" + result.RawResult);
		}
		else
		{
			URL.Instance.DebugPrint("Empty Response\n");
		}
	}
}
