using System;
using UnityEngine;

[Serializable]
public class StoreDataResponse
{
	public string orderId;

	public string packageName;

	public string productId;

	public string purchaseTime;

	public string purchaseState;

	public string purchaseToken;

	public string signature;

	public static TableResponseDataResponse CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<TableResponseDataResponse>(data);
	}
}
