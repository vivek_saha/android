using TMPro;
using UnityEngine;

public class SelectVariationText : MonoBehaviour
{
	private TextMeshProUGUI varText;

	private int varNum;

	private void Awake()
	{
		varText = GetComponent<TextMeshProUGUI>();
	}

	private void OnEnable()
	{
		InvokeRepeating("RepeatText", 0f, 0.2f);
	}

	private void OnDisable()
	{
		CancelInvoke("RepeatText");
	}

	private void RepeatText()
	{
		varNum++;
		if (varNum == 1)
		{
			varText.text = "Selecting a Variation";
			return;
		}
		if (varNum == 2)
		{
			varText.text = "Selecting a Variation.";
			return;
		}
		if (varNum == 3)
		{
			varText.text = "Selecting a Variation..";
			return;
		}
		if (varNum == 4)
		{
			varText.text = "Selecting a Variation...";
			return;
		}
		varText.text = "Selecting a Variation";
		varNum = 0;
	}
}
