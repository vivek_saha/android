using UnityEngine;

public class EnableGameObject : MonoBehaviour
{
	public GetButtonList getButtonList;

	private void OnEnable()
	{
		getButtonList.EnableButtons();
	}
}
