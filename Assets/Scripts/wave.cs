using UnityEngine;

public class wave : MonoBehaviour
{
	public Material mat;

	private void Update()
	{
		float x = Time.time * 0.5f;
		mat.mainTextureOffset = new Vector2(x, 0f);
	}
}
