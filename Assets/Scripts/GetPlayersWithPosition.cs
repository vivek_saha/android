using UnityEngine;

public class GetPlayersWithPosition : MonoBehaviour
{
	public Players[] _players = new Players[5];

	private void Awake()
	{
		ResetPlayers();
	}

	private void ResetPlayers()
	{
		for (int i = 0; i < _players.Length; i++)
		{
			_players[i].playerPanelObject.SetActive(value: false);
		}
	}
}
