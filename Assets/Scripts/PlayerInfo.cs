﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour
{
    [Header("UI")]
    public Image profileImg;
    public Text nameText;
    public Text coinText;
    public Text handWonText;

    [Header("Profile Data")]
    public string userID;
    public Sprite userSprite;
    public string userName;
    public string userCoin;
    public string userHandWon;


    public void CloseButton()
    {
        gameObject.SetActive(false);
    }

    public void OpenPlayerInfo(string uID, Sprite uSprite, string uName, string uCoin, string uHandWon)
    {
        userID = uID;
        userSprite = uSprite;
        userName = uName;
        userCoin = uCoin;
        userHandWon = uHandWon;

        profileImg.sprite = userSprite;
        nameText.text = userName;
        coinText.text = userCoin;
        handWonText.text = userHandWon;

        gameObject.SetActive(true);
    }

}
