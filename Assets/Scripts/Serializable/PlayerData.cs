using System;
using UnityEngine;

[Serializable]
public class PlayerData
{
	public string status;

	public string responseMessage;

	public string popupMessageTitle;

	public string popupMessageBody;

	public PlayerDataResponse response;

	public static PlayerData CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<PlayerData>(data);
	}
}
