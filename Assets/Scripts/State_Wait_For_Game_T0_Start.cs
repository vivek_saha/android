using UnityEngine;

public class State_Wait_For_Game_T0_Start : MonoBehaviour
{
	[HideInInspector]
	public PlayerElements playerElements;

	private void Awake()
	{
		playerElements = GetComponent<PlayerElements>();
	}

	public void WaitForGameToStart()
	{
		playerElements.PackColor();
	}
}
