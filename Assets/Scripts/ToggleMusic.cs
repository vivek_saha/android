﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleMusic : MonoBehaviour
{
	[SerializeField]
	private Sprite onSprite;

	[SerializeField]
	private Sprite offSprite;

	[SerializeField]
	private Image buttonImage;

	private void OnEnable()
	{
		if (PlayerPrefs.GetInt("Music") == 0)
		{
			buttonImage.sprite = offSprite;
		}
		else
		{
			buttonImage.sprite = onSprite;
		}
	}

	public void ToggleClick()
	{
		if (PlayerPrefs.GetInt("Music") == 0)
		{
			PlayerPrefs.SetInt("Music", 1);
			buttonImage.sprite = onSprite;
			AudioManager.instance.PlaySound("click");
		}
		else
		{
			PlayerPrefs.SetInt("Music", 0);
			buttonImage.sprite = offSprite;
		}
	}
}
