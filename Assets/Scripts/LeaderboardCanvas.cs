﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;

public class LeaderboardCanvas : MonoBehaviour
{
    public LeaderboardUI[] leaderboardUIs;
    public GameObject panel;

    public void OpenLeaderboard()
    {
        //GameManager.Instance.loadingCanvas.LoadingState(true, false);

        string queryString = "/me/friends?fields=id,first_name,picture.width(128).height(128)&limit=100";
        FB.API(queryString, HttpMethod.GET, GetFriendsCallback);
    }

    public void CloseLeaderboard()
    {

    }

    private static void GetFriendsCallback(IGraphResult result)
    {
        Debug.Log("GetFriendsCallback");
        if (result.Error != null)
        {
            Debug.LogError(result.Error);
            return;
        }
        Debug.Log(result.RawResult);

        // Store /me/friends result
        object dataList;
        if (result.ResultDictionary.TryGetValue("data", out dataList))
        {
            var friendsList = (List<object>)dataList;
            //CacheFriends(friendsList);
        }
    }



    [System.Serializable]
    public class LeaderboardUI
    {
        public GameObject parent;
        public Image prImg;
        public Text prName;
        public Text prChipsText;
    }
}
