using System;

namespace EasyMobile
{
	public struct SavedGameInfoUpdate
	{
		public struct Builder
		{
			internal bool _descriptionUpdated;

			internal string _newDescription;

			internal bool _coverImageUpdated;

			internal byte[] _newPngCoverImage;

			internal bool _playedTimeUpdated;

			internal TimeSpan _newPlayedTime;

			public Builder WithUpdatedDescription(string description)
			{
				_descriptionUpdated = true;
				_newDescription = description;
				return this;
			}

			public Builder WithUpdatedPngCoverImage(byte[] newPngCoverImage)
			{
				_coverImageUpdated = true;
				_newPngCoverImage = newPngCoverImage;
				return this;
			}

			public Builder WithUpdatedPlayedTime(TimeSpan newPlayedTime)
			{
				_playedTimeUpdated = true;
				_newPlayedTime = newPlayedTime;
				return this;
			}

			public SavedGameInfoUpdate Build()
			{
				return new SavedGameInfoUpdate(this);
			}
		}

		private readonly bool _descriptionUpdated;

		private readonly string _newDescription;

		private readonly bool _coverImageUpdated;

		private readonly byte[] _newPngCoverImage;

		private readonly bool _playedTimeUpdated;

		private readonly TimeSpan _newPlayedTime;

		public bool IsDescriptionUpdated => _descriptionUpdated;

		public string UpdatedDescription => _newDescription;

		public bool IsCoverImageUpdated => _coverImageUpdated;

		public byte[] UpdatedPngCoverImage => _newPngCoverImage;

		public bool IsPlayedTimeUpdated => _playedTimeUpdated;

		public TimeSpan UpdatedPlayedTime => _newPlayedTime;

		private SavedGameInfoUpdate(Builder builder)
		{
			_descriptionUpdated = builder._descriptionUpdated;
			_newDescription = builder._newDescription;
			_coverImageUpdated = builder._coverImageUpdated;
			_newPngCoverImage = builder._newPngCoverImage;
			_playedTimeUpdated = builder._playedTimeUpdated;
			_newPlayedTime = builder._newPlayedTime;
		}
	}
}
