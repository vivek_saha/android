using System;
using UnityEngine;

[Serializable]
public class SocketDataResponse
{
	public string tbid;

	public string host;

	public string port;

	public string proto;

	public string type;

	public string tblBootedAmount;

	public string url;

	public static SocketDataResponse CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<SocketDataResponse>(data);
	}
}
