using EasyMobile;
using Facebook.Unity;
using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoginManager : MonoBehaviour
{
	public OpenClosePopup openBlockIDGO;

	public Sprite[] profileSprites;

	public void GuestLoginCO(string name)
	{
		GameManager.Instance.loadingCanvas.LoadingState(true, false);
		LoginCO(SystemInfo.deviceUniqueIdentifier, SystemInfo.deviceUniqueIdentifier, "false", name, SystemInfo.deviceUniqueIdentifier);
	}

	public void LoginWithFacebook()
	{
		LoadLoginElements.profilePicIndex = "-1";
		FB.LogOut();
		AudioManager.instance.PlaySound("click");
		GameManager.Instance.loadingCanvas.LoadingState(true, false);
		List<string> list = new List<string>();
		//list.Add("public_profile");
		list.Add("public_profile");
		list.Add("email");
		list.Add("user_friends");
		List<string> permissions = list;
		FB.LogInWithReadPermissions(permissions, AuthCallback);
	}

	private void AuthCallback(ILoginResult result)
	{
		if (FB.IsLoggedIn)
		{
			AccessToken aToken = AccessToken.CurrentAccessToken;
			Debug.Log(aToken.UserId);
			foreach (string permission in aToken.Permissions)
			{
				Debug.Log(permission);
			}
			GameManager.Instance.localPlayerID = aToken.UserId;
			FB.API("/me?field=name", HttpMethod.GET, delegate(IGraphResult result1)
			{
				if (result1.Error == null)
				{
					GameManager.Instance.localPlayerName = result1.ResultDictionary["name"].ToString();
					FBCheck(aToken.UserId, result1.ResultDictionary["name"].ToString());
				}
			});
		}
		else
		{
			Debug.Log("User cancelled login");
			GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
			NativeUI.ShowToast("User cancelled login");
		}
	}

	private void FBCheck(string _fbUserID, string _fbName)
	{
		LoginCO(_fbUserID, SystemInfo.deviceUniqueIdentifier, "true", _fbName, SystemInfo.deviceUniqueIdentifier);
	}

	private void LoginCO(string _playerID, string _type, string _facebookType, string _name, string _deviceID)
	{
		if (_facebookType == "true")
		{
			GameManager.Instance.LogMessage("Login Type : Facebook");
		}
		else
		{
			GameManager.Instance.LogMessage("Login Type : Guest");
		}

		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("playerId", _playerID);
		jSONObject.AddField("type", _type);
		jSONObject.AddField("type_facebook", _facebookType);
		jSONObject.AddField("deviceType", "Android");
		jSONObject.AddField("profile", LoadLoginElements.profilePicIndex);
		jSONObject.AddField("name", _name);
		jSONObject.AddField("deviceId", _deviceID);

		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.USER_LOGIN_API, jSONObject.ToString())).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint(x);
			AssignValuesAfterLogin(x);
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
		});
	}

	private void AssignValuesAfterLogin(string _response)
	{
		Debug.Log("RES: " + _response);
		PlayerData playerData = PlayerData.CreateFromJSON(_response);
		if (playerData.status == "success")
		{
			GameManager.Instance.localPlayerID = playerData.response.playerId;
			GameManager.Instance.localPlayerName = playerData.response.name;
			GameManager.Instance.id = playerData.response.uid;
			GameManager.Instance.localPlayerChips = long.Parse(playerData.response.chips);
			//GameManager.Instance.localPlayerForceSideshow = long.Parse(playerData.response.forcesideshow);
			GameManager.Instance.lastbonusTime = float.Parse(playerData.response.lastbonus);
			GameManager.Instance.isFacebookLogin = (bool.Parse(playerData.response.facebook) ? true : false);
			GameManager.Instance.tempName = playerData.response.tempname;
			GameManager.Instance.tempImage = playerData.response.profile;
			//GameManager.Instance.VipDetails = int.Parse(playerData.response.vipDetail);
			GameManager.Instance.maxSlotsChaalAmount = long.Parse(playerData.response.maxSlotChaalAmount);
			//GameManager.Instance.slotSeq = playerData.response.slotRewards.slotSeq;
			//GameManager.Instance.slotPair = playerData.response.slotRewards.slotPair;
			//GameManager.Instance.slotColor = playerData.response.slotRewards.slotColor;
			//GameManager.Instance.slotTrail = playerData.response.slotRewards.slotTrail;
			//GameManager.Instance.slotPureSeq = playerData.response.slotRewards.slotPureSeq;
			if (playerData.response.fbMultiMsg != string.Empty)
			{
				NativeUI.Alert("Login Warning!", playerData.response.fbMultiMsg);
			}
			/*if (long.Parse(playerData.response.vip) == 1)
			{
				GameManager.Instance.isVIP = true;
			}
			else
			{
				GameManager.Instance.isVIP = false;
			}*/
			if (playerData.response.host.Length > 0 && playerData.response.port.Length > 0 && playerData.response.proto.Length > 0 && playerData.response.tbid.Length > 0)
			{
				GameManager.Instance.tbid = playerData.response.tbid;
				GameManager.Instance.host = playerData.response.host;
				GameManager.Instance.port = playerData.response.port;
				SceneManager.LoadScene(URL.Instance.TABLE_SCENE);
			}
			else
			{
				SceneManager.LoadScene("MainMenuScene");
			}
			if (GameManager.Instance.isFacebookLogin && PlayerPrefs.GetFloat("Facebook") == 0f)
			{
				PlayerPrefs.SetFloat("Facebook", 1f);
			}
		}
		else
		{
			GameManager.Instance.loadingCanvas.LoadingState(false, false);
			if (playerData.popupMessageTitle == "block")
			{
				openBlockIDGO.OpenPopup();
			}
			else if (playerData.popupMessageTitle == "otherlocation")
			{
				NativeUI.ShowToast(playerData.popupMessageBody, true);
			}
			else if (playerData.responseMessage == "deviceLimit")
			{
				NativeUI.Alert("Device Limit!", playerData.popupMessageBody);
			}
		}
	}

	public void SendEmail()
	{
		string text = "support@teenpattitycoon.com";
		string text2 = MyEscapeURL("Account Unblock Request - Device ID : " + SystemInfo.deviceUniqueIdentifier);
		string text3 = MyEscapeURL(string.Empty);
		Application.OpenURL("mailto:" + text + "?subject=" + text2 + "&body=" + text3);
	}

	private string MyEscapeURL(string url)
	{
		return WWW.EscapeURL(url).Replace("+", "%20");
	}

}
