using System;
using UnityEngine;

[Serializable]
public class DealsDataResponse
{
    public string chips_display;
    public int chips;
    public string discount_percent;
    public int discount_chips;
    public string product_id;
    public string price;


	public static DealsDataResponse CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<DealsDataResponse>(data);
	}
}
