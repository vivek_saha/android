using UnityEngine;

public class State_User_Show : MonoBehaviour
{
	[HideInInspector]
	public PlayerElements playerElements;

	private void Awake()
	{
		playerElements = GetComponent<PlayerElements>();
	}

	public void UserShow()
	{
		playerElements.ResetPlayerTimer();
		if (playerElements.isLocalPlayer && playerElements.isActive)
		{
			playerElements.tElements.NotMyTurn();
		}
	}
}
