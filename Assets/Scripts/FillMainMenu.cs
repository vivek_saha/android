using EasyMobile;
using Facebook.Unity;
using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FillMainMenu : MonoBehaviour
{
    [Header("Profile Elements")]
    public TextMeshProUGUI profileNameText;
    public Text profileChipsText;

    [Header("Profile Status")]
    public GameObject profileStatus;
    public TextMeshProUGUI profileIDStatusText;
    public TextMeshProUGUI profileNameStatusText;
    public Text profileChipsStatusText;

    //[SerializeField]
    //private TextMeshProUGUI profileForceSideshowText;

    public Image profileImage;

    //[SerializeField]
    //private TextMeshProUGUI bonusTimerText;

    //[SerializeField]
    //private Button bonusTimerButton;

    [SerializeField]
    private Transform menuParent;

    [SerializeField]
    private GameObject menuGO;

    public UpdateProfileData profileData;

    public Image initialProfileImage;

    //[SerializeField]
    //private Button nameChangeButton;

    //public GameObject vipGO;

    public Sprite profileSprite;

    [Header("Bonus Elements")]
    [SerializeField]
    private TextMeshProUGUI rewardText;

    [SerializeField]
    private TextMeshProUGUI forceSideShowText;

    [Header("Deals Elements")]
    [SerializeField]
    private GameObject dealsPanelGO;

    [SerializeField]
    private TextMeshProUGUI dealsChipsText;

    [SerializeField]
    private BuyDeals buyDeals;

    [Header("Package In-App Elements")]
    [SerializeField]
    private GameObject inAppGO;

    [SerializeField]
    private GameObject inAppPrefab;

    [Header("BootAmount")]
    [SerializeField]
    private PlayNowBootAmountList playNowBootAmount;

    [SerializeField]
    private PrivateBootAmountList privateBootAmountList;

    [SerializeField]
    private MuflisBootAmountList variationLimitBootAmountList;

    [SerializeField]
    private GameObject exitPopup;

    [SerializeField]
    private OpenClosePopup watchAdsPopup;

    [SerializeField]
    private OpenClosePopup promotionalPopup;

    [SerializeField]
    private Image promotionalImage;

    [SerializeField]
    private string promotionalURLString;

    public TMP_InputField privateTableIF;

    public long privateTableBootAmount;

    private float bonusTimer;

    private bool startBonusTimer;

    [Header("Video Seen Reward")]
    public OpenClosePopup videoSeenGO;

    public TextMeshProUGUI videoChipsReward;

    public TextMeshProUGUI videoForceReward;

    public TapToOpenBonus tapToOpenBonus;

    [Header("Booted Out Canvas")]
    public OpenClosePopup bootedOutPopup;

    public TextMeshProUGUI bootedOutTitle;

    public TextMeshProUGUI bootedOutBody;

    public OpenClosePopup vipDetailsGO;

    public OpenClosePopup bonusOpenGO;


    [Header("Private Table")]
    public GameObject privatePanel;
    public GameObject joinTablePanel;
    public GameObject createTablePanel;
    public InputField findTableInput;
    public Text totalChipsText, bootValText, modeText, chaalLimitText, potLimitText;
    public Slider bootSlider;

    [Header("Other")]
    public GameObject ratePanel;
    public TextMeshProUGUI rewardValue;


    private void Awake()
    {
        TableListing();
        /*if (GameManager.Instance.promotionalImageDisplay == 1 && false)
		{
			DisplayPromotionalImage();
		}*/
        //GameManager.Instance.sCanvas.settingsNameText.text = URL.Instance.TrimName(GameManager.Instance.localPlayerName);
        //GameManager.Instance.sCanvas.settingsIDText.text = "ID : " + GameManager.Instance.id;
        profileChipsStatusText.text = profileChipsText.text = GameManager.Rs + GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
        //profileForceSideshowText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerForceSideshow);
        /*if (GameManager.Instance.isVIP)
		{
			vipGO.SetActive(true);
		}
		else
		{
			vipGO.SetActive(false);
		}*/
        profileNameStatusText.text = profileNameText.text = URL.Instance.TrimNameForOthers(GameManager.Instance.localPlayerName, GameManager.Instance.isFacebookLogin, GameManager.Instance.tempName);
        profileIDStatusText.text = "Your Account ID: " + GameManager.Instance.localPlayerID;
        if (GameManager.Instance.isFacebookLogin)
        {
            //nameChangeButton.gameObject.SetActive(value: true);
            //profileData.proText.text = URL.Instance.TrimName(GameManager.Instance.localPlayerName);
            if (GameManager.Instance.tempName == "-1")
            {
                profileData.nameToggles[0].isOn = true;
            }
            else
            {
                for (int i = 0; i < profileData.nameToggles.Length; i++)
                {
                    if (profileData.nameToggles[i].GetComponentInChildren<TextMeshProUGUI>().text == GameManager.Instance.tempName)
                    {
                        profileData.nameToggles[i].isOn = true;
                    }
                }
            }
            FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, delegate (IGraphResult result)
            {
                if (result.Error == null)
                {
                    //GameManager.Instance.sCanvas.settingsProfileImage.sprite = Sprite.Create(result.Texture, new Rect(0f, 0f, result.Texture.width, result.Texture.height), default(Vector2));
                    initialProfileImage.sprite = Sprite.Create(result.Texture, new Rect(0f, 0f, result.Texture.width, result.Texture.height), default(Vector2));
                    if (GameManager.Instance.tempImage == "-1")
                    {
                        profileImage.sprite = Sprite.Create(result.Texture, new Rect(0f, 0f, result.Texture.width, result.Texture.height), default(Vector2));
                        profileData.profileToggles[0].isOn = true;
                    }
                    else
                    {
                        profileData.profileToggles[int.Parse(GameManager.Instance.tempImage) + 1].isOn = true;
                        GameManager.Instance.LoadImagesCO(profileImage, GameManager.Instance.tempImage);
                    }
                }
            });
        }
        else
        {
            //nameChangeButton.gameObject.SetActive(value: false);
            profileNameStatusText.text = profileNameText.text = URL.Instance.TrimName(GameManager.Instance.localPlayerName);
            //GameManager.Instance.sCanvas.settingsProfileImage.sprite = profileSprite;
        }
        if (GameManager.Instance.lastbonusTime != 14400f)
        {
            bonusTimer = GameManager.Instance.lastbonusTime;
            startBonusTimer = true;
        }
        else
        {
            startBonusTimer = false;
        }
        GameManager.Instance.LogMessage("Main Menu");
        //HeyzapAds.Start("73b58dda351d4116bd18d13400cbfef0", 1);
        if (GameManager.Instance.localPlayerChips <= 500)
        {
            GameManager.Instance.inAppValuesCanvas.OpenIAPCanvas();
        }
    }

    private void Start()
    {
        Invoke("OpneRatePanel", 2);
        rewardValue.text = "Earn <color=#fefd40>" + GameManager.Instance.format.FormatNumber(GameManager.Instance.adsManager.reward_video_amount) + "</color> Chips\nWatch AD";
    }

    public void ClickPromotionalImage()
    {
        if (!(promotionalURLString == string.Empty) && promotionalURLString.Length > 0)
        {
            Application.OpenURL(promotionalURLString);
        }
    }

    public void PackageListing()
    {
        GameManager.Instance.inAppValuesCanvas.OpenIAPCanvas();
    }

    public void VideoReward()
    {
        GameManager.Instance.loadingCanvas.LoadingState(true, false);
        JSONObject jSONObject = new JSONObject();
        jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
        ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.VIDEO_REWARD, jSONObject.ToString())).Subscribe(delegate (string x)
        {
            URL.Instance.DebugPrint("VideoRewardCO " + GameManager.Instance.security.Decrypt(x));
            PlayerData playerData = PlayerData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
            if (playerData.status == "error")
            {
                videoSeenGO.ClosePopup();
            }
            else if (playerData.status == "success")
            {
                UpdateProfileSectionWithAdBonus(playerData.response.reward, playerData.response.forcesideshow);
                GameManager.Instance.LogMessage("Video Seen Completed");
            }
            GameManager.Instance.loadingCanvas.LoadingState(false, false);
        }, delegate (Exception ex)
        {
            Debug.LogException(ex);
            NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
            GameManager.Instance.loadingCanvas.LoadingState(false, false);
        });
    }

    public void DailyBonus()
    {
        AudioManager.instance.PlaySound("click");
        AudioManager.instance.StopSound("bonus-idle");
        AudioManager.instance.PlaySound("bonus-open");
        JSONObject jSONObject = new JSONObject();
        jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
        ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.DAILY_REWARD_API, jSONObject.ToString())).Subscribe(delegate (string x)
        {
            URL.Instance.DebugPrint("Daily Reward" + GameManager.Instance.security.Decrypt(x));
            PlayerData playerData = PlayerData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
            if (playerData.status == "error")
            {
                bonusTimer = float.Parse(playerData.response.timediff);
                TimeSpan timeSpan = TimeSpan.FromSeconds(bonusTimer);
                string text = $"{timeSpan.Hours:D2}h:{timeSpan.Minutes:D2}m:{timeSpan.Seconds:D2}s";
                //bonusTimerText.text = text;
                NativeUI.ShowToast("Wait for " + text);
                AudioManager.instance.StopSound("bonus-open");
                AudioManager.instance.StopSound("bonus-idle");
                AudioManager.instance.StopSound("bonus-impact");
                if (bonusTimer > 0f)
                {
                    startBonusTimer = true;
                }
                else
                {
                    startBonusTimer = false;
                }
                AudioManager.instance.StopSound("bonus-open");
            }
            else if (playerData.status == "success")
            {
                GameManager.Instance.LogMessage("Bonus Claimed");
                GameManager.Instance.lastbonusTime = 14399f;
                //bonusTimerButton.interactable = false;
                UpdateProfileSection(playerData.response.reward, playerData.response.forcesideshow);
                startBonusTimer = true;
                bonusTimer = 14400f;
                tapToOpenBonus.chipsText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(playerData.response.reward));
                tapToOpenBonus.forceText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(playerData.response.forcesideshow));
                //SetLocalNotification();
            }
        }, delegate (Exception ex)
        {
            Debug.LogException(ex);
            GameManager.Instance.LogMessage("Bonus Claimed Error : " + ex);
            NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
            AudioManager.instance.StopSound("bonus-open");
        });
    }

    private void UpdateProfileSection(string _reward, string _forceSideShow)
    {
        GameManager.Instance.localPlayerChips += long.Parse(_reward);
        //GameManager.Instance.localPlayerForceSideshow += long.Parse(_forceSideShow);
        rewardText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(_reward));
        forceSideShowText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(_forceSideShow));
        profileChipsStatusText.text = profileChipsText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
        //profileForceSideshowText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerForceSideshow);
        bonusOpenGO.OpenPopup();
    }

    private void UpdateProfileSectionWithAdBonus(string _reward, string _forceSideShow)
    {
        GameManager.Instance.localPlayerChips += long.Parse(_reward);
        //GameManager.Instance.localPlayerForceSideshow += long.Parse(_forceSideShow);
        rewardText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(_reward));
        forceSideShowText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(_forceSideShow));
        profileChipsStatusText.text = profileChipsText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
        //profileForceSideshowText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerForceSideshow);
        videoChipsReward.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(_reward));
        videoForceReward.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(_forceSideShow));
        videoSeenGO.OpenPopup();
    }

    public void TableListing()
    {
        JSONObject jSONObject = new JSONObject();
        jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
        ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.TABLE_LIST, jSONObject.ToString())).Subscribe(delegate (string x)
        {
            URL.Instance.DebugPrint("Table List: " + x);
            TableData tableData = TableData.CreateFromJSON(x);
            if (tableData.status == "success")
            {
                for (int i = 0; i < tableData.response.Length; i++)
                {
                    if (tableData.response[i].type == "limit")
                    {
                        for (int j = 0; j < tableData.response[i].bootAmount.Length; j++)
                        {
                            playNowBootAmount.playNowBootAmounts[j].SetActive(value: true);
                            playNowBootAmount.playNowBootAmounts[j].GetComponent<GetBootAmount>().bootAmount = tableData.response[i].bootAmount[j];
                            playNowBootAmount.playNowBootAmounts[j].GetComponent<GetBootAmount>().bootAmountText.text = GameManager.Instance.format.FormatNumber(long.Parse(tableData.response[i].bootAmount[j]));
                        }
                    }
                    else if (tableData.response[i].type == "private")
                    {
                        for (int l = 0; l < tableData.response[i].bootAmount.Length; l++)
                        {
                            URL.Instance.DebugPrint("Private Table : " + tableData.response[i].bootAmount[l]);
                            privateBootAmountList.privateBootAmounts[l].SetActive(value: true);
                            privateBootAmountList.privateBootAmounts[l].GetComponent<GetBootAmount>().bootAmount = tableData.response[i].bootAmount[l];
                            privateBootAmountList.privateBootAmounts[l].GetComponent<GetBootAmount>().bootAmountText.text = GameManager.Instance.format.FormatNumber(long.Parse(tableData.response[i].bootAmount[l]));
                        }
                    }
                    else if (tableData.response[i].type == "variation_limit")
                    {
                        for (int n = 0; n < tableData.response[i].bootAmount.Length; n++)
                        {
                            URL.Instance.DebugPrint("variation_limit : " + tableData.response[i].bootAmount[n]);
                            variationLimitBootAmountList.muflisBootAmount[n].SetActive(value: true);
                            variationLimitBootAmountList.muflisBootAmount[n].GetComponent<GetBootAmount>().bootAmount = tableData.response[i].bootAmount[n];
                            variationLimitBootAmountList.muflisBootAmount[n].GetComponent<GetBootAmount>().bootAmountText.text = GameManager.Instance.format.FormatNumber(long.Parse(tableData.response[i].bootAmount[n]));
                        }
                    }
                }
            }
            GameManager.Instance.loadingCanvas.LoadingState(false, false);

            if (GameManager.Instance.isBootedOut)
            {
                GameManager.Instance.LanguageConverter(bootedOutBody, GameManager.Instance.bootedOutBody);
                GameManager.Instance.LanguageConverter(bootedOutTitle, GameManager.Instance.bootedOutTitle);
                bootedOutPopup.OpenPopup();
            }
            else
            {
                bootedOutPopup.ClosePopup();
            }
        }, delegate (Exception ex)
        {
            Debug.LogException(ex);
            NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
        });
    }

    /*public void JoinPrivateTableButton()
	{
		GameManager.Instance.loadingCanvas.LoadingState(true, false);
		if (privateTableIF.text.Length != 0)
		{
			JSONObject jSONObject = new JSONObject();
			jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
			jSONObject.AddField("code", privateTableIF.text);
			ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.PRIVATE_TABLE_JOIN, jSONObject.ToString())).Subscribe(delegate(string x)
			{
				URL.Instance.DebugPrint("PRIVATE_TABLE_JOIN " + GameManager.Instance.security.Decrypt(x));
				SocketData socketData = SocketData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
				if (socketData.status == "success")
				{
					GameManager.Instance.host = socketData.response.host;
					GameManager.Instance.port = socketData.response.port;
					GameManager.Instance.tbid = socketData.response.tbid;
					SceneManager.LoadScene(URL.Instance.TABLE_SCENE);
				}
				else
				{
					NativeUI.ShowToast(socketData.popupMessageBody, isLongToast: true);
					GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
				}
				GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
			}, delegate(Exception ex)
			{
				Debug.LogException(ex);
				NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
				GameManager.Instance.loadingCanvas.LoadingState(false, false);
			});
		}
		else
		{
			URL.Instance.DebugPrint("Please enter code...");
			NativeUI.ShowToast("Please enter code...", true);
			GameManager.Instance.loadingCanvas.LoadingState(false, false);
		}
	}*/

    private void Update()
    {
        /*if (startBonusTimer && bonusTimer > 0f)
		{
			bonusTimer -= Time.deltaTime;
			GameManager.Instance.lastbonusTime = bonusTimer;
			TimeSpan timeSpan = TimeSpan.FromSeconds(bonusTimer);
			string text = $"{timeSpan.Hours:D2}h:{timeSpan.Minutes:D2}m:{timeSpan.Seconds:D2}s";
			bonusTimerText.text = text;
			bonusTimerButton.interactable = false;
		}
		else
		{
			GameManager.Instance.LanguageConverter(bonusTimerText, "Bonus");
			bonusTimerButton.interactable = true;
		}*/
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OpenExitPopup();
        }
    }

    public void OpenSettingsPanel()
    {
        GameManager.Instance.OpenSettingCanvas();
    }

    public void OpenExitPopup()
    {
        exitPopup.SetActive(true);
    }

    public void OpenWatchAdsPopup()
    {
        watchAdsPopup.OpenPopup();
    }

    public void CloseWatchAdsPopup()
    {
        watchAdsPopup.ClosePopup();
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void OpenProfileStatus()
    {
        profileStatus.SetActive(true);
    }

    public void UpdatePlayerChips(string chips)
    {
        GameManager.Instance.localPlayerChips = long.Parse(chips);
        string totalChips = GameManager.Rs + GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
        profileChipsText.text = totalChips;
        profileChipsStatusText.text = totalChips;
    }

    private void OpneRatePanel()
    {
        if (!PlayerPrefs.HasKey("AppRated") && UnityEngine.Random.Range(0, 5) == 0)
        {
            ratePanel.SetActive(true);
        }
    }

    public void RateSubmitButton()
    {
        ratePanel.SetActive(true);
        PlayerPrefs.SetInt("AppRated", 1);
        Application.OpenURL(GameManager.Instance.appURL);
    }

    public void RewardVideoButton()
    {
        if (GameManager.Instance.adsManager.IsRewardVideoLoaded())
        {
            GameManager.Instance.adsManager.ShowRewardVideo();
        }
        else
        {
            NativeUI.ShowToast("Ads not available. Please try again after some time.");
        }
    }

    public void OpenPrivateCanvas(bool isCreate)
    {
        privatePanel.SetActive(true);
        joinTablePanel.SetActive(!isCreate);
        createTablePanel.SetActive(isCreate);
        findTableInput.text = "";
        ResetPrivateTableData();
    }

    public void ClosePrivateCanvas()
    {
        privatePanel.SetActive(false);
    }

    public void JoinButton()
    {
        string code = findTableInput.text;
        if (!string.IsNullOrEmpty(code))
        {
            GameManager.Instance.loadingCanvas.LoadingState(true, false);

            JSONObject jSONObject = new JSONObject();
            jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
            jSONObject.AddField("code", code);

            ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.PRIVATE_TABLE_JOIN, jSONObject.ToString())).Subscribe(delegate (string x)
            {
                URL.Instance.DebugPrint("PRIVATE_TABLE_JOIN " + x);
                SocketData socketData = SocketData.CreateFromJSON(x);
                if (socketData.status == "success")
                {
                    GameManager.Instance.host = socketData.response.host;
                    GameManager.Instance.port = socketData.response.port;
                    GameManager.Instance.tbid = socketData.response.tbid;
                    SceneManager.LoadScene(URL.Instance.TABLE_SCENE);
                }
                else
                {
                    NativeUI.ShowToast(socketData.popupMessageBody, true);
                }

                GameManager.Instance.loadingCanvas.LoadingState(false, false);

            }, delegate (Exception ex)
            {
                Debug.LogException(ex);
                NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
                GameManager.Instance.loadingCanvas.LoadingState(false, false);
            });
        }
        else
        {
            NativeUI.ShowToast("Please Enter Valid Code");
        }
    }

    public void CreateTableButton()
    {
        int bootValue = (int)bootSlider.value;
        if (GameManager.Instance.localPlayerChips > (bootValue * 3))
        {
            GameManager.Instance.loadingCanvas.LoadingState(true, false);

            JSONObject jSONObject = new JSONObject();
            jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
            jSONObject.AddField("type", "private");
            jSONObject.AddField("bootAmount", bootValue);

            ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.FIND_TABLE, jSONObject.ToString())).Subscribe(delegate (string x)
            {
                URL.Instance.DebugPrint("FIND_TABLE " + x);
                SocketData socketData = SocketData.CreateFromJSON(x);

                if (socketData.status == "success")
                {
                    GameManager.Instance.host = socketData.response.host;
                    GameManager.Instance.port = socketData.response.port;
                    GameManager.Instance.tbid = socketData.response.tbid;
                    GameManager.Instance.type = socketData.response.type;
                    GameManager.Instance.tblBootedAmount = socketData.response.tblBootedAmount;

                    GameManager.Instance.adsManager.ShowInterstitial();

                    SceneManager.LoadScene(URL.Instance.TABLE_SCENE);
                }
                else
                {
                    NativeUI.Alert("Oh Snap!", socketData.popupMessageBody);
                    GameManager.Instance.loadingCanvas.LoadingState(false, false);
                }
            }, delegate (Exception ex)
            {
                Debug.LogException(ex);
                NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
                GameManager.Instance.loadingCanvas.LoadingState(false, false);
            });
        }
        else
        {
            NativeUI.ShowToast("You don't have enough chips");
        }
    }

    public void ChnageModePrivateTable()
    {
        if (modeText.text == "Regular")
        {
            modeText.text = "Variation";
        }
        else
        {
            modeText.text = "Regular";
        }
    }

    public void ChangeSliderPrivateTable(bool i)
    {
        int value = (int)bootSlider.value;

        if (i)
        {
            value += 500;
            value = Mathf.Clamp(value, 500, 10000);
        }
        else
        {
            value -= 500;
            value = Mathf.Clamp(value, 500, 10000);
        }

        bootSlider.value = value;
        bootValText.text = GameManager.Instance.format.FormatNumberOnTable(value);
        chaalLimitText.text = GameManager.Instance.format.FormatNumberOnTable(value * 256);
        potLimitText.text = GameManager.Instance.format.FormatNumberOnTable(value * 4000);

    }

    private void ResetPrivateTableData()
    {
        int pboot = 500;
        bootSlider.value = pboot;
        modeText.text = "Regular";
        bootValText.text = GameManager.Instance.format.FormatNumberOnTable(pboot);
        chaalLimitText.text = GameManager.Instance.format.FormatNumberOnTable(pboot * 256);
        potLimitText.text = GameManager.Instance.format.FormatNumberOnTable(pboot * 4000);
        totalChipsText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
    }

}
