using UnityEngine;

public class State_Wait_For_My_Turn : MonoBehaviour
{
	[HideInInspector]
	public PlayerElements playerElements;

	private void Awake()
	{
		playerElements = GetComponent<PlayerElements>();
	}

	public void WaitForMyTurn()
	{
		playerElements.tElements.NotMyTurn();
		playerElements.ResetColor();
	}

	public void WaitForMyTurnForSS()
	{
		playerElements.tElements.NotMyTurnSS();
		playerElements.ResetColor();
	}

	public void StopTimer()
	{
		playerElements.isTimerRunning = false;
		playerElements.firstTimer = false;
		playerElements.secondTimer = false;
		playerElements.ResetPlayerTimer();
	}
}
