﻿using EasyMobile;
using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class InviteFriends : MonoBehaviour
{
    public Button inviteFriendsButton;
    public FillMainMenu fillMainMenu;
    public GameObject bgPanel;
    public GameObject invitePanel;
    public GameObject redemePanel;
    public TextMeshProUGUI codeText;
    public Text inviteChipsMsgText;
    public TMP_InputField inputField;

    private static string inviteCode;
    private static string inviteMessage;

    private void Start()
    {
        JSONObject jSONObject = new JSONObject();
        jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);

        ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.INVITATION_CODE, jSONObject.ToString())).Subscribe(delegate (string x)
        {
            PlayerData playerData = PlayerData.CreateFromJSON(x);
            if (playerData.status == "success")
            {
                inviteCode = playerData.response.inviteCode;
                inviteMessage = playerData.response.message;
                inviteFriendsButton.interactable = true;

                string inviteChipsValue = GameManager.Rs + GameManager.Instance.format.FormatNumberOnTable(long.Parse(playerData.response.inviteChips));
                inviteChipsMsgText.text = "Your friends will get: " + inviteChipsValue + " chips";
                codeText.text = "Your Code : " + inviteCode;
            }
            else
            {
                inviteFriendsButton.interactable = false;
            }

        }, delegate (Exception ex)
        {
            Debug.LogException(ex);
            //NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
        });
    }

    public void ShareCode()
    {
        GameManager.ShareMessage(inviteMessage);
    }

    public void CopyCode()
    {
        GUIUtility.systemCopyBuffer = inviteCode;
        NativeUI.ShowToast("Success");
    }

    public void ApplyCode()
    {
        string code = inputField.text;

        if (string.IsNullOrEmpty(code))
        {
            NativeUI.ShowToast("Plz Enter Valid Code");
            return;
        }

        JSONObject jSONObject = new JSONObject();
        jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
        jSONObject.AddField("inviteCode", code);

        GameManager.Instance.loadingCanvas.LoadingState(true);

        ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.INVITATION_CODE, jSONObject.ToString())).Subscribe(delegate (string x)
        {
            PlayerData playerData = PlayerData.CreateFromJSON(x);
            if (playerData.status == "success")
            {
                NativeUI.ShowToast(playerData.response.message, true);
                fillMainMenu.UpdatePlayerChips(playerData.response.chips);
            }
            else
            {
                NativeUI.ShowToast(playerData.response.message, true);
            }
            GameManager.Instance.loadingCanvas.LoadingState(false);

        }, delegate (Exception ex)
        {
            Debug.LogException(ex);
            GameManager.Instance.loadingCanvas.LoadingState(false);
            //NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
        });

    }

    public void OpenInvitePanel()
    {
        invitePanel.SetActive(true);
        redemePanel.SetActive(false);
    }

    public void OpenRedemePanel()
    {
        invitePanel.SetActive(false);
        redemePanel.SetActive(true);
    }

}
