using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
[ExecuteInEditMode]
[AddComponentMenu("2DxFX/Standard/EnergyBar")]
public class _2dxFX_EnergyBar : MonoBehaviour
{
	[HideInInspector]
	public Material ForceMaterial;

	[HideInInspector]
	public bool ActiveChange = true;

	private string shader = "2DxFX/Standard/EnergyBar";

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Alpha = 1f;

	[HideInInspector]
	[Range(0f, 1f)]
	public float BarProgress = 0.5f;

	[HideInInspector]
	[Range(0.9f, 1f)]
	public float _Value2 = 0.975f;

	[HideInInspector]
	[Range(0f, 0.5f)]
	public float _Value3 = 0.5f;

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Value4 = 1f;

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Value5;

	[HideInInspector]
	public int ShaderChange;

	private Material tempMaterial;

	private Material defaultMaterial;

	private Image CanvasImage;

	private void Awake()
	{
		if (base.gameObject.GetComponent<Image>() != null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
	}

	private void Start()
	{
		ShaderChange = 0;
	}

	public void CallUpdate()
	{
		Update();
	}

	private void Update()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (BarProgress > 1f)
		{
			BarProgress = 1f;
		}
		if (BarProgress < 0f)
		{
			BarProgress = 0f;
		}
		if (ShaderChange == 0 && ForceMaterial != null)
		{
			ShaderChange = 1;
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
			ForceMaterial.hideFlags = HideFlags.None;
			ForceMaterial.shader = Shader.Find(shader);
		}
		if (ForceMaterial == null && ShaderChange == 1)
		{
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
			ShaderChange = 0;
		}
		if (ActiveChange)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial.SetFloat("_Alpha", 1f - _Alpha);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_Value1", BarProgress);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_Value2", 1f - _Value2);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_Value3", 1f - _Value3);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_Value4", _Value4);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_Value5", _Value5);
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material.SetFloat("_Alpha", 1f - _Alpha);
				CanvasImage.material.SetFloat("_Value1", BarProgress);
				CanvasImage.material.SetFloat("_Value2", 1f - _Value2);
				CanvasImage.material.SetFloat("_Value3", 1f - _Value3);
				CanvasImage.material.SetFloat("_Value4", _Value4);
				CanvasImage.material.SetFloat("_Value5", _Value5);
			}
		}
	}

	private void OnDestroy()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (Application.isPlaying || !Application.isEditor)
		{
			return;
		}
		if (tempMaterial != null)
		{
			UnityEngine.Object.DestroyImmediate(tempMaterial);
		}
		if (base.gameObject.activeSelf && defaultMaterial != null)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnDisable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (base.gameObject.activeSelf && defaultMaterial != null)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnEnable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (defaultMaterial == null)
		{
			defaultMaterial = new Material(Shader.Find("Sprites/Default"));
		}
		if (ForceMaterial == null)
		{
			ActiveChange = true;
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
		}
		else
		{
			ForceMaterial.shader = Shader.Find(shader);
			ForceMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
		}
	}
}
