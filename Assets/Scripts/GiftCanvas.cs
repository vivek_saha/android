﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GiftCanvas : MonoBehaviour
{
    public NetworkManager networkManager;

    public GameObject panel;
    public List<GameObject> giftButtonList;
    public Sprite select, unselect;
    private string giftIndex;


    public void OpenGiftPanel()
    {
        panel.SetActive(true);
    }

    public void CloseGiftPanel()
    {
        panel.SetActive(false);
    }

    public void GiftButton(int index)
    {
        giftIndex = index.ToString();
        for (int i = 0; i < giftButtonList.Count; i++)
        {
            if (index == i)
            {
                giftButtonList[i].gameObject.GetComponent<Image>().sprite = select;
            }
            else
            {
                giftButtonList[i].gameObject.GetComponent<Image>().sprite = unselect;
            }
        }
    }

    public void SendButton()
    {
        if (!string.IsNullOrEmpty(giftIndex))
        {
            int index = int.Parse(giftIndex);
            networkManager.SendGift(index);
        }
    }

}
