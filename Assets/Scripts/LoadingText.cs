using TMPro;
using UnityEngine;

public class LoadingText : MonoBehaviour
{
	private TextMeshProUGUI varText;

	private int varNum;

	private void Awake()
	{
		varText = GetComponent<TextMeshProUGUI>();
	}

	private void OnEnable()
	{
		InvokeRepeating("RepeatText", 0f, 0.2f);
	}

	private void OnDisable()
	{
		CancelInvoke("RepeatText");
	}

	private void RepeatText()
	{
		varNum++;
		if (varNum == 1)
		{
			varText.text = "Loading";
			return;
		}
		if (varNum == 2)
		{
			varText.text = "Loading.";
			return;
		}
		if (varNum == 3)
		{
			varText.text = "Loading..";
			return;
		}
		if (varNum == 4)
		{
			varText.text = "Loading...";
			return;
		}
		varText.text = "Loading";
		varNum = 0;
	}


}
