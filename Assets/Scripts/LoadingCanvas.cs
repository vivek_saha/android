using UnityEngine;

public class LoadingCanvas : MonoBehaviour
{
	[SerializeField]
	private GameObject loadingGOWithoutTip;

	private void Start()
	{
		LoadingState(false);
	}

	public void LoadingState(bool _isActive, bool _tip = false)
	{
		if (_isActive)
		{
			loadingGOWithoutTip.SetActive(true);
		}
		else
		{
			loadingGOWithoutTip.SetActive(false);
		}
	}
}
