namespace SocketIO
{
	public enum SocketPacketType
	{
		UNKNOWN = -1,
		CONNECT,
		DISCONNECT,
		EVENT,
		ACK,
		ERROR,
		BINARY_EVENT,
		BINARY_ACK,
		CONTROL
	}
}
