using System;

[Serializable]
public class InAppJSON
{
	public string package_name;

	public string order_id;

	public string amount;

	public InAppJSON(string _packageName, string _order_id, string _amount)
	{
		package_name = _packageName;
		order_id = _order_id;
		amount = _amount;
	}
}
