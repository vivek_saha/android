using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LerpColor : MonoBehaviour
{
	public Image shadowImage;

	public Color newColor;

	private void Awake()
	{
	}

	private void Start()
	{
		shadowImage.DOColor(newColor, 1f).SetLoops(-1, LoopType.Yoyo);
	}
}
