﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using AudienceNetwork;
using GoogleMobileAds.Api;
using UniRx;
using EasyMobile;

public class AdsManager : MonoBehaviour, IUnityAdsListener
{
    [Header("admob, fb, unity")]
    public string firstAdName;

    [Space]
    public string admob_interstitial_id;
    public string admob_reward_id;

    public string fb_interstitial_id;
    public string fb_reward_id;

    public string unity_app_id;
    public bool unity_test_mode;

    public int reward_video_amount;

    private bool watchVideoCompletely;

    public static string RewardActionName;
    public static string InterActionName;

    private void Start()
    {
        /*Advertisement.AddListener(this);
        Advertisement.Initialize(unity_app_id, unity_test_mode);
        AudienceNetworkAds.Initialize();

        LoadAdmobInterstitial();
        LoadAdmobReward();
        LoadFBInterstitial();
        LoadFBReward();*/

    }

    public bool IsInterstitialLoaded()
    {
        return IsAdmobInterLoaded() || Advertisement.IsReady() || IsFBInterLoaded();
    }

    public void ShowInterstitial()
    {
        return;
        switch (firstAdName)
        {
            case "admob":
                if (IsAdmobInterLoaded())
                {
                    ShowAdmobInterstitial();
                }
                else if (Advertisement.IsReady())
                {
                    ShowUnityInterstitial();
                }
                else if (IsFBInterLoaded())
                {
                    ShowFBInterstitial();
                }
                break;
            case "fb":
                if (IsFBInterLoaded())
                {
                    ShowFBInterstitial();
                }
                else if (Advertisement.IsReady())
                {
                    ShowUnityInterstitial();
                }
                else if (IsAdmobInterLoaded())
                {
                    ShowAdmobInterstitial();
                }
                break;
            case "unity":
                if (Advertisement.IsReady())
                {
                    ShowUnityInterstitial();
                }
                else if (IsAdmobInterLoaded())
                {
                    ShowAdmobInterstitial();
                }
                else if (IsFBInterLoaded())
                {
                    ShowFBInterstitial();
                }
                break;
        }
    }

    public bool IsRewardVideoLoaded()
    {
        return false;
        return IsAdmobRewardLoaded() || Advertisement.IsReady("rewardedVideo") || IsFBRewardLoaded();
    }

    public void ShowRewardVideo()
    {
        return;
        watchVideoCompletely = false;

        switch (firstAdName)
        {
            case "admob":
                if (IsAdmobRewardLoaded())
                {
                    ShowAdmobReward();
                }
                else if (Advertisement.IsReady("rewardedVideo"))
                {
                    ShowUnityReward();
                }
                else if (IsFBRewardLoaded())
                {
                    ShowFBReward();
                }
                break;
            case "fb":
                if (IsFBRewardLoaded())
                {
                    ShowFBReward();
                }
                else if (Advertisement.IsReady("rewardedVideo"))
                {
                    ShowUnityReward();
                }
                else if (IsAdmobRewardLoaded())
                {
                    ShowAdmobReward();
                }
                break;
            case "unity":
                if (Advertisement.IsReady("rewardedVideo"))
                {
                    ShowUnityReward();
                }
                else if (IsAdmobRewardLoaded())
                {
                    ShowAdmobReward();
                }
                else if (IsFBRewardLoaded())
                {
                    ShowFBReward();
                }
                break;
        }
    }


    private GoogleMobileAds.Api.InterstitialAd interstitial_admob;
    private RewardedAd reward_admob;
    private bool IsAdmobInterLoaded()
    {
        if (!interstitial_admob.IsLoaded())
        {
            LoadAdmobInterstitial();
            return false;
        }
        else
        {
            return true;
        }
    }
    private void ShowAdmobInterstitial()
    {
        interstitial_admob.Show();
    }
    private void LoadAdmobInterstitial()
    {
        interstitial_admob = new GoogleMobileAds.Api.InterstitialAd(admob_interstitial_id);
        interstitial_admob.OnAdOpening += AdmobInterOpened;
        interstitial_admob.OnAdClosed += AdmobInterClosed;
        AdRequest request = new AdRequest.Builder().Build();
        interstitial_admob.LoadAd(request);
    }
    private void AdmobInterOpened(object sender, EventArgs e)
    {
        UnityMainThreadDispatcher.Instance.Enqueue(AdOpenFunction());
    }
    private void AdmobInterClosed(object sender, EventArgs e)
    {
        interstitial_admob.Destroy();
        UnityMainThreadDispatcher.Instance.Enqueue(AdCloseFunction());
        LoadAdmobInterstitial();
    }

    private bool IsAdmobRewardLoaded()
    {
        if (!reward_admob.IsLoaded())
        {
            LoadAdmobReward();
            return false;
        }
        else
        {
            return true;
        }
    }
    private void ShowAdmobReward()
    {
        reward_admob.Show();
    }
    private void LoadAdmobReward()
    {
        reward_admob = new RewardedAd(admob_reward_id);
        reward_admob.OnAdOpening += AdmobRewardOpening;
        reward_admob.OnUserEarnedReward += AdmobRewardEarned;
        reward_admob.OnAdClosed += AdmobRewardClosed;
        AdRequest request = new AdRequest.Builder().Build();
        reward_admob.LoadAd(request);
    }
    private void AdmobRewardOpening(object sender, EventArgs e)
    {
        UnityMainThreadDispatcher.Instance.Enqueue(AdOpenFunction());
    }
    private void AdmobRewardEarned(object sender, Reward e)
    {
        watchVideoCompletely = true;
    }
    private void AdmobRewardClosed(object sender, EventArgs e)
    {
        UnityMainThreadDispatcher.Instance.Enqueue(RewardAdCloseFunction(watchVideoCompletely));
        LoadAdmobReward();
    }

    private AudienceNetwork.InterstitialAd interstitial_fb;
    private bool isLoaded_fb_interstitial;
    private RewardedVideoAd rewarded_fb;
    private bool isLoaded_fb_reward;
    private bool IsFBInterLoaded()
    {
        if (!isLoaded_fb_interstitial)
        {
            LoadFBInterstitial();
            return false;
        }
        else
        {
            return true;
        }
    }
    private void ShowFBInterstitial()
    {
        interstitial_fb.Show();
        isLoaded_fb_interstitial = false;
    }
    private void LoadFBInterstitial()
    {
        interstitial_fb = new AudienceNetwork.InterstitialAd(fb_interstitial_id);
        interstitial_fb.Register(gameObject);

        interstitial_fb.InterstitialAdDidLoad = delegate ()
        {
            isLoaded_fb_interstitial = true;
            string isAdValid = interstitial_fb.IsValid() ? "valid" : "invalid";
        };
        interstitial_fb.InterstitialAdDidFailWithError = delegate (string error)
        {
            //Debug.Log("Interstitial ad failed to load with error: " + error);
        };
        interstitial_fb.InterstitialAdWillLogImpression = delegate
        {
            //Debug.Log("interstitial ad willLog impression");
            UnityMainThreadDispatcher.Instance.Enqueue(AdOpenFunction());
        };
        interstitial_fb.InterstitialAdDidClose = delegate ()
        {
            if (interstitial_fb != null)
            {
                interstitial_fb.Dispose();
            }
            UnityMainThreadDispatcher.Instance.Enqueue(AdCloseFunction());
            LoadFBInterstitial();
        };
        interstitial_fb.LoadAd();
    }

    private bool IsFBRewardLoaded()
    {
        if (!isLoaded_fb_reward)
        {
            LoadFBReward();
            return false;
        }
        else
        {
            return true;
        }
    }
    private void ShowFBReward()
    {
        if (isLoaded_fb_reward)
        {
            rewarded_fb.Show();
            isLoaded_fb_reward = false;
        }
        else
        {
            LoadFBReward();
        }
    }
    private void LoadFBReward()
    {
        rewarded_fb = new RewardedVideoAd(fb_reward_id);
        rewarded_fb.Register(gameObject);

        rewarded_fb.RewardedVideoAdDidLoad = delegate ()
        {
            isLoaded_fb_reward = true;
            string isAdValid = rewarded_fb.IsValid() ? "valid" : "invalid";
        };
        rewarded_fb.RewardedVideoAdDidFailWithError = delegate (string error)
        {
            //Debug.Log("RewardedVideo ad failed to load with error: " + error);
        };
        rewarded_fb.RewardedVideoAdWillLogImpression = delegate ()
        {
            //Debug.Log("RewardedVideo ad logged impression.");
            UnityMainThreadDispatcher.Instance.Enqueue(AdOpenFunction());
        };
        rewarded_fb.RewardedVideoAdComplete = delegate ()
        {
            //Debug.Log("RewardedVideo ad logged impression.");
            watchVideoCompletely = true;
        };
        rewarded_fb.RewardedVideoAdDidClose = delegate ()
        {
            if (rewarded_fb != null)
            {
                rewarded_fb.Dispose();
            }
            UnityMainThreadDispatcher.Instance.Enqueue(RewardAdCloseFunction(watchVideoCompletely));
            LoadFBReward();
        };
        rewarded_fb.LoadAd();
    }

    private void ShowUnityInterstitial()
    {
        Advertisement.Show();
    }

    private void ShowUnityReward()
    {
        Advertisement.Show("rewardedVideo");
    }

    public void OnUnityAdsReady(string placementId)
    {
    }

    public void OnUnityAdsDidError(string message)
    {
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        UnityMainThreadDispatcher.Instance.Enqueue(AdOpenFunction());
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if (placementId == "rewardedVideo")
        {
            if (showResult == ShowResult.Finished)
            {
                watchVideoCompletely = true;
            }
            else
            {
                watchVideoCompletely = false;
            }

            UnityMainThreadDispatcher.Instance.Enqueue(RewardAdCloseFunction(watchVideoCompletely));
        }
        else
        {
            UnityMainThreadDispatcher.Instance.Enqueue(AdCloseFunction());
        }
    }

    private IEnumerator AdOpenFunction()
    {
        yield return null;
    }

    private IEnumerator AdCloseFunction()
    {
        yield return null;
    }

    private IEnumerator RewardAdCloseFunction(bool value)
    {
        Debug.Log("Complete Reward Video: " + value);
        if (value)
        {
            JSONObject jSONObject = new JSONObject();
            jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
            jSONObject.AddField("amount", reward_video_amount);
            jSONObject.AddField("type", "user_watch_reward_video");

            ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.ADD_Chips, jSONObject.ToString())).Subscribe(delegate (string x)
            {
                PlayerData playerData = PlayerData.CreateFromJSON(x);
                if (playerData.status == "success")
                {
                    GameManager.Instance.localPlayerChips = long.Parse(playerData.response.chips);
                    FillMainMenu fmm = FindObjectOfType<FillMainMenu>();
                    fmm.profileChipsStatusText.text = fmm.profileChipsText.text = GameManager.Rs + GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
                }
            }, delegate (Exception ex)
            {
                Debug.LogException(ex);
                NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
            });
        }
        yield return null;
    }
}
