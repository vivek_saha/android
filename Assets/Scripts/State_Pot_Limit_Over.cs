using UnityEngine;

public class State_Pot_Limit_Over : MonoBehaviour
{
	[HideInInspector]
	public PlayerElements playerElements;

	private void Awake()
	{
		playerElements = GetComponent<PlayerElements>();
	}

	public void PotLimitOver()
	{
		if (playerElements.isActive)
		{
			playerElements.isTimerRunning = false;
			playerElements.firstTimer = false;
			playerElements.secondTimer = false;
			playerElements.tElements.NotMyTurn();
			playerElements.ResetPlayerTimer();
			playerElements.ResetColor();
		}
	}
}
