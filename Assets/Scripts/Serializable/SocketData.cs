using System;
using UnityEngine;

[Serializable]
public class SocketData
{
	public string status;

	public string responseMessage;

	public string popupMessageTitle;

	public string popupMessageBody;

	public SocketDataResponse response;

	public static SocketData CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<SocketData>(data);
	}
}
