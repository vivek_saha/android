using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LerpColorSideWinner : MonoBehaviour
{
	public Image shadowImage;

	public Color newColor;

	public Color oldColor;

	private void Awake()
	{
	}

	private void Start()
	{
		oldColor = shadowImage.color;
		shadowImage.DOColor(newColor, 0.2f).SetLoops(-1, LoopType.Restart).OnComplete(delegate
		{
			shadowImage.color = oldColor;
		});
	}
}
