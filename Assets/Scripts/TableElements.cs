using DG.Tweening;
using EasyMobile;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TableElements : MonoBehaviour
{
	[Header("Table Info")]
	public GameObject tableInfoGO;

	public GameObject cardDistributePos;

	public Text bootAmountText;
	public Text maxChaalText;
	public Text maxPotLimitText;

	public Text bootAmountText_Info;
	public Text maxChaalText_Info;
	public Text maxPotLimitText_Info;
	public Text maxNumberOfBlindText_Info;

	public Text messageText;
	public Text tableMessageText;


	public string maxNumberOfBlind;

	public long maxChaalLimit;

	public long maxTableLimit;

	public string tableType;

	public bool isGamePlaying;

	public Sprite defaultSprite;

	//[Header("Gift Item Main Panel")]
	//public Button[] localGiftItems;

	//public GameObject[] nonLocalGiftItems;

	[Header("Total Amount Table")]
	public GameObject totalAmountOnTableGO;

	public Text totalAmountOnTableText;

	public long totalAmountOnTable;

	public long tempTotalAmountOnTable;

	public string privateTableCode;

	public GameObject privateTableCodeGO;

	public TextMeshProUGUI privateTableCodeText;

	public GameObject variationTableCodeGO;

	public Text variationTableCodeText;

	public long forceDeduct;

	//public TextMeshProUGUI forceDeductText;

	//public Animator chipsBounceAnim;

	public Button increaseButton;

	public Button decreaseButton;

	public GameObject tableTotalAmountGO;

	/*[Header("Loading Buttons")]
	public GameObject loadingBlindButton;

	public GameObject loadingShowButton;

	public GameObject loadingPackButton;

	public GameObject loadingForceButton;

	public GameObject loadingBlindParticle;

	public GameObject loadingShowParticle;

	public GameObject loadingPackParticle;

	public GameObject loadingForceParticle;*/

	[Header("Player Chaal Menu")]
	public GameObject chaalMenuGO;

	//public GameObject chaalNONMenuGO;

	public Text chaalAmountText;

	//public TextMeshProUGUI chaalNONAmountText;

	//public TextMeshProUGUI forceSideShowCountText;

	public long amountMultiplier;

	public long amount = 1L;

	public long tempAmount = 1L;

	public Text amountText;

	public Button showButton;

	public Button chaalButton;

	//public Button forcesideshowButton;

	public EventTrigger showButtonEvent;

	public EventTrigger chaalButtonEvent;

	//public EventTrigger forcesideshowButtonEvent;

	public EventTrigger packButtonEvent;

	//public Toggle showButtonToggle;

	//public Toggle chaalButtonToggle;

	//public Toggle packButtonToggle;

	public Text chaalButtonText;

	//public Text chaalToggleText;

	[Header("Show Elements")]
	public GameObject userShowGO;

	[Header("Side Show Elements")]
	public GameObject userSideShowGO;

	//[Header("Force Side Show Elements")]
	//public GameObject userForceSideShowGO;

	[Header("SideShow")]
	public GameObject sideShowPanelGO;

	public TextMeshProUGUI acceptSideShowText;

	public bool isSideShowSliderOn;

	public Slider sideShowSlider;

	[Header("Player Color")]
	public Color packColor;

	public Color playColor;

	public Color waitColor;

	[Header("Items")]
	public GameObject emojiGO;

	public GameObject giftsGO;

	[Header("Counting Elements")]
	public float chipCountDuration;

	[Header("Leave Canvas")]
	public GameObject leaveCanvas;

	[Header("Switch Button")]
	public GameObject switchButton;

	[Header("Select Variations Canvas")]
	public GameObject selectVariationCanvas;

	[Header("Chat Canvas")]
	public GameObject chatCanvas;

	public TMP_InputField chatInputField;

	[Header("Table Info Panel")]
	public GameObject mainTableInfoPanel;

	//public GameObject[] tableInfoPanels;

	[Header("Variation Selector")]
	public bool isVariation;

	public Slider variationSliderValue;

	public Image variationSliderImage;

	public Color variationSliderNormalColor;

	public Color variationSliderHighlitedColor;

	public TextMeshProUGUI valriationSliderText;

	[HideInInspector]
	public Cards _cards;

	public NativeShare nativeShare;

	public PlayerInfo playerInfo;

	public ThemeSelectCanvas themeSelectCanvas;

	private bool isColorChanging;

	[Header("Dealer Canvas")]
	public GameObject dealerCanvas;
	public GameObject tipCanvas;
	public GameObject changeDealerCanvas;

	public DealerGirl[] dealerGirls;
	public Image dealerGirlTable;
	public Image dealerImage;
	public Text dealerName;
	public Text tipText;
	public Button itipButton, dtipButton;
	private int[] tipValue = { 100, 500, 1000, 5000, 10000 };
	private int tipIndex;
	private int dealerIndex;


	private void Awake()
	{
		themeSelectCanvas.SetCurrantTheme();
		ResetTableElements();
		_cards = FindObjectOfType<Cards>();
		CloseLeaveCanvas();
		//CloseChatCanvas();
		DisableLoadingButtons();
		OffSwitchButton();
		OnSwitchButton(2f);
	}

	public void InvokeSwitch()
	{
		switchButton.GetComponent<Button>().interactable = true;
	}

	public void OnSwitchButton(float _delay)
	{
		Invoke("InvokeSwitch", _delay);
	}

	public void OffSwitchButton()
	{
		switchButton.GetComponent<Button>().interactable = false;
	}

	public void EnableLoadingButtons(bool _buttonLoading, bool _blindParticle, bool _showParticle, bool _packParticle, bool _forceParticle)
	{
		/*loadingBlindButton.SetActive(_buttonLoading);
		loadingShowButton.SetActive(_buttonLoading);
		loadingPackButton.SetActive(_buttonLoading);
		loadingForceButton.SetActive(_buttonLoading);
		loadingBlindParticle.SetActive(_blindParticle);
		loadingShowParticle.SetActive(_showParticle);
		loadingPackParticle.SetActive(_packParticle);
		loadingForceParticle.SetActive(_forceParticle);*/
	}

	public void DisableLoadingButtons()
	{
		/*loadingBlindButton.SetActive(false);
		loadingShowButton.SetActive(false);
		loadingPackButton.SetActive(false);
		loadingForceButton.SetActive(false);
		loadingBlindParticle.SetActive(false);
		loadingShowParticle.SetActive(false);
		loadingPackParticle.SetActive(false);
		loadingForceParticle.SetActive(false);*/
	}

	public void SetTotalAmountOnTable(long _amount)
	{
		long prev = GameManager.Instance.tempTotalAmountOnTable;
		totalAmountOnTable = _amount;
		GameManager.Instance.CountChips(prev, totalAmountOnTable, totalAmountOnTableText, prev);
		//chipsBounceAnim.SetTrigger("bounce");
	}

	public void GetTotalAmountOnTable(long _amount)
	{
		long prev = GameManager.Instance.tempTotalAmountOnTable;
		totalAmountOnTable = _amount;
		GameManager.Instance.CountChips(prev, totalAmountOnTable, totalAmountOnTableText, prev);
		if (!totalAmountOnTableGO.activeInHierarchy)
		{
			if (totalAmountOnTable > 0)
			{
				totalAmountOnTableGO.SetActive(true);
			}
			else
			{
				totalAmountOnTableGO.SetActive(false);
			}
		}
	}

	public void ResetTableElements()
	{
		isGamePlaying = false;
		totalAmountOnTableGO.SetActive(false);
		tableInfoGO.SetActive(true);
		totalAmountOnTable = 0L;
		totalAmountOnTableText.text = string.Empty;
		/*if (chaalButtonToggle.isOn)
		{
			chaalButtonToggle.isOn = false;
		}
		if (packButtonToggle.isOn)
		{
			packButtonToggle.isOn = false;
		}
		if (showButtonToggle.isOn)
		{
			showButtonToggle.isOn = false;
		}*/
		variationTableCodeGO.SetActive(false);
		privateTableCodeText.alignment = TextAlignmentOptions.Midline;
		if (tableType != "private")
		{
			privateTableCodeText.text = string.Empty;
		}
		GameObject gameObject = GameObject.FindGameObjectWithTag("TAmount");
		if (gameObject != null)
		{
			Destroy(gameObject);
		}
		GameObject[] array = GameObject.FindGameObjectsWithTag("SideShowTag");
		for (int i = 0; i < array.Length; i++)
		{
			if (array != null && array[i].activeInHierarchy)
			{
				Destroy(array[i]);
			}
		}
		CloseSelectVariationCanvas();
		GameObject gameObject2 = GameObject.FindGameObjectWithTag("ForceSideShowTag");
		if (gameObject2 != null && gameObject2.activeInHierarchy)
		{
			gameObject2.SetActive(false);
		}
		userShowGO.SetActive(false);
		GameManager.Instance.LanguageConverter(chaalButtonText, "Blind");
		//GameManager.Instance.LanguageConverter(chaalToggleText, "Blind");
		chaalMenuGO.SetActive(false);
		//chaalNONMenuGO.SetActive(value: false);
		sideShowPanelGO.SetActive(false);
		isSideShowSliderOn = false;
		DisableLoadingButtons();
		tableMessageText.text = string.Empty;
		UserPackLocally();
	}

	public void DisableEmojis()
	{
		GameObject[] array = GameObject.FindGameObjectsWithTag("items_emojis");
		for (int i = 0; i < array.Length; i++)
		{
			if (array != null)
			{
				Destroy(array[i]);
			}
		}
	}

	public void MyTurn()
	{
		if (amount * 2 <= GameManager.Instance.localPlayerChips)
		{
			if (tableType == "limit" || tableType == "variation_limit" || tableType == "private" || tableType == "OLC")
			{
				if (GameManager.Instance.isBlind)
				{
					if (amount * 2 >= maxChaalLimit)
					{
						increaseButton.interactable = false;
						decreaseButton.interactable = false;
					}
					else
					{
						increaseButton.interactable = true;
						decreaseButton.interactable = false;
					}
				}
				else if (amount >= maxChaalLimit)
				{
					increaseButton.interactable = false;
					decreaseButton.interactable = false;
				}
				else
				{
					increaseButton.interactable = true;
					decreaseButton.interactable = false;
				}
			}
			else
			{
				increaseButton.interactable = true;
				decreaseButton.interactable = false;
			}
		}
		else
		{
			increaseButton.interactable = false;
			decreaseButton.interactable = false;
		}
		if (amount > GameManager.Instance.localPlayerChips)
		{
			chaalButton.interactable = false;
			chaalButtonEvent.enabled = false;
			showButton.interactable = false;
			showButtonEvent.enabled = false;
			//forcesideshowButton.interactable = false;
			//forcesideshowButtonEvent.enabled = false;
		}
		else
		{
			chaalButton.interactable = true;
			chaalButtonEvent.enabled = true;
			showButton.interactable = true;
			showButtonEvent.enabled = true;
			/*if (GameManager.Instance.localPlayerForceSideshow >= forceDeduct)
			{
				forcesideshowButton.interactable = true;
				forcesideshowButtonEvent.enabled = true;
			}
			else
			{
				forcesideshowButton.interactable = false;
				forcesideshowButtonEvent.enabled = false;
			}*/
		}
		chaalMenuGO.SetActive(true);
		//chaalNONMenuGO.SetActive(false);
		tableInfoGO.SetActive(false);
		sideShowPanelGO.SetActive(false);
	}

	public void NotMyTurn()
	{
		chaalMenuGO.SetActive(false);
        //chaalNONMenuGO.SetActive(true);
        tableInfoGO.SetActive(true);
		sideShowPanelGO.SetActive(false);
	}

	public void NotMyTurnSS()
	{
		Debug.LogError("----------- Check Here");
		/*chaalMenuGO.SetActive(false);
		chaalNONMenuGO.SetActive(true);
		tableInfoGO.SetActive(false);*/

		chaalMenuGO.SetActive(false);
        tableInfoGO.SetActive(true);
	}

	public void OpenSideShowPanel()
	{
		Debug.LogError("----------- Check Here");

		/*chaalMenuGO.SetActive(false);
		chaalNONMenuGO.SetActive(true);
		tableInfoGO.SetActive(false);
		sideShowPanelGO.SetActive(true);*/

		chaalMenuGO.SetActive(false);
		tableInfoGO.SetActive(false);
		sideShowPanelGO.SetActive(true);
	}

	public void CloseSideShowPanel()
	{
		Debug.LogError("----------- Check Here");

		/*chaalMenuGO.SetActive(false);
		chaalNONMenuGO.SetActive(true);
		tableInfoGO.SetActive(false);
		sideShowPanelGO.SetActive(false);
		isSideShowSliderOn = false;*/

		chaalMenuGO.SetActive(false);
        tableInfoGO.SetActive(true);
		sideShowPanelGO.SetActive(false);
		isSideShowSliderOn = false;
	}

	public void UserPackLocally()
	{
		chaalMenuGO.SetActive(false);
		//chaalNONMenuGO.SetActive(false);
		tableInfoGO.SetActive(true);
		sideShowPanelGO.SetActive(false);
	}

	public void CloseEmojiPopup()
	{
		AudioManager.instance.PlaySound("close-popup");
		emojiGO.GetComponent<Animator>().SetBool("isopen", false);
	}

	public void CloseGiftPopup()
	{
		AudioManager.instance.PlaySound("close-popup");
		giftsGO.GetComponent<Animator>().SetBool("isopen", false);
	}

	public void IncreaseAmount()
	{
		AudioManager.instance.PlaySound("click");
		if (amount * 2 <= GameManager.Instance.localPlayerChips)
		{
			if (tableType == "limit" || tableType == "variation_limit" || tableType == "private" || tableType == "OLC")
			{
				if (GameManager.Instance.isBlind)
				{
					if (amount < maxTableLimit / 2)
					{
						amountMultiplier++;
						tempAmount *= 2L;
						amountText.text = GameManager.Instance.format.FormatNumberOnTable(tempAmount);
					}
				}
				else if (amount < maxTableLimit)
				{
					amountMultiplier++;
					tempAmount *= 2L;
					amountText.text = GameManager.Instance.format.FormatNumberOnTable(tempAmount);
				}
				increaseButton.interactable = false;
			}
			else
			{
				amountMultiplier++;
				tempAmount *= 2L;
				amountText.text = GameManager.Instance.format.FormatNumberOnTable(tempAmount);
			}
			if (amountMultiplier > 1)
			{
				decreaseButton.interactable = true;
			}
			if (tempAmount * 2 > GameManager.Instance.localPlayerChips)
			{
				increaseButton.interactable = false;
			}
		}
		else
		{
			increaseButton.interactable = false;
		}
	}

	public void DecreaseAmount()
	{
		AudioManager.instance.PlaySound("click");
		if (amountMultiplier > 1)
		{
			if (tableType == "limit" || tableType == "variation_limit" || tableType == "private" || tableType == "OLC")
			{
				amountMultiplier--;
				tempAmount /= 2L;
				amountText.text = GameManager.Instance.format.FormatNumberOnTable(tempAmount);
				increaseButton.interactable = true;
				decreaseButton.interactable = false;
			}
			else
			{
				amountMultiplier--;
				tempAmount /= 2L;
				amountText.text = GameManager.Instance.format.FormatNumberOnTable(tempAmount);
				increaseButton.interactable = true;
			}
			if (amountMultiplier <= 1)
			{
				decreaseButton.interactable = false;
			}
		}
		else
		{
			decreaseButton.interactable = false;
		}
	}

	public void ResetAmountMultiplier(long _amount)
	{
		amountMultiplier = 1L;
		DisableLoadingButtons();
	}

	public void OpenSettingCanvas()
	{
		GameManager.Instance.OpenSettingCanvas();
	}

	public void OpenLeaveCanvas()
	{
		AudioManager.instance.PlaySound("click");
		leaveCanvas.GetComponent<Animator>().SetBool("isopen", true);
	}

	public void CloseLeaveCanvas()
	{
		AudioManager.instance.PlaySound("close-popup");
		leaveCanvas.GetComponent<Animator>().SetBool("isopen", false);
	}

	public void OpenSelectVariationCanvas()
	{
		AudioManager.instance.PlaySound("click");
		variationSliderValue.value = variationSliderValue.maxValue;
		selectVariationCanvas.SetActive(true);
		isVariation = true;
		isColorChanging = true;
	}

	public void CloseSelectVariationCanvas()
	{
		selectVariationCanvas.SetActive(false);
	}

	public void OpenTableInfoPanelCanvas()
	{
		AudioManager.instance.PlaySound("click");
		mainTableInfoPanel.SetActive(true);
		//mainTableInfoPanel.GetComponent<Animator>().SetBool("isopen", value: true);
		/*if (tableType == "limit" || tableType == "nolimit" || tableType == "private")
		{
			tableInfoPanels[0].SetActive(false);
			tableInfoPanels[1].SetActive(true);
			tableInfoPanels[2].SetActive(false);
		}
		else if (tableType == "variation_limit" || tableType == "variation_nolimit")
		{
			tableInfoPanels[0].SetActive(true);
			tableInfoPanels[1].SetActive(false);
			tableInfoPanels[2].SetActive(false);
		}
		else if (tableType == "OLC")
		{
			tableInfoPanels[0].SetActive(false);
			tableInfoPanels[1].SetActive(false);
			tableInfoPanels[2].SetActive(true);
		}
		else
		{
			tableInfoPanels[0].SetActive(false);
			tableInfoPanels[1].SetActive(false);
			tableInfoPanels[2].SetActive(false);
		}*/
	}

	public void CloseTableInfoPanelCanvas()
	{
		AudioManager.instance.PlaySound("close-popup");
		mainTableInfoPanel.SetActive(false);
		//mainTableInfoPanel.GetComponent<Animator>().SetBool("isopen", value: false);
	}

	public void OpenChatCanvas()
	{
		AudioManager.instance.PlaySound("click");
		chatCanvas.GetComponent<Animator>().SetBool("isopen", value: true);
	}

	public void CloseChatCanvas()
	{
		AudioManager.instance.PlaySound("close-popup");
		chatCanvas.GetComponent<Animator>().SetBool("isopen", value: false);
	}

	public void OpenInApp()
	{
		GameManager.Instance.inAppValuesCanvas.OpenIAPCanvas();
	}

	public void OpenSetting()
    {
		GameManager.Instance.OpenSettingCanvas();
    }

	private void Update()
	{
		if (isSideShowSliderOn)
		{
			sideShowSlider.value += Time.deltaTime;
		}
		else
		{
			sideShowSlider.value = 0f;
		}
		if (isVariation)
		{
			variationSliderValue.value -= Time.deltaTime;
			valriationSliderText.text = variationSliderValue.value.ToString("00.00");
			if (variationSliderValue.value <= 3f && isColorChanging)
			{
				variationSliderImage.DOColor(variationSliderHighlitedColor, 0.4f).SetLoops(-1).SetEase(Ease.OutBack);
				isColorChanging = false;
			}
			else if (variationSliderValue.value > 3f)
			{
				variationSliderImage.color = variationSliderNormalColor;
				DOTween.Kill(variationSliderImage);
			}
			if (variationSliderValue.value <= 0f)
			{
				isColorChanging = true;
				isVariation = false;
			}
		}
	}

	public void SharePCode()
	{
		GameManager.ShareMessage("Check Here");
		//nativeShare.Share("Waiting for you to join my table on Teen Patti Tycoon, #1 card game. Use this code : " + privateTableCode + " to join me or click here to download the game http://bit.ly/tptgame", "Private Table Code");
	}

	public void OpenDealerCanvas(bool isTip)
	{
		dealerCanvas.SetActive(true);
		tipCanvas.SetActive(isTip);
		changeDealerCanvas.SetActive(!isTip);

		dealerImage.sprite = dealerGirls[dealerIndex].sprite;
		dealerName.text = dealerGirls[dealerIndex].name;

		tipIndex = 0;
		edDealerButton();
	}

	public void CloseDealerCanvas()
	{
		dealerCanvas.SetActive(false);
	}

	public void TipButtonI()
	{
		tipIndex++;
		edDealerButton();
	}

	public void TipButtonD()
	{
		tipIndex--;
		edDealerButton();
	}

	public void TipButton()
    {
        if (GameManager.Instance.localPlayerChips > tipValue[tipIndex])
        {
			// get tip
			//CloseDealerCanvas();

		}
        else
        {
			NativeUI.ShowToast("You don't have enough chips");
		}
    }

	public void ChangeDealer(int index)
    {
		if (GameManager.Instance.localPlayerChips > tipValue[tipIndex])
		{
			dealerIndex = index;

			// call socket for chnage dealer

			//CloseDealerCanvas();
		}
		else
		{
			NativeUI.ShowToast("You don't have enough chips");
		}
	}

	private void edDealerButton()
	{
		if (tipIndex >= tipValue.Length - 1)
		{
			itipButton.interactable = false;
		}
		else
		{
			itipButton.interactable = true;
		}

		if (tipIndex <= 0)
		{
			dtipButton.interactable = false;
		}
		else
		{
			dtipButton.interactable = true;
		}

		tipText.text = GameManager.Instance.format.FormatNumberOnTable(tipValue[tipIndex]);
	}



	[System.Serializable]
    public class DealerGirl
    {
		public Sprite sprite;
		public string name;
    }

}
