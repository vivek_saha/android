using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
[ExecuteInEditMode]
[AddComponentMenu("2DxFX/Standard/Offset")]
public class _2dxFX_Offset : MonoBehaviour
{
	[HideInInspector]
	public Material ForceMaterial;

	[HideInInspector]
	public bool ActiveChange = true;

	private string shader = "2DxFX/Standard/Offset";

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Alpha = 1f;

	[HideInInspector]
	[Range(-1f, 1f)]
	public float _OffsetX;

	[HideInInspector]
	[Range(-1f, 1f)]
	public float _OffsetY;

	[HideInInspector]
	[Range(0.001f, 8f)]
	public float _ZoomX = 1f;

	[HideInInspector]
	[Range(0.001f, 8f)]
	public float _ZoomY = 1f;

	[HideInInspector]
	[Range(0.001f, 64f)]
	public float _ZoomXY = 1f;

	[HideInInspector]
	public bool _AutoScrollX;

	[HideInInspector]
	[Range(-100f, 100f)]
	public float _AutoScrollSpeedX;

	[HideInInspector]
	public bool _AutoScrollY;

	[HideInInspector]
	[Range(-100f, 100f)]
	public float _AutoScrollSpeedY;

	[HideInInspector]
	private float _AutoScrollCountX;

	[HideInInspector]
	private float _AutoScrollCountY;

	[HideInInspector]
	public int ShaderChange;

	private Material tempMaterial;

	private Material defaultMaterial;

	private Image CanvasImage;

	private void Awake()
	{
		if (base.gameObject.GetComponent<Image>() != null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
	}

	private void Start()
	{
		ShaderChange = 0;
	}

	public void CallUpdate()
	{
		Update();
	}

	private void Update()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (ShaderChange == 0 && ForceMaterial != null)
		{
			ShaderChange = 1;
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
			ForceMaterial.hideFlags = HideFlags.None;
			ForceMaterial.shader = Shader.Find(shader);
		}
		if (ForceMaterial == null && ShaderChange == 1)
		{
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
			ShaderChange = 0;
		}
		if (!ActiveChange)
		{
			return;
		}
		if (base.gameObject.GetComponent<SpriteRenderer>() != null)
		{
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Alpha", 1f - _Alpha);
			if (_AutoScrollX)
			{
				_AutoScrollCountX += _AutoScrollSpeedX * 0.01f * Time.deltaTime;
				if (_AutoScrollCountX < 0f)
				{
					_AutoScrollCountX = 1f;
				}
				GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetX", 1f + _AutoScrollCountX);
			}
			else
			{
				GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetX", 1f + _OffsetX);
			}
			if (_AutoScrollY)
			{
				_AutoScrollCountY += _AutoScrollSpeedY * 0.01f * Time.deltaTime;
				if (_AutoScrollCountY < 0f)
				{
					_AutoScrollCountY = 1f;
				}
				GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetY", 1f + _AutoScrollCountY);
			}
			else
			{
				GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetY", 1f + _OffsetY);
			}
			GetComponent<Renderer>().sharedMaterial.SetFloat("_ZoomX", _ZoomX * _ZoomXY);
			GetComponent<Renderer>().sharedMaterial.SetFloat("_ZoomY", _ZoomY * _ZoomXY);
		}
		else
		{
			if (!(base.gameObject.GetComponent<Image>() != null))
			{
				return;
			}
			CanvasImage.material.SetFloat("_Alpha", 1f - _Alpha);
			if (_AutoScrollX)
			{
				_AutoScrollCountX += _AutoScrollSpeedX * 0.01f * Time.deltaTime;
				if (_AutoScrollCountX < 0f)
				{
					_AutoScrollCountX = 1f;
				}
				CanvasImage.material.SetFloat("_OffsetX", 1f + _AutoScrollCountX);
			}
			else
			{
				CanvasImage.material.SetFloat("_OffsetX", 1f + _OffsetX);
			}
			if (_AutoScrollY)
			{
				_AutoScrollCountY += _AutoScrollSpeedY * 0.01f * Time.deltaTime;
				if (_AutoScrollCountY < 0f)
				{
					_AutoScrollCountY = 1f;
				}
				CanvasImage.material.SetFloat("_OffsetY", 1f + _AutoScrollCountY);
			}
			else
			{
				CanvasImage.material.SetFloat("_OffsetY", 1f + _OffsetY);
			}
			CanvasImage.material.SetFloat("_ZoomX", _ZoomX * _ZoomXY);
			CanvasImage.material.SetFloat("_ZoomY", _ZoomY * _ZoomXY);
		}
	}

	private void OnDestroy()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (Application.isPlaying || !Application.isEditor)
		{
			return;
		}
		if (tempMaterial != null)
		{
			UnityEngine.Object.DestroyImmediate(tempMaterial);
		}
		if (base.gameObject.activeSelf && defaultMaterial != null)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnDisable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (base.gameObject.activeSelf && defaultMaterial != null)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnEnable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (defaultMaterial == null)
		{
			defaultMaterial = new Material(Shader.Find("Sprites/Default"));
		}
		if (ForceMaterial == null)
		{
			ActiveChange = true;
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
		}
		else
		{
			ForceMaterial.shader = Shader.Find(shader);
			ForceMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
		}
	}
}
