using UnityEngine;

public class PopupView : MonoBehaviour
{
    [Tooltip("market://details?id=BUNDLE-ID")]
    public string gameLink = "market://details?id=com.tag.tabletennis3D";

    private void OnEnable()
    {
        AndroidRateUsPopUp.onRateUSPopupComplete += OnRateUSPopupComplete;
        AndroidDialog.onDialogPopupComplete += OnDialogPopupComplete;
        AndroidMessage.onMessagePopupComplete += OnMessagePopupComplete;
    }

    private void OnDisable()
    {
        AndroidRateUsPopUp.onRateUSPopupComplete -= OnRateUSPopupComplete;
        AndroidDialog.onDialogPopupComplete -= OnDialogPopupComplete;
        AndroidMessage.onMessagePopupComplete -= OnMessagePopupComplete;
    }

    private void OnRateUSPopupComplete(MessageState state)
    {
        switch (state)
        {
            case MessageState.RATED:
                Debug.Log("Rate Button pressed");
                break;
            case MessageState.REMIND:
                Debug.Log("Remind Button pressed");
                break;
            case MessageState.DECLINED:
                Debug.Log("Declined Button pressed");
                break;
        }
    }

    private void OnDialogPopupComplete(MessageState state)
    {
        switch (state)
        {
            case MessageState.YES:
                UnityEngine.Debug.Log("Yes button pressed");
                break;
            case MessageState.NO:
                UnityEngine.Debug.Log("No button pressed");
                break;
        }
    }

    private void OnMessagePopupComplete(MessageState state)
    {
        UnityEngine.Debug.Log("Ok button Clicked");
    }

    public void OnDialogPopUp()
    {
        NativeDialog nativeDialog = new NativeDialog("TheAppGuruz", "Do you wants to know about TheAppGuruz");
        nativeDialog.SetUrlString("http://theappguruz.com/");
        nativeDialog.init();
    }

    public void OnRatePopUp()
    {
        NativeRateUS nativeRateUS = new NativeRateUS("Like this game?", "Please rate to support future updates!");
        nativeRateUS.SetAppLink(gameLink);
        nativeRateUS.InitRateUS();
    }

    public void OnMessagePopUp()
    {
    }
}
