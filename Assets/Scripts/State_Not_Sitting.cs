using UnityEngine;

public class State_Not_Sitting : MonoBehaviour
{
	[HideInInspector]
	public PlayerElements playerElements;

	private void Awake()
	{
		playerElements = GetComponent<PlayerElements>();
	}

	private void Start()
	{
		NotSitting(true);
	}

	public void NotSitting(bool _isFirstTime)
	{
		playerElements.RemovePlayer(_isFirstTime);
	}
}
