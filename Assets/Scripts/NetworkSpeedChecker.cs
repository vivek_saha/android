using UnityEngine;
using UnityEngine.UI;

public class NetworkSpeedChecker : MonoBehaviour
{
	public Image networkImage;

	public Sprite[] sprites;

	private void Update()
	{
		if (GameManager.Instance.internetSpeed > 100.0)
		{
			networkImage.sprite = sprites[0];
		}
		else if (GameManager.Instance.internetSpeed > 50.0)
		{
			networkImage.sprite = sprites[1];
		}
		else if (GameManager.Instance.internetSpeed > 25.0)
		{
			networkImage.sprite = sprites[2];
		}
		else
		{
			networkImage.sprite = sprites[3];
		}
	}
}
