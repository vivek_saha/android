using System.IO;
using UnityEngine;

public class URL : MonoBehaviour
{
    public static URL Instance;

    [Header("URL")]
    private string BASEURL = "http://134.209.103.120";
    private string PORT = "5000";

    [HideInInspector]
    public string ASSET_BUNDLE_URL;

    [HideInInspector]
    public string DEVICE_TYPE = "Android/";

    [Header("Scene Name")]
    [HideInInspector]
    public string LOADING_SCENE = "LoadingScene";

    [HideInInspector]
    public string LOGIN_SCENE = "LoginScene";

    [HideInInspector]
    public string MAIN_MENU_SCENE = "MainMenuScene";

    [HideInInspector]
    public string TABLE_SCENE = "TableScene";


    [Header("API Name")]
    [HideInInspector]
    public string VERSION_CHECK = "getVersion";

    [HideInInspector]
    public string MAINTENANCE_CHECK = "getMmode";

    [HideInInspector]
    public string USER_LOGIN_API = "onUserLogin";

    [HideInInspector]
    public string DEALS_LISTING_API = "dealsListing";

    [HideInInspector]
    public string DAILY_REWARD_API = "dailyReward";

    [HideInInspector]
    public string TAP_BONUS_API = "tapBonus";

    [HideInInspector]
    public string FIND_TABLE = "findTable";

    [HideInInspector]
    public string PRIVATE_TABLE_JOIN = "private_table_code";

    [HideInInspector]
    public string TABLE_LIST = "tableList";

    [HideInInspector]
    public string PURCHASE_CHIPS = "purchase_chips";

    [HideInInspector]
    public string DEAL_CHIPS = "purchase_deal_chips";

    [HideInInspector]
    public string PACKAGE_LISTING = "packageListing";

    [HideInInspector]
    public string IAP_COMPLETE = "iap_complete";

    [HideInInspector]
    public string VIDEO_REWARD = "videoSeenReward";

    [HideInInspector]
    public string PROMOTIONAL_IMAGE = "getPromotionalImage";

    [HideInInspector]
    public string SLOTS = "getSlots";

    [HideInInspector]
    public string NAME_SELECTOR = "nameSelector";

    [HideInInspector]
    public string MULTI_LOGIN = "checkUserMultiLogin";

    [HideInInspector]
    public string UPDATE_VIP_DETAILS = "updatevipdetails";

    [HideInInspector]
    public string LIVE_VIDEO_URL = "getYoutubeUrl";

    [HideInInspector]
    public string INVITATION_CODE = "invitationCode";

    [HideInInspector]
    public string ADD_Chips = "add_chips";

    [HideInInspector]
    public string CUT_CHIPS = "cut_chips";

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(base.gameObject);
        }
        DontDestroyOnLoad(base.gameObject);
    }

    private void Start()
    {
        ASSET_BUNDLE_URL = Path.Combine(BASEURL, "mohit-images/");
        Instance.DebugPrint("URL PATH : " + ASSET_BUNDLE_URL);
    }

    private string BASE_URL()
    {
        return BASEURL + ":" + PORT + "/";
    }

    private string API(string _api)
    {
        return _api;
    }

    public string AssetBundleURL()
    {
        return ASSET_BUNDLE_URL + DEVICE_TYPE;
    }

    public string CALLAPI(string _api, string _response)
    {
        if (_response.Length > 0)
        {
            return BASE_URL() + API(_api) + "?id=" + _response;
        }
        else
        {
            return BASE_URL() + API(_api);
        }
    }

    public string CALLAPI(string _api)
    {
        return BASE_URL() + API(_api);
    }

    public void DebugPrint(object _message)
    {
        Debug.LogError(_message);
    }

    public string TrimName(string _name)
    {
        return _name;

        /*if (GameManager.Instance.isFacebookLogin)
        {
            return _name;
        }
        string text = _name.Trim();
        string result = text;
        if (text.Length > 0)
        {
            result = "TPT_" + text.Substring(0, 3).Trim();
        }
        return result;*/
    }

    public string TrimNameForOthers(string _name, bool _isLoginWithFacebook, string _tempname)
    {
        return _name;

        /*if (_isLoginWithFacebook)
        {
            if (_tempname == "-1")
            {
                char[] separator = new char[2]
                {
                    ' ',
                    '\t'
                };
                string[] array = _name.Split(separator);
                return array[0];
            }
            return _tempname;
        }
        string text = _name.Trim();
        string result = text;
        if (text.Length > 0)
        {
            result = "TPT_" + text.Substring(0, 3).Trim();
        }
        return result;*/
    }

    public string TrimNameForSideshow(string _name, string _isLoginWithFacebook, string _tempname)
    {
        if (_isLoginWithFacebook == "1")
        {
            if (_tempname == "-1")
            {
                char[] separator = new char[2]
                {
                    ' ',
                    '\t'
                };
                string[] array = _name.Split(separator);
                return array[0];
            }
            return _tempname;
        }
        string text = _name.Trim();
        string result = text;
        if (text.Length > 0)
        {
            result = "TPT_" + text.Substring(0, 3).Trim();
        }
        return result;
    }
}
