using UnityEngine;

public class State_Game_Starts : MonoBehaviour
{
	[HideInInspector]
	public PlayerElements playerElements;

	private void Awake()
	{
		playerElements = GetComponent<PlayerElements>();
	}

	public void GameStarts()
	{
		playerElements.ResetColor();
		playerElements.CardsDistribute();
	}
}
