using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
[ExecuteInEditMode]
[AddComponentMenu("2DxFX/Standard/Pattern")]
public class _2dxFX_Pattern : MonoBehaviour
{
	[HideInInspector]
	public Material ForceMaterial;

	[HideInInspector]
	public bool ActiveChange = true;

	private string shader = "2DxFX/Standard/Pattern";

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Alpha = 1f;

	[HideInInspector]
	public Texture2D __MainTex2;

	[HideInInspector]
	public float _OffsetX;

	[HideInInspector]
	public float _OffsetY;

	[HideInInspector]
	public bool _AutoScrollX;

	[HideInInspector]
	[Range(-3f, 3f)]
	public float _AutoScrollSpeedX;

	[HideInInspector]
	public bool _AutoScrollY;

	[HideInInspector]
	[Range(-3f, 3f)]
	public float _AutoScrollSpeedY;

	[HideInInspector]
	private float _AutoScrollCountX;

	[HideInInspector]
	private float _AutoScrollCountY;

	[HideInInspector]
	public int ShaderChange;

	private Material tempMaterial;

	private Material defaultMaterial;

	private Image CanvasImage;

	private void Awake()
	{
		if (base.gameObject.GetComponent<Image>() != null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
	}

	private void Start()
	{
		ShaderChange = 0;
	}

	public void CallUpdate()
	{
		Update();
	}

	private void Update()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (ShaderChange == 0 && ForceMaterial != null)
		{
			ShaderChange = 1;
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
			ForceMaterial.hideFlags = HideFlags.None;
			ForceMaterial.shader = Shader.Find(shader);
			ActiveChange = false;
		}
		if (ForceMaterial == null && ShaderChange == 1)
		{
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
			ShaderChange = 0;
		}
		if (!ActiveChange)
		{
			return;
		}
		if (base.gameObject.GetComponent<SpriteRenderer>() != null)
		{
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Alpha", 1f - _Alpha);
			if (!_AutoScrollX && !_AutoScrollY)
			{
				GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetX", _OffsetX);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetY", _OffsetY);
			}
			if (_AutoScrollX && !_AutoScrollY)
			{
				_AutoScrollCountX += _AutoScrollSpeedX * Time.deltaTime;
				GetComponent<Renderer>().material.SetFloat("_OffsetX", _AutoScrollCountX);
				GetComponent<Renderer>().material.SetFloat("_OffsetY", _OffsetY);
			}
			if (!_AutoScrollX && _AutoScrollY)
			{
				_AutoScrollCountY += _AutoScrollSpeedY * Time.deltaTime;
				GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetX", _OffsetX);
				GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetY", _AutoScrollCountY);
			}
			if (_AutoScrollX && _AutoScrollY)
			{
				_AutoScrollCountX += _AutoScrollSpeedX * Time.deltaTime;
				GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetX", _AutoScrollCountX);
				_AutoScrollCountY += _AutoScrollSpeedY * Time.deltaTime;
				GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetY", _AutoScrollCountY);
			}
		}
		else if (base.gameObject.GetComponent<Image>() != null)
		{
			CanvasImage.material.SetFloat("_Alpha", 1f - _Alpha);
			if (!_AutoScrollX && !_AutoScrollY)
			{
				CanvasImage.material.SetFloat("_OffsetX", _OffsetX);
				CanvasImage.material.SetFloat("_OffsetY", _OffsetY);
			}
			if (_AutoScrollX && !_AutoScrollY)
			{
				_AutoScrollCountX += _AutoScrollSpeedX * Time.deltaTime;
				CanvasImage.material.SetFloat("_OffsetX", _AutoScrollCountX);
				CanvasImage.material.SetFloat("_OffsetY", _OffsetY);
			}
			if (!_AutoScrollX && _AutoScrollY)
			{
				_AutoScrollCountY += _AutoScrollSpeedY * Time.deltaTime;
				CanvasImage.material.SetFloat("_OffsetX", _OffsetX);
				CanvasImage.material.SetFloat("_OffsetY", _AutoScrollCountY);
			}
			if (_AutoScrollX && _AutoScrollY)
			{
				_AutoScrollCountX += _AutoScrollSpeedX * Time.deltaTime;
				CanvasImage.material.SetFloat("_OffsetX", _AutoScrollCountX);
				_AutoScrollCountY += _AutoScrollSpeedY * Time.deltaTime;
				CanvasImage.material.SetFloat("_OffsetY", _AutoScrollCountY);
			}
		}
		if (_AutoScrollCountX > 1f)
		{
			_AutoScrollCountX = 0f;
		}
		if (_AutoScrollCountX < -1f)
		{
			_AutoScrollCountX = 0f;
		}
		if (_AutoScrollCountY > 1f)
		{
			_AutoScrollCountY = 0f;
		}
		if (_AutoScrollCountY < -1f)
		{
			_AutoScrollCountY = 0f;
		}
	}

	private void OnDestroy()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (Application.isPlaying || !Application.isEditor)
		{
			return;
		}
		if (ForceMaterial != null && tempMaterial != null)
		{
			UnityEngine.Object.DestroyImmediate(tempMaterial);
		}
		if (base.gameObject.activeSelf)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnDisable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (ForceMaterial != null && tempMaterial != null)
		{
			UnityEngine.Object.DestroyImmediate(tempMaterial);
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnEnable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		defaultMaterial = new Material(Shader.Find("Sprites/Default"));
		if (ForceMaterial == null)
		{
			ActiveChange = true;
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
		}
		else
		{
			ForceMaterial.shader = Shader.Find(shader);
			ForceMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
		}
		if ((bool)__MainTex2)
		{
			__MainTex2.wrapMode = TextureWrapMode.Repeat;
			GetComponent<Renderer>().sharedMaterial.SetTexture("_MainTex2", __MainTex2);
		}
	}
}
