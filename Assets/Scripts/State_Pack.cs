using DG.Tweening;
using UnityEngine;

public class State_Pack : MonoBehaviour
{
	[HideInInspector]
	public PlayerElements playerElements;

	private void Awake()
	{
		playerElements = GetComponent<PlayerElements>();
	}

	public void UserPack()
	{
		playerElements.PackColor();
		playerElements.ResetPlayerTimer();
		if (playerElements.isLocalPlayer)
		{
			playerElements.tElements.UserPackLocally();
			playerElements.viewCardButton.gameObject.SetActive(value: false);
		}
		else
		{
			//playerElements.seenGO.SetActive(value: false);
		}
		/*playerElements.packGO.transform.localScale = Vector3.zero;
		playerElements.packGO.transform.rotation = Quaternion.Euler(0f, 0f, 180f);
		playerElements.packGO.SetActive(value: true);
		playerElements.packGO.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBack);
		playerElements.packGO.transform.DORotate(Vector3.zero, 0.5f).SetEase(Ease.OutBack);*/
		playerElements.cardStatus.text = "Packed";
	}
}
