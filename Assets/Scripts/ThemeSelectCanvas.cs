﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using EasyMobile;
using UniRx;
using System;

public class ThemeSelectCanvas : MonoBehaviour
{

    public Image backButton;
    public Sprite[] backSprite;

    public Image settingButton;
    public Sprite[] settingSprite;

    public Image themeButton;
    public Sprite[] themeSprite;

    public Image chatButton;
    public Sprite[] chatSprite;

    public Image infoButton;
    public Sprite[] infoSprite;

    public Image buyButton;
    public Sprite[] buySprite;

    public Image plusButton;
    public Sprite[] plusSprite;

    public Image minusButton;
    public Sprite[] minusSprite;

    public Image betButton;
    public Sprite[] betSprite;

    public Image table;
    public Sprite[] tableSprite;

    public Image background;
    public Sprite[] backgroundSprite;

    public Image[] otherPlayButtons;
    public Sprite[] otherPlaySprite;

    public SpriteRenderer[] cardBack;
    public Sprite[] cardBackSprite;

    public GameObject price;
    public GameObject purchase;
    public GameObject select;

    public GameObject[] theme;
    public int[] themePrice;

    public RectTransform content;
    public RectTransform center;

    private float[] distance;
    private bool dragging;
    private int minThemeNum;
    private int themeLength;
    private bool msgSend;

    private int currantIndex;


    private void Start()
    {
        themeLength = theme.Length;
        distance = new float[themeLength];
    }

    private void Update()
    {
        for (int i = 0; i < theme.Length; i++)
        {
            distance[i] = Mathf.Abs(center.position.x - theme[i].GetComponent<RectTransform>().position.x);
        }

        float minDistance = Mathf.Min(distance);

        for (int i = 0; i < theme.Length; i++)
        {
            if (minDistance == distance[i])
            {
                minThemeNum = i;
            }
        }

        if (!dragging)
        {
            MoveTheme(-theme[minThemeNum].GetComponent<RectTransform>().anchoredPosition.x);
        }

    }

    private void MoveTheme(float postion)
    {
        float newx = Mathf.Lerp(content.anchoredPosition.x, postion, Time.deltaTime * 20f);

        if (Mathf.Abs(postion - newx) < 20f)
        {
            newx = postion;
        }

        if (Mathf.Abs(newx) >= Mathf.Abs(postion) -1f && Mathf.Abs(newx) <= Mathf.Abs(postion) + 1 && !msgSend)
        {
            msgSend = true;
            SendMsgButton(minThemeNum);
        }

        Vector2 newpos = new Vector2(newx, content.anchoredPosition.y);
        content.anchoredPosition = newpos;
    }


    private void SendMsgButton(int index)
    {
        string unlocked = PlayerPrefs.GetString("ulTheme", "0");
        currantIndex = index;

        for (int i = 0; i < theme.Length; i++)
        {
            if (i == index)
            {
                theme[i].transform.DOScale(Vector3.one, .15f);
            }
            else
            {
                theme[i].transform.DOScale(new Vector3(.9f, .9f, .9f), .15f);
            }

            if (unlocked.Contains(index.ToString()))
            {
                price.SetActive(false);
                purchase.SetActive(false);
                select.SetActive(true);
            }
            else
            {
                price.SetActive(true);
                purchase.SetActive(true);
                select.SetActive(false);
                price.GetComponentInChildren<Text>().text = "₹ " + themePrice[index];
            }
        }
    }

    public void SelectButton()
    {
        PlayerPrefs.SetInt("cTheme", currantIndex);
        SetCurrantTheme();
        NativeUI.ShowToast("Set Theme Successfully");
    }

    public void PurchaseButton()
    {
        if (GameManager.Instance.localPlayerChips > themePrice[currantIndex])
        {
            // purchess theme

            JSONObject jSONObject = new JSONObject();
            jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
            jSONObject.AddField("amount", themePrice[currantIndex]);
            jSONObject.AddField("type", "user_purchase_theme");

            ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.CUT_CHIPS, jSONObject.ToString())).Subscribe(delegate (string x)
            {
                PlayerData playerData = PlayerData.CreateFromJSON(x);
                if (playerData.status == "success")
                {
                    GameManager.Instance.localPlayerChips = long.Parse(playerData.response.chips);

                    string unlocked = PlayerPrefs.GetString("ulTheme", "0");
                    if (!unlocked.Contains(currantIndex.ToString()))
                    {
                        unlocked = "," + currantIndex;
                        PlayerPrefs.SetString("ulTheme", unlocked);
                    }

                    price.SetActive(false);
                    purchase.SetActive(false);
                    select.SetActive(true);

                }
            }, delegate (Exception ex)
            {
                Debug.LogException(ex);
                NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
            });
        }
        else
        {
            NativeUI.ShowToast("You don't have enough chips.");
        }
    }

    public void StartDrag()
    {
        msgSend = false;
        dragging = true;
    }

    public void EndDrag()
    {
        dragging = false;
    }

    public void SetCurrantTheme()
    {
        int ctheme = PlayerPrefs.GetInt("cTheme", 0);

        backButton.sprite = backSprite[ctheme];
        settingButton.sprite = settingSprite[ctheme];
        themeButton.sprite = themeSprite[ctheme];
        chatButton.sprite = chatSprite[ctheme];
        infoButton.sprite = infoSprite[ctheme];
        buyButton.sprite = buySprite[ctheme];
        plusButton.sprite = plusSprite[ctheme];
        minusButton.sprite = minusSprite[ctheme];
        betButton.sprite = betSprite[ctheme];
        table.sprite = tableSprite[ctheme];
        background.sprite = backgroundSprite[ctheme];

        foreach (var item in otherPlayButtons)
        {
            item.sprite = otherPlaySprite[ctheme];
        }

        foreach (var item in cardBack)
        {
            item.sprite = cardBackSprite[ctheme];
        }
    }


}
