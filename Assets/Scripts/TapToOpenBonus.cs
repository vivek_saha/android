using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TapToOpenBonus : MonoBehaviour
{
	[SerializeField]
	private GameObject bonusGO;

	[SerializeField]
	private GameObject chipsPrefabGO;

	[SerializeField]
	private Transform chipsPos;

	[SerializeField]
	private Transform mainchipsPos;

	public Animator animator;

	public Button tapButtonInter;

	public GameObject tapToOpenGO;

	public CanvasGroup winningPanel;

	public CanvasGroup congratsGO;

	public TextMeshProUGUI chipsText;

	public TextMeshProUGUI forceText;

	public GameObject collectButton;

	public Transform[] waypoints;

	public Vector3[] waypointsVector;

	private int chipCounter;

	private void Start()
	{
		waypointsVector = new Vector3[waypoints.Length];
		for (int i = 0; i < waypointsVector.Length; i++)
		{
			waypointsVector[i] = waypoints[i].position;
		}
	}

	public void CollectButtonOnCLick()
	{
		InvokeRepeating("InstChips", 0f, 0.1f);
	}

	private void InstChips()
	{
		chipCounter++;
		if (chipCounter <= 10)
		{
			GameObject chipP = Instantiate(chipsPrefabGO, chipsPos.position, Quaternion.identity);
			chipP.transform.DOScale(new Vector3(0.3f, 0.3f, 0.3f), 1f);
			Sequence s = DOTween.Sequence();
			s.PrependInterval(0.5f);
			s.AppendCallback(delegate
			{
				AudioManager.instance.PlaySound("click");
			});
			chipP.transform.DOPath(waypointsVector, 1f, PathType.CatmullRom, PathMode.Full3D, 5, Color.red).SetEase(Ease.OutQuint).OnComplete(delegate
			{
				mainchipsPos.DOShakeScale(0.1f, 1f, 10, 90f, fadeOut: false).OnComplete(delegate
				{
					mainchipsPos.localScale = Vector3.one;
				});
				Destroy(chipP);
			});
		}
		else
		{
			CancelInvoke("InstChips");
			bonusGO.SetActive(false);
		}
	}
}
