using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

[Serializable]
[ExecuteInEditMode]
[AddComponentMenu("2DxFX/Advanced Lightning/Mystic_Distortion")]
public class _2dxFX_AL_Mystic_Distortion : MonoBehaviour
{
	[HideInInspector]
	public Material ForceMaterial;

	[HideInInspector]
	public bool ActiveChange = true;

	[HideInInspector]
	public bool AddShadow = true;

	[HideInInspector]
	public bool ReceivedShadow;

	[HideInInspector]
	public int BlendMode;

	private string shader = "2DxFX/AL/Mystic_Distortion";

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Alpha = 1f;

	[HideInInspector]
	[Range(0f, 0.45f)]
	public float _Pitch = 0.45f;

	[HideInInspector]
	public bool Pitch_Wave = true;

	[HideInInspector]
	[Range(0f, 16f)]
	public float _Pitch_Speed = 1f;

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Pitch_Offset;

	[HideInInspector]
	[Range(0f, 128f)]
	public float _OffsetX = 56f;

	[HideInInspector]
	[Range(0f, 128f)]
	public float _OffsetY = 28f;

	[HideInInspector]
	[Range(0f, 1f)]
	public float _DistanceX = 0.01f;

	[HideInInspector]
	[Range(0f, 1f)]
	public float _DistanceY = 0.04f;

	[HideInInspector]
	[Range(0f, 6.28f)]
	public float _WaveTimeX = 1.16f;

	[HideInInspector]
	[Range(0f, 6.28f)]
	public float _WaveTimeY = 5.12f;

	[HideInInspector]
	public bool AutoPlayWaveX;

	[HideInInspector]
	[Range(0f, 5f)]
	public float AutoPlaySpeedX = 5f;

	[HideInInspector]
	public bool AutoPlayWaveY;

	[HideInInspector]
	[Range(0f, 50f)]
	public float AutoPlaySpeedY = 5f;

	[HideInInspector]
	public bool AutoRandom;

	[HideInInspector]
	[Range(0f, 50f)]
	public float AutoRandomRange = 10f;

	[HideInInspector]
	public int ShaderChange;

	private Material tempMaterial;

	private Material defaultMaterial;

	private Image CanvasImage;

	private void Awake()
	{
		if (base.gameObject.GetComponent<Image>() != null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
	}

	private void Start()
	{
		ShaderChange = 0;
	}

	public void CallUpdate()
	{
		Update();
	}

	private void Update()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (ShaderChange == 0 && ForceMaterial != null)
		{
			ShaderChange = 1;
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
			ForceMaterial.hideFlags = HideFlags.None;
			ForceMaterial.shader = Shader.Find(shader);
		}
		if (ForceMaterial == null && ShaderChange == 1)
		{
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
			ShaderChange = 0;
		}
		if (!ActiveChange)
		{
			return;
		}
		if (Pitch_Wave)
		{
			_Pitch_Offset = Mathf.Sin(Time.time * _Pitch_Speed) * 0.05f;
			UnityEngine.Debug.Log(_Pitch_Offset);
		}
		else
		{
			_Pitch_Offset = 0f;
		}
		if (base.gameObject.GetComponent<SpriteRenderer>() != null)
		{
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Alpha", 1f - _Alpha);
			if (_2DxFX.ActiveShadow && AddShadow)
			{
				GetComponent<Renderer>().shadowCastingMode = ShadowCastingMode.On;
				if (ReceivedShadow)
				{
					GetComponent<Renderer>().receiveShadows = true;
					GetComponent<Renderer>().sharedMaterial.renderQueue = 2450;
					GetComponent<Renderer>().sharedMaterial.SetInt("_Z", 1);
				}
				else
				{
					GetComponent<Renderer>().receiveShadows = false;
					GetComponent<Renderer>().sharedMaterial.renderQueue = 3000;
					GetComponent<Renderer>().sharedMaterial.SetInt("_Z", 0);
				}
			}
			else
			{
				GetComponent<Renderer>().shadowCastingMode = ShadowCastingMode.Off;
				GetComponent<Renderer>().receiveShadows = false;
				GetComponent<Renderer>().sharedMaterial.renderQueue = 3000;
				GetComponent<Renderer>().sharedMaterial.SetInt("_Z", 0);
			}
			if (BlendMode == 0)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 0);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 1);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 10);
			}
			if (BlendMode == 1)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 0);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 1);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 1);
			}
			if (BlendMode == 2)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 2);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 1);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 2);
			}
			if (BlendMode == 3)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 4);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 1);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 1);
			}
			if (BlendMode == 4)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 2);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 1);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 1);
			}
			if (BlendMode == 5)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 4);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 10);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 10);
			}
			if (BlendMode == 6)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 0);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 2);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 10);
			}
			if (BlendMode == 7)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 0);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 4);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 1);
			}
			if (BlendMode == 8)
			{
				GetComponent<Renderer>().sharedMaterial.SetInt("_BlendOp", 2);
				GetComponent<Renderer>().sharedMaterial.SetInt("_SrcBlend", 7);
				GetComponent<Renderer>().sharedMaterial.SetInt("_DstBlend", 2);
			}
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Pitch", _Pitch + _Pitch_Offset);
			GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetX", _OffsetX);
			GetComponent<Renderer>().sharedMaterial.SetFloat("_OffsetY", _OffsetY);
			GetComponent<Renderer>().sharedMaterial.SetFloat("_DistanceX", _DistanceX);
			GetComponent<Renderer>().sharedMaterial.SetFloat("_DistanceY", _DistanceY);
			GetComponent<Renderer>().sharedMaterial.SetFloat("_WaveTimeX", _WaveTimeX);
			GetComponent<Renderer>().sharedMaterial.SetFloat("_WaveTimeY", _WaveTimeY);
		}
		else if (base.gameObject.GetComponent<Image>() != null)
		{
			CanvasImage.material.SetFloat("_Alpha", 1f - _Alpha);
			CanvasImage.material.SetFloat("_Pitch", _Pitch + _Pitch_Offset);
			CanvasImage.material.SetFloat("_OffsetX", _OffsetX);
			CanvasImage.material.SetFloat("_OffsetY", _OffsetY);
			CanvasImage.material.SetFloat("_DistanceX", _DistanceX);
			CanvasImage.material.SetFloat("_DistanceY", _DistanceY);
			CanvasImage.material.SetFloat("_WaveTimeX", _WaveTimeX);
			CanvasImage.material.SetFloat("_WaveTimeY", _WaveTimeY);
		}
		float num = (!AutoRandom) ? Time.deltaTime : (UnityEngine.Random.Range(1f, AutoRandomRange) / 5f * Time.deltaTime);
		if (AutoPlayWaveX)
		{
			_WaveTimeX += AutoPlaySpeedX * num;
		}
		if (AutoPlayWaveY)
		{
			_WaveTimeY += AutoPlaySpeedY * num;
		}
		if (_WaveTimeX > 6.28f)
		{
			_WaveTimeX = 0f;
		}
		if (_WaveTimeY > 6.28f)
		{
			_WaveTimeY = 0f;
		}
	}

	private void OnDestroy()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (Application.isPlaying || !Application.isEditor)
		{
			return;
		}
		if (tempMaterial != null)
		{
			UnityEngine.Object.DestroyImmediate(tempMaterial);
		}
		if (base.gameObject.activeSelf && defaultMaterial != null)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnDisable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (base.gameObject.activeSelf && defaultMaterial != null)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnEnable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (defaultMaterial == null)
		{
			defaultMaterial = new Material(Shader.Find("Sprites/Default"));
		}
		if (ForceMaterial == null)
		{
			ActiveChange = true;
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
		}
		else
		{
			ForceMaterial.shader = Shader.Find(shader);
			ForceMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
		}
	}
}
