using System;

[Serializable]
public class SelectVariationJSON
{
	public string var_type;

	public SelectVariationJSON(string _var_type)
	{
		var_type = _var_type;
	}
}
