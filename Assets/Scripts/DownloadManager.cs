using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DownloadManager : MonoBehaviour
{
	public TextMeshProUGUI downloadingText;

	public Slider downloadSlider;

	public string url1 = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/The_Heavy-Bowery_Ballroom-1.jpg/1200px-The_Heavy-Bowery_Ballroom-1.jpg";

	public string url2 = "http://www.teamfortress.com/images/posts/heavy_problem.jpg";

	public string url3 = "https://i.kym-cdn.com/entries/icons/original/000/021/830/maxresdefault.jpg";

	public string url4 = "https://i.ytimg.com/vi/gbYPjmEgo2c/maxresdefault.jpg";

	public string url5 = "https://i.kinja-img.com/gawker-media/image/upload/s--8mlv6KLF--/c_scale,f_auto,fl_progressive,q_80,w_800/187fsdxp9yu20jpg.jpg";

	public Queue<string> downloadResponses;

	private bool canDownload;

	public Image img;

	public List<DownloaderClass> dClass = new List<DownloaderClass>();

	private UnityWebRequest wr;

	private WWW www;

	private void Start()
	{
		downloadResponses = new Queue<string>();
		downloadResponses.Enqueue(url1);
		downloadResponses.Enqueue(url2);
		downloadResponses.Enqueue(url3);
		downloadResponses.Enqueue(url4);
		downloadResponses.Enqueue(url5);
		canDownload = true;
	}

	private void Update()
	{
		if (downloadResponses.Count > 0 && canDownload)
		{
			canDownload = false;
			StartCoroutine(DownloadImages(downloadResponses.Peek(), num() + ".png"));
		}
		downloadSlider.value = wr.downloadProgress;
	}

	private string num()
	{
		if (downloadResponses.Count == 5)
		{
			return "url1";
		}
		if (downloadResponses.Count == 4)
		{
			return "url2";
		}
		if (downloadResponses.Count == 3)
		{
			return "url3";
		}
		if (downloadResponses.Count == 2)
		{
			return "url4";
		}
		if (downloadResponses.Count == 1)
		{
			return "url5";
		}
		return "url1";
	}

	private IEnumerator DownloadImages(string _url, string _name)
	{
		downloadResponses.Dequeue();
		if (File.Exists(Path.Combine(Application.persistentDataPath, _name)))
		{
			downloadingText.text = "Loaded from device " + _name;
			byte[] array = File.ReadAllBytes(Path.Combine(Application.persistentDataPath, _name));
			if ((array[0] == 137 && array[1] == 80 && array[2] == 78 && array[3] == 71 && array[4] == 13 && array[5] == 10 && array[6] == 26 && array[7] == 10) || (array[0] == byte.MaxValue && array[1] == 216 && array[2] == byte.MaxValue && array[3] == 224 && array[4] == 0 && array[5] == 16 && array[6] == 74 && array[7] == 70 && array[8] == 73 && array[9] == 70))
			{
				Texture2D texture2D = new Texture2D(8, 8);
				texture2D.LoadImage(array);
				Sprite imgSprite = Sprite.Create(texture2D, new Rect(0f, 0f, texture2D.width, texture2D.height), Vector2.zero);
				dClass[0].imgSprite = imgSprite;
				img.sprite = dClass[0].imgSprite;
				img.preserveAspect = true;
				canDownload = true;
			}
			else
			{
				downloadingText.text = "Image not loaded from device" + _name;
			}
			yield break;
		}
		downloadingText.text = "Downloading : " + _name;
		wr = new UnityWebRequest(_url);
		DownloadHandlerTexture texDl = new DownloadHandlerTexture(readable: true);
		wr.downloadHandler = texDl;
		yield return wr.SendWebRequest();
		if (!wr.isNetworkError && !wr.isHttpError)
		{
			Sprite sprite = Sprite.Create(texDl.texture, new Rect(0f, 0f, texDl.texture.width, texDl.texture.height), Vector2.zero);
			img.sprite = sprite;
			img.preserveAspect = true;
			File.WriteAllBytes(Path.Combine(Application.persistentDataPath, _name), wr.downloadHandler.data);
			canDownload = true;
			if (downloadResponses.Count == 0)
			{
				downloadingText.text = "Download Complete";
			}
		}
		else
		{
			downloadingText.text = "Internet Error" + _name;
		}
	}
}
