using DG.Tweening;
using UnityEngine;

public class State_Game_Win : MonoBehaviour
{
	[HideInInspector]
	public PlayerElements playerElements;

	private void Awake()
	{
		playerElements = GetComponent<PlayerElements>();
	}

	public void WinnerCard()
	{
	}

	public void Winner()
	{
		if (playerElements.playWinnerAnimation)
		{
			playerElements.playWinnerAnimation = false;
			playerElements.winGO.SetActive(true);
			//playerElements.winGO_Particles.SetActive(true);
			playerElements.winnerTextImage.rectTransform.DOScale(Vector3.one, 1f);
			playerElements.winnerTextImage.rectTransform.DORotate(Vector3.zero, 1f).OnComplete(delegate
			{
				playerElements.winnerTextImage.rectTransform.DOScale(Vector3.zero, 0.5f).SetDelay(0.5f);
			});
		}
		playerElements.tElements.UserPackLocally();
		playerElements.ResetColor();
		playerElements.ResetPlayerTimer();
		if (playerElements.isLocalPlayer)
		{
			AudioManager.instance.PlaySound("player-winner");
		}
	}
}
