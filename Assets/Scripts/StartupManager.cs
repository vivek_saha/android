using System.Collections;
using UnityEngine;

public class StartupManager : MonoBehaviour
{
	public GameObject localisedScreen;

	public GameObject mainMenuScreen;

	private void Start()
	{
		localisedScreen.SetActive(false);
		mainMenuScreen.SetActive(true);
	}
}
