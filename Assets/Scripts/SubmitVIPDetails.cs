using EasyMobile;
using System;
using TMPro;
using UniRx;
using UnityEngine;

public class SubmitVIPDetails : MonoBehaviour
{
	public OpenClosePopup vipGO;

	public TMP_InputField email;

	public TMP_InputField number;

	public void SubmitVIPDetailsButton()
	{
		if (email.text.Length <= 0 || number.text.Length <= 0)
		{
			NativeUI.ShowToast("*Fields can not be empty.");
			return;
		}
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
		jSONObject.AddField("mobile", number.text);
		jSONObject.AddField("email", email.text);
		URL.Instance.DebugPrint("Submitting..." + jSONObject);
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.UPDATE_VIP_DETAILS, jSONObject.ToString())).Subscribe((Action<string>)delegate(string x)
		{
			TableData tableData = TableData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
			URL.Instance.DebugPrint(GameManager.Instance.security.Decrypt(x));
			if (tableData.status == "success")
			{
				NativeUI.ShowToast("Enjoy the game :) :)");
				vipGO.ClosePopup();
				//GameManager.Instance.VipDetails = 1;
			}
			else
			{
				NativeUI.ShowToast(tableData.popupMessageBody);
			}
		}, (Action<Exception>)delegate
		{
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
		});
	}
}
