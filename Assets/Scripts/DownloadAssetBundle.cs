using EasyMobile;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DownloadAssetBundle : MonoBehaviour
{
	private bool isDownloadStarted;

	[SerializeField]
	private string url;

	[SerializeField]
	private GameObject downloadUI;

	[SerializeField]
	private Slider progressSlider;

	[SerializeField]
	private Button downloadButton;

	[SerializeField]
	private GameObject downloadButtonImage;

	[SerializeField]
	private TableElements tableElements;

	private WWW www;

	private void Awake()
	{
		tableElements = FindObjectOfType<TableElements>();
	}

	private void Start()
	{
		if (PlayerPrefs.HasKey(url))
		{
			downloadUI.SetActive(value: false);
		}
	}

	public void DownloadAsset()
	{
		NativeUI.ShowToast("Download Started...");
		isDownloadStarted = true;
		StartCoroutine(DownloadAssetBundleCO(url));
		DisableButtons();
	}

	private IEnumerator DownloadAssetBundleCO(string _url)
	{
		URL.Instance.DebugPrint("Download Started");
		www = WWW.LoadFromCacheOrDownload(Path.Combine(URL.Instance.AssetBundleURL(), _url), 1);
		yield return www;
		PlayerPrefs.SetInt(_url, 1);
		URL.Instance.DebugPrint("Download Finished");
		NativeUI.ShowToast("Download Completed...");
		isDownloadStarted = false;
		downloadUI.SetActive(value: false);
		EnableButtons();
	}

	private void Update()
	{
		if (isDownloadStarted)
		{
			progressSlider.value = www.progress;
			URL.Instance.DebugPrint("Progress : " + www.progress);
		}
	}

	private void DisableButtons()
	{
		/*for (int i = 0; i < tableElements.localGiftItems.Length; i++)
		{
			tableElements.localGiftItems[i].interactable = false;
		}
		downloadButtonImage.SetActive(false);*/
	}

	private void EnableButtons()
	{
		/*for (int i = 0; i < tableElements.localGiftItems.Length; i++)
		{
			tableElements.localGiftItems[i].interactable = true;
		}*/
	}
}
