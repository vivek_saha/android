using EasyMobile;
using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GetBootAmount : MonoBehaviour
{
	public enum GameType
	{
		Limit,
		NoLimit,
		Private,
		Tournament,
		Variation_Limit,
		Variation_NoLimit
	}

	public GameType gameType;

	public string bootAmount;

	public TextMeshProUGUI bootAmountText;

	private FillMainMenu fillMenu;

	private void Awake()
	{
		fillMenu = FindObjectOfType<FillMainMenu>();
	}

	public void SelectBootAmount()
	{
		AudioManager.instance.PlaySound("click");
		GameManager.Instance.loadingCanvas.LoadingState(true, false);
		if (gameType == GameType.Limit)
		{
			URL.Instance.DebugPrint("Limit - Boot Amount : " + bootAmount);
			PlayNowButton(bootAmount);
		}
		else if (gameType == GameType.NoLimit)
		{
			URL.Instance.DebugPrint("No Limit - Boot Amount : " + bootAmount);
			NoLimitButton(bootAmount);
		}
		else if (gameType == GameType.Private)
		{
			GameManager.Instance.loadingCanvas.LoadingState(false, false);
			URL.Instance.DebugPrint("Private - Boot Amount : " + bootAmount);
			fillMenu.privateTableBootAmount = long.Parse(bootAmount);
		}
		else if (gameType == GameType.Tournament)
		{
			URL.Instance.DebugPrint("Tournament - Boot Amount : " + bootAmount);
			TournamentButton();
		}
		else if (gameType == GameType.Variation_Limit)
		{
			URL.Instance.DebugPrint("Variation_Limit - Boot Amount : " + bootAmount);
			VariationButton(bootAmount);
		}
		else if (gameType == GameType.Variation_NoLimit)
		{
			URL.Instance.DebugPrint("Variation_NoLimit - Boot Amount : " + bootAmount);
			VariationNoLimitButton(bootAmount);
		}
	}

	public void CreatePrivateTable()
	{
		if (fillMenu.privateTableBootAmount == 0)
		{
			URL.Instance.DebugPrint("Please select any boot amount");
			NativeUI.ShowToast("Please select any boot amount");
		}
		else
		{
			PrivateButton(fillMenu.privateTableBootAmount.ToString());
			GameManager.Instance.loadingCanvas.LoadingState(true, false);
		}
	}

	public void JoinPreviousTable()
	{
		string @string = PlayerPrefs.GetString("PreviousBootAmount", "0");
		if (@string == "0")
		{
			URL.Instance.DebugPrint("Previous table not found");
			NativeUI.ShowToast("Table not found");
		}
		else
		{
			URL.Instance.DebugPrint("boot amount : " + @string);
			PrivateButton(@string);
			GameManager.Instance.loadingCanvas.LoadingState(true, false);
		}
	}

	private void OnEnable()
	{
		fillMenu.privateTableBootAmount = 0L;
		if (long.Parse(bootAmount) > GameManager.Instance.localPlayerChips)
		{
			base.gameObject.GetComponent<Button>().interactable = false;
		}
		else
		{
			base.gameObject.GetComponent<Button>().interactable = true;
		}
	}

	public void PlayNowButton(string _bootAmount)
	{
		PlayCO(GameManager.Instance.localPlayerID, "limit", _bootAmount);
	}

	public void NoLimitButton(string _bootAmount)
	{
		PlayCO(GameManager.Instance.localPlayerID, "nolimit", _bootAmount);
	}

	public void PrivateButton(string _bootAmount)
	{
		PlayCO(GameManager.Instance.localPlayerID, "private", _bootAmount);
	}

	public void TournamentButton()
	{
		PlayCO(GameManager.Instance.localPlayerID, "OLC", "1000");
	}

	public void VariationButton(string _bootAmount)
	{
		PlayCO(GameManager.Instance.localPlayerID, "variation_limit", _bootAmount);
	}

	public void VariationNoLimitButton(string _bootAmount)
	{
		PlayCO(GameManager.Instance.localPlayerID, "variation_nolimit", _bootAmount);
	}

	private void PlayCO(string _playerID, string _type, string _bootAmount)
	{
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("playerId", _playerID);
		jSONObject.AddField("type", _type);
		jSONObject.AddField("bootAmount", _bootAmount);

		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.FIND_TABLE, jSONObject.ToString())).Subscribe(delegate (string x)
		{
			URL.Instance.DebugPrint("FIND_TABLE " + x);
			SocketData socketData = SocketData.CreateFromJSON(x);

			if (socketData.status == "success")
			{
				GameManager.Instance.host = socketData.response.host;
				GameManager.Instance.port = socketData.response.port;
				GameManager.Instance.tbid = socketData.response.tbid;
				GameManager.Instance.type = socketData.response.type;
				GameManager.Instance.tblBootedAmount = socketData.response.tblBootedAmount;

				GameManager.Instance.adsManager.ShowInterstitial();

				SceneManager.LoadScene(URL.Instance.TABLE_SCENE);
			}
			else
			{
				NativeUI.Alert("Oh Snap!", socketData.popupMessageBody);
				GameManager.Instance.loadingCanvas.LoadingState(false, false);
			}
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
		});
	}
}
