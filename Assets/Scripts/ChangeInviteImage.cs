using UnityEngine;
using UnityEngine.UI;

public class ChangeInviteImage : MonoBehaviour
{
	public Sprite sp;

	public Image img;

	private void Update()
	{
		if (PlayerPrefs.GetInt("Language") == 1)
		{
			img.sprite = sp;
		}
	}
}
