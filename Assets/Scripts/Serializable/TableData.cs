using System;
using UnityEngine;

[Serializable]
public class TableData
{
	public string status;

	public string responseMessage;

	public string popupMessageTitle;

	public string popupMessageBody;

	public TableDataResponse[] response;

	public static TableData CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<TableData>(data);
	}
}
